package de.heining21.Helfer;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;

public class FileUtils {
	
	public static void createOrdner(String path){
		
		File file = new File(path);
		
		if(file.isDirectory()){
			
			file.mkdirs();
			
		}
	}

	public static boolean saveYAMLCfg(FileConfiguration cfg, File file){
		
		try {
			cfg.save(file);
			return true;
		} catch (IOException e) {
			return false;
		}
		
	}
	
	
	
	
}
