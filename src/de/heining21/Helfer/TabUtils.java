package de.heining21.Helfer;

import java.lang.reflect.Field;

import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_8_R1.PlayerConnection;

import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class TabUtils
{
  
  
  public static void setTab(String head, String foot, Player plr)
  {
    CraftPlayer craftplayer = (CraftPlayer)plr;
    PlayerConnection connection = craftplayer.getHandle().playerConnection;
    IChatBaseComponent header = ChatSerializer.a("{\"text\":\"" + head + 
      "\"}");
    IChatBaseComponent footer = ChatSerializer.a("{\"text\":\"" + foot + 
      "\"}");
    PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
    try
    {
      Field headerField = packet.getClass().getDeclaredField("a");
      headerField.setAccessible(true);
      headerField.set(packet, header);
      headerField.setAccessible(!headerField.isAccessible());
      Field footerField = packet.getClass().getDeclaredField("b");
      footerField.setAccessible(true);
      footerField.set(packet, footer);
      footerField.setAccessible(!footerField.isAccessible());
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    connection.sendPacket(packet);
  }
}
