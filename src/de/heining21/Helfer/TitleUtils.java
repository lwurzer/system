package de.heining21.Helfer;

import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.EnumTitleAction;
import net.minecraft.server.v1_8_R1.PacketPlayOutTitle;

import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class TitleUtils {

	/**
	 * Send a Title
	 * 
	 * @param p
	 			  Erhaltender Spieler
	 * @param title
	 *            Json Title String
	 */
	public static void sendTitle(Player p, String title) {
		try {
			PacketPlayOutTitle ppot = new PacketPlayOutTitle(EnumTitleAction.TITLE,  ChatSerializer.a("{\"text\": \"" + title + "\"}"));

			((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppot);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Send a Subtitle
	 * 
	 * @param p
	 *            Player to send the SubTitle to
	 * @param subtitle
	 *            Json SubTitle String
	 */
	public static void sendSubTitle(Player p, String subtitle) {
		try {
			PacketPlayOutTitle ppot2 = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE,  ChatSerializer.a("{\"text\": \"" + subtitle + "\"}"));

			((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppot2);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set the Title Timings
	 * 
	 * @param p
	 *            Player to Update the Timings
	 * @param fadeIn
	 *            Time it should take to fade In
	 * @param stay
	 *            Time the Title should stay on screen
	 * @param fadeOut
	 *            Time it should take to fade Out
	 */
	public static void sendTimings(Player p, int fadeIn, int stay, int fadeOut) {
		try {
			PacketPlayOutTitle times = new PacketPlayOutTitle(fadeIn, stay,fadeOut);
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(times);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reset the Players Timing, Title, SubTitle
	 * 
	 * @param p
	 *            Player to Reset
	 */
	public static void reset(Player p) {
		try {
			@SuppressWarnings("static-access")
			PacketPlayOutTitle ppot2 = new PacketPlayOutTitle(EnumTitleAction.RESET,  new ChatSerializer().a(""));

			((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppot2);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
}
