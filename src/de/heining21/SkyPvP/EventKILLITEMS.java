package de.heining21.SkyPvP;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import de.heining21.System.Data;

public class EventKILLITEMS implements Listener {

	@EventHandler
	public void onKill(PlayerDeathEvent e) {
		Player killer = e.getEntity().getKiller();
		if (killer instanceof Player) {
			if (killer.getWorld() == Bukkit.getWorld("SkyPvP")) {

				ItemStack dia = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
				killer.getInventory().addItem(new ItemStack[] { dia });
				killer.sendMessage(Data.p + "�eDir wurde �a1 Diamant �ef�r den Kill gegeben.");
			}
		}
	}
}