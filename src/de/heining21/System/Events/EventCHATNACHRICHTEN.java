package de.heining21.System.Events;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class EventCHATNACHRICHTEN implements Listener {
	
	public static String smiley = ChatColor.GREEN + "☺" + ChatColor.RESET;
	public static String sad = ChatColor.RED + "☹" + ChatColor.RESET;
	public static String heart = ChatColor.DARK_RED + "❤" + ChatColor.RESET;
	public static String arrowLeft = ChatColor.GOLD + "←" + ChatColor.RESET;
	public static String arrowRight = ChatColor.GOLD + "→" + ChatColor.RESET;
	public static String cloud = ChatColor.GOLD + "☁" + ChatColor.RESET;
	public static String sun = ChatColor.GOLD + "☀" + ChatColor.RESET;
	public static String umbrella = ChatColor.GOLD + "☂" + ChatColor.RESET;
	public static String snowman = ChatColor.GOLD + "☃" + ChatColor.RESET;
	public static String comet = ChatColor.GOLD + "☄" + ChatColor.RESET;
	public static String star = ChatColor.GOLD + "★" + ChatColor.RESET;
	public static String phone = ChatColor.GOLD + "☎" + ChatColor.RESET;
	public static String skull = ChatColor.GOLD + "☠" + ChatColor.RESET;
	public static String radioactive = ChatColor.GOLD + "☢" + ChatColor.RESET;
	public static String biohazard = ChatColor.GOLD + "☣" + ChatColor.RESET;
	public static String peace = ChatColor.GOLD + "☮" + ChatColor.RESET;
	public static String yingyang = ChatColor.GOLD + "☯" + ChatColor.RESET;
	public static String moon = ChatColor.GOLD + "☾" + ChatColor.RESET;
	public static String crown = ChatColor.GOLD + "♔" + ChatColor.RESET;
	public static String music = ChatColor.GOLD + "♩" + ChatColor.RESET;
	public static String scissor = ChatColor.GOLD + "✁" + ChatColor.RESET;
	public static String plane = ChatColor.GOLD + "✈" + ChatColor.RESET;
	public static String mail = ChatColor.GOLD + "✉" + ChatColor.RESET;
	public static String pencil = ChatColor.GOLD + "✎" + ChatColor.RESET;
	public static String check = ChatColor.GOLD + "✓" + ChatColor.RESET;
	public static String flower = ChatColor.GOLD + "✿" + ChatColor.RESET;
	public static String yuno = ChatColor.GOLD + "ლ(ಠ益ಠლ)" + ChatColor.RESET;
	public static String tableflip = ChatColor.GOLD + "(╯°□°）╯︵ ┻━┻" + ChatColor.RESET;
	public static String fuckyou = ChatColor.GOLD + "┌∩┐(◣_◢)┌∩┐" + ChatColor.RESET;
	public static String meh = ChatColor.GOLD + "¯\\_(ツ)_/¯" + ChatColor.RESET;
	public static String bear = ChatColor.GOLD + "ˁ˚ᴥ˚ˀ" + ChatColor.RESET;
	public static String fun = ChatColor.DARK_RED + "System," + ChatColor.RESET;
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onChat(AsyncPlayerChatEvent e)  {
	    String message = e.getMessage();
	    message = message.replace(":)", smiley);
	    message = message.replace(":(", sad);
	    message = message.replace("<3", heart);
	    message = message.replace("<-", arrowLeft);
	    message = message.replace("->", arrowRight);
	    message = message.replace("(cloud)", cloud);
	    message = message.replace("(sun)", sun);
	    message = message.replace("(umbrella)", umbrella);
	    message = message.replace("(snowman)", snowman);
	    message = message.replace("(comet)", comet);
	    message = message.replace("(star)", star);
	    message = message.replace("(phone)", phone);
	    message = message.replace("(skull)", skull);
	    message = message.replace("(radioactive)", radioactive);
	    message = message.replace("(biohazard)", biohazard);
	    message = message.replace("(peace)", peace);
	    message = message.replace("(yingyang)", yingyang);
	    message = message.replace("(moon)", moon);
	    message = message.replace("(crown)", crown);
	    message = message.replace("(music)", music);
	    message = message.replace("(scissor)", scissor);
	    message = message.replace("(plane)", plane);
	    message = message.replace("(mail)", mail);
	    message = message.replace("(pencil)", pencil);
	    message = message.replace("(check)", check);
	    message = message.replace("(yuno)", yuno);
	    message = message.replace("(tableflip)", tableflip);
	    message = message.replace("(fuckyou)", fuckyou);
	    message = message.replace("(meh)", meh);
	    message = message.replace("(bear)", bear);
	    message = message.replace("~", "‰ˆ");
	    e.setMessage(message);
	  }
}
