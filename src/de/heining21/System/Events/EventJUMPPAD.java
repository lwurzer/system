package de.heining21.System.Events;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class EventJUMPPAD implements Listener {
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();

		if (p.getLocation().getBlock().getType() == Material.CARPET) {
			if (p.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() == Material.SPONGE) {
				org.bukkit.util.Vector v = p.getLocation().getDirection().multiply(2.5D).setY(1D);
				p.setVelocity(v);
				p.playSound(p.getLocation(), Sound.LEVEL_UP, 50F, 1F);
			}
		}
	}
}
