package de.heining21.System.Events;

import java.io.IOException;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import de.heining21.System.Main;
import de.heining21.System.commands.Ban.CommandBAN;
import de.heining21.System.commands.Ban.CommandWARN;
import de.heining21.System.commands.Game.CommandWARTUNG;
import de.heining21.System.utils.UUIDCache;

public class EventLOGINBANCHECK implements Listener {
	private Main plugin;

	public EventLOGINBANCHECK(Main instance) {
		plugin = instance;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onBanned(PlayerLoginEvent event) {
		Player p = event.getPlayer();
		
		if(CommandWARTUNG.Wartung()){
			
			if(p.hasPermission("System.wartung")){
				
			}else{
				
				event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "�4�lDer Wartungmodus ist aktiv. Versuche es sp�ter nochmal!");
				return;
				
			}
			
		}
		
		if (CommandBAN.isBanned(event.getPlayer().getName())) {
			String UUID = UUIDCache.getUserUUIDOffline(p.getName()).toString();
			String grund = CommandBAN.speere
					.getString("ban." + UUID + ".grund").replace("&", "�");
			if (CommandBAN.getBanTime(event.getPlayer().getName()) == 0) {
				event.disallow(
						PlayerLoginEvent.Result.KICK_OTHER,
						ChatColor.RED
								+ "Du bist permanent gebannt! Grund: "
								+ ChatColor.translateAlternateColorCodes(
										'&',
										new StringBuilder()
												.append(ChatColor.AQUA)
												.append(grund).toString()));
				return;
			} else {

				int resttime = (int) CommandBAN.getBanTime(event.getPlayer()
						.getName());
				if (resttime <= 0) {

					CommandBAN.speere.set("ban." + UUID + ".isBanned", false);
					try {
						CommandBAN.speere.save(CommandBAN.sperre);
					} catch (IOException e) {

					}

				} else {

					int hr = resttime / 3600;
					int rem = resttime % 3600;
					int mn = rem / 60;
					int sec = rem % 60;
					String hrStr = (hr < 10 ? "0" : "") + hr;
					String mnStr = (mn < 10 ? "0" : "") + mn;
					String secStr = (sec < 10 ? "0" : "") + sec;

					event.disallow(PlayerLoginEvent.Result.KICK_BANNED,
							"Du wurdest wegen �5'" + grund
									+ "�5'�r gebannt. Es bleibt noch �5"
									+ hrStr + ":" + mnStr + ":" + secStr);

				}

			}

		}
		if (CommandWARN.isBann(UUIDCache.getUserUUIDOffline(event.getPlayer()
				.getName()))) {
			UUID uuid = UUIDCache.getUserUUIDOffline(event.getPlayer()
					.getName());
			int warns = CommandWARN.warnings.getInt("warns." + uuid);
			String grund1 = warns + " Verwarnungen";
			event.disallow(PlayerLoginEvent.Result.KICK_OTHER, ChatColor.RED
					+ "Du bist permanent gebannt! Grund: " + ChatColor.AQUA
					+ "AUTO-BAN " + grund1);
		}
	}
}
