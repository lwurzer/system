package de.heining21.System.Events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import de.heining21.System.Data;
import de.heining21.System.Main;
import de.heining21.System.Money.MoneyUtils;

public class EventPVP_SHOP implements Listener {

	@SuppressWarnings("unused")
	private Main plugin;
	public EventPVP_SHOP(Main plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onRightclic(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (p.getWorld() != Bukkit.getWorld("Lobby") || p.getWorld() != Bukkit.getWorld("SkyPvP") || p.getWorld() != Bukkit.getWorld("GunGame")) {
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if ((e.getClickedBlock().getType() == Material.WALL_SIGN) || (e.getClickedBlock().getType() == Material.SIGN_POST)) {
					Sign s = (Sign) e.getClickedBlock().getState();
					
					//KAUFEN
					if (s.getLine(0).contains("[Kaufen]")) {
						String Line1 = s.getLine(1);
						String Line2 = s.getLine(2);
						String Line3 = s.getLine(3);

						if (!Line2.isEmpty()) {
							String[] MoneyCostSplit = Line2.split(" ");
							int MoneyCost = Integer.parseInt(MoneyCostSplit[0]);

							if (!Line3.isEmpty()) {
								if ((((Integer) MoneyUtils.getUserMoney(p) > MoneyCost) || (((Integer) MoneyUtils.getUserMoney(p) == MoneyCost)))) {
									MoneyUtils.takeUserMoney(p, MoneyCost);

									String[] ItemSplit = Line3.split(":");
									String IDstring = ItemSplit[0];
									String Amountstring = ItemSplit[1];
									int ID = Integer.parseInt(IDstring);
									int Amount = Integer.parseInt(Amountstring);
									
									@SuppressWarnings("deprecation")
									ItemStack ShopItem = new ItemStack(Material.getMaterial(ID), Amount);
									p.sendMessage(Data.p + "Du hast dir erfolgreich das Item '�a" + Line1 + "�e' f�r �2" + MoneyCost + " GC �egekauft.");
									p.getInventory().addItem(new ItemStack[] { ShopItem });
								} else {
									p.sendMessage(Data.p + "�cDu hast nicht gen�gend Geld um dir das Item '�4" + Line1 + "�c' zu kaufen.");
								}
							} else
								p.sendMessage(Data.p + "�cEs wurde kein Preis festgelegt!");
						} else {
							p.sendMessage(Data.p + "�cEs wurde kein Preis festgelegt!");
						}
					}
					
					//VERKAUFEN
	/*				if (s.getLine(0).contains("[Verkaufen]")) {
						String Line1 = s.getLine(1);
						String Line2 = s.getLine(2);
						String Line3 = s.getLine(3);

						if (!Line2.isEmpty()) {
							String[] MoneyCostSplit = Line2.split(" ");
							int MoneyCost = Integer.parseInt(MoneyCostSplit[0]);

							if (!Line3.isEmpty()) {
								if ((((Integer) MoneyUtils.getUser2Money(p2) > MoneyCost) || (((Integer) MoneyUtils.getUserMoney(p) == MoneyCost)))) {
									MoneyUtils.takeUserMoney(p, MoneyCost);

									String[] ItemSplit = Line3.split(":");
									String IDstring = ItemSplit[0];
									String Amountstring = ItemSplit[1];
									int ID = Integer.parseInt(IDstring);
									int Amount = Integer.parseInt(Amountstring);

									@SuppressWarnings("deprecation")
									ItemStack ShopItem = new ItemStack(Material.getMaterial(ID), Amount);
									p.sendMessage(Data.p + "Du hast dir erfolgreich das Item '�a" + Line1 + "�e' f�r �2" + MoneyCost + " GC �egekauft.");
									p.getInventory().addItem(new ItemStack[] { ShopItem });
								} else {
									p.sendMessage(Data.p + "�cDu hast nicht gen�gend Geld um dir das Item '�4" + Line1 + "�c' zu kaufen.");
								}
							} else
								p.sendMessage(Data.p + "�cEs wurde kein Preis festgelegt!");
						} else {
							p.sendMessage(Data.p + "�cEs wurde kein Preis festgelegt!");
						}
					}
					*/
					
				}
			}
		}
	}
}