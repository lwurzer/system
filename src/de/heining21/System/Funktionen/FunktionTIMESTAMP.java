package de.heining21.System.Funktionen;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FunktionTIMESTAMP {
	
	static File file = new File("plugins/System/TimeStamps", "TimeStamps.yml");
	static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	public void addTimestamp(int TimeInSec){
		
		long timestamp = System.currentTimeMillis();
		int time = TimeInSec*1000;
		long nextTime = time + timestamp;
		
		cfg.set("TimeStamp", nextTime);
		try {
			cfg.save(file);
		} catch (IOException e) {
		}
	}
	
	public boolean isReady() {
		long time = cfg.getLong("TimeStamp");
		if(time >= System.currentTimeMillis()){
			return true;
		}
		return false;
	}
}
