package de.heining21.System.Funktionen;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class FunktionenAUTOMSG {
	
	
	public static File file = new File("plugins/System", "automsg.yml");
	public static FileConfiguration cfg;
	private static Main main;
	static ArrayList<String> msgs = new ArrayList<String>();
	static int timeInS;
	static int taskId;
	static int lenght;
	
	public FunktionenAUTOMSG(Main main){
		
		cfg = YamlConfiguration.loadConfiguration(file);
		FunktionenAUTOMSG.main = main;
		
	}
	public static void createFile(){
		
		ArrayList<String> Messages = new ArrayList<String>();
		Messages.add("Mit einer Spende fuer einen Rang erhaeltst du automatisch deinen Rang in Minecraft, sowie auf dem TS.");
		Messages.add("Du brauchst Hilfe? &c/Support");
		Messages.add("Owner in Skype? &c/Skype");
		Messages.add("Rede mit anderen im TS� &c/TS");
		Messages.add("Sehe das Team mit &c/Team");
		Messages.add("Raenge mit mehr Rechten: &c/Rang");
		
		
		
		cfg.addDefault("Messages", Messages);
		cfg.addDefault("TimeInS", 500);
		cfg.options().copyDefaults(true);
		try {
			cfg.save(file);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	
	
	public static void loadMSG(){
		
		createFile();
		java.util.List<String> load = cfg.getStringList("Messages");
		for(String s : load){
			
			msgs.add(s);
			
		}
		timeInS = cfg.getInt("TimeInS");
		
		
		
	}
	public static void startMsg(){
		
		loadMSG();
		
		lenght = msgs.size() - 1;
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
			
			public void run() {
				
				if(lenght == 0){
					lenght = msgs.size() - 1;
				}
				
				Bukkit.broadcastMessage(Data.stern + msgs.get(lenght).replace("ae", "�")
						.replace("ue", "�")
						.replace("oe", "�")
						.replace("&", "�"));
				
				lenght --;
			}
		}, 0, timeInS*20);
		
	}
	public static void stopMsg(){
		
		Bukkit.getScheduler().cancelTask(taskId);
		
	}
	

}
