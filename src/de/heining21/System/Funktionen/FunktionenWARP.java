package de.heining21.System.Funktionen;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class FunktionenWARP implements Listener {

	public static void CheckOrdner() {
		File file = new File(path);
		if (file.isDirectory() == false) {
			file.mkdirs();
		}

	}

	public static void DelWarp(Player p, String WarpName) {
		File file = new File(path, WarpName.toLowerCase() + ".yml");
		if (file.exists()) {
			file.delete();
			p.sendMessage(Data.p + "�eWarp �a" + WarpName
					+ " �ewurde erfolgreich gel�scht.");
			return;
		}
		if (!file.exists()) {
			p.sendMessage(Data.p + "�cDieser Warp existiert nicht.");

		}

	}

	public static void SetWarp(Player p, String WarpName) throws IOException {
		File file = new File(path, WarpName.toLowerCase() + ".yml");
		FunktionenWARP.CheckOrdner();

		if (file.exists()) {
			p.sendMessage(Data.p + "�cDieser Warp existiert bereits");
			return;
		}

		else {
			file.createNewFile();

			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			cfg.set("Location.X", p.getLocation().getX());
			cfg.set("Location.Y", p.getLocation().getY());
			cfg.set("Location.Z", p.getLocation().getZ());
			cfg.set("Location.Yaw", p.getLocation().getYaw());
			cfg.set("Location.Pitch", p.getLocation().getPitch());
			cfg.set("Location.World", p.getWorld().getName());
			cfg.set("Name", WarpName);
			cfg.save(file);
			p.sendMessage(Data.p + "�eWarp�a " + WarpName
					+ " �ewurde erfolgreich erstellt.");
		}

	}

	public static void Warp(final Player p, final String WarpName) {
		File file = new File(path, WarpName.toLowerCase() + ".yml");
		if (!file.exists()) {

			p.sendMessage(Data.p + "�cDieser Warp existiert nicht.");
			return;

		}
		pToWarp.add(p.getName());
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		double x = cfg.getDouble("Location.X");
		double y = cfg.getDouble("Location.Y");
		double z = cfg.getDouble("Location.Z");
		float yaw = (float) cfg.getDouble("Location.Yaw");
		float pitch = (float) cfg.getDouble("Location.Pitch");
		String worldname = cfg.getString("Location.World");

		final Location loc = p.getLocation();
		loc.setX(x);
		loc.setY(y);
		loc.setZ(z);
		loc.setYaw(yaw);
		loc.setPitch(pitch);
		loc.setWorld(Bukkit.getWorld(worldname));

		p.sendMessage(Data.pfett
				+ "�eDu wirst in �a3 Sekunden �eteleportiert. �7Bewege dich nicht.");

		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {

				if (pToWarp.contains(p.getName())) {
					p.teleport(loc);
					pToWarp.remove(p.getName());
					p.sendMessage(Data.p + "�eDu wurdest zum Warp �a"
							+ WarpName + " �eteleportiert.");
					p.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 0);
					p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 50F,
							1F);

				}

			}
		}, 60L);
	}

	private static Main plugin;

	public static String path = "plugins/System/Locations/Warps";

	public static ArrayList<String> pToWarp = new ArrayList<String>();

	public FunktionenWARP(Main main) {
		plugin = main;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {

		if (sender instanceof ConsoleCommandSender) {
			System.out
					.println("�cDu musst ein spieler sein, um diesen Befehl auszufuehren");
			return true;
		}
		Player p = (Player) sender;

		// Warp
		if (cmd.getName().equalsIgnoreCase("warp")) {

			if (args.length == 0) {
				p.sendMessage(Data.ZuWenigArgs);
				p.sendMessage(Data.p + "�cBenutze /warp <warpname>");
				return true;
			}
			if (args.length == 1) {
				if (!p.hasPermission("System.warp")) {
					p.sendMessage(Data.KeineRechte);
					return true;
				}
				FunktionenWARP.Warp(p, args[0]);
				// p.sendMessage("�7Du wurdest erfolgreich zum Warp �a" +
				// args[0] + " �7geportet");
				return true;
			}
			if (args.length >= 2) {
				p.sendMessage(Data.ZuVieleArgs);
				p.sendMessage(Data.p + "�cBenutze: /warp <warpname>");
				return true;
			}
		}
		// Set Warp
		if (cmd.getName().equalsIgnoreCase("setwarp")) {

			if (args.length == 0) {
				p.sendMessage(Data.ZuWenigArgs);
				return true;
			}
			if (args.length == 1) {
				if (!p.hasPermission("System.warp.set")) {
					p.sendMessage(Data.KeineRechte);
					return true;
				}
				try {
					FunktionenWARP.SetWarp(p, args[0]);
					// p.sendMessage(Data.p + "�7Warp �a" + args[0] +
					// "�7 wurde erfolgreich erstellt.");
				} catch (IOException e) {

					p.sendMessage(Data.p
							+ "�4Fehler: Wende dich an einen Admin oder Dev");
				}
				return true;
			}
			if (args.length >= 2) {

				p.sendMessage(Data.ZuVieleArgs);
				p.sendMessage(Data.p + "�cBenutze: /setwarp <warpname>");
				return true;

			}

		}
		// Delete Warp
		if (cmd.getName().equalsIgnoreCase("delwarp")) {

			if (args.length == 0) {
				p.sendMessage(Data.ZuWenigArgs);
				p.sendMessage(Data.p + "�cBenutze: /delwarp <warpname>");
				return true;
			}
			if (args.length == 1) {
				if (!p.hasPermission("System.warp.del")) {
					p.sendMessage(Data.KeineRechte);
					return true;
				}
				FunktionenWARP.DelWarp(p, args[0]);

				return true;
			}
			if (args.length >= 2) {
				p.sendMessage(Data.ZuVieleArgs);
				p.sendMessage(Data.p + "�cBenutze: /delwarp <warpname>");
				return true;
			}

		}

		return false;
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();

		if (e.getFrom().getX() == e.getTo().getX()
				&& e.getFrom().getY() == e.getTo().getY()
				&& e.getFrom().getZ() == e.getTo().getZ()) {

			return;

		}

		if (pToWarp.contains(p.getName())) {
			pToWarp.remove(p.getName());
			p.sendMessage(Data.p
					+ "�fDer Teleportvorgang wurde abgebrochen, da du dich bewegt hast.");

		}

	}

}
