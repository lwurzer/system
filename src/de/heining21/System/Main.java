package de.heining21.System;

import java.io.IOException;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerOptions;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedServerPing;

import de.heining21.SkyPvP.EventFREESCHILDER;
import de.heining21.System.Events.EventANTIWOERTER;
import de.heining21.System.Events.EventAUTO_COMPLETE;
import de.heining21.System.Events.EventBUGFIX;
import de.heining21.System.Events.EventCHATNACHRICHTEN;
import de.heining21.System.Events.EventJOIN_LEAVE;
import de.heining21.System.Events.EventJUMPPAD;
import de.heining21.System.Events.EventLOGINBANCHECK;
import de.heining21.System.Events.EventOP_APFEL;
import de.heining21.System.Events.EventUNBEKANNTERBEFEHL;
import de.heining21.System.Funktionen.FunktionenAUTOMSG;
import de.heining21.System.Funktionen.FunktionenWARP;
import de.heining21.System.MySQL.MySQL;
import de.heining21.System.ServerListe.ProtocolLib;
import de.heining21.System.commands.Ban.CommandBAN;
import de.heining21.System.commands.Ban.CommandDELWARN;
import de.heining21.System.commands.Ban.CommandKICK;
import de.heining21.System.commands.Ban.CommandKICKALL;
import de.heining21.System.commands.Ban.CommandTEMPBAN;
import de.heining21.System.commands.Ban.CommandUNBAN;
import de.heining21.System.commands.Ban.CommandWARN;
import de.heining21.System.commands.Chat.CommandBC;
import de.heining21.System.commands.Chat.CommandBLOCKER;
import de.heining21.System.commands.Chat.CommandCC;
import de.heining21.System.commands.Chat.CommandCOINSHOP;
import de.heining21.System.commands.Chat.CommandCOUNTDOWN;
import de.heining21.System.commands.Chat.CommandGLOBALMUTE;
import de.heining21.System.commands.Chat.CommandGMLIST;
import de.heining21.System.commands.Chat.CommandJA;
import de.heining21.System.commands.Chat.CommandMSG;
import de.heining21.System.commands.Chat.CommandMUTE;
import de.heining21.System.commands.Chat.CommandNEIN;
import de.heining21.System.commands.Chat.CommandONLINE;
import de.heining21.System.commands.Chat.CommandOTC;
import de.heining21.System.commands.Chat.CommandPEX;
import de.heining21.System.commands.Chat.CommandRANDOM;
import de.heining21.System.commands.Chat.CommandREPORT;
import de.heining21.System.commands.Chat.CommandSAY;
import de.heining21.System.commands.Chat.CommandSKYPE;
import de.heining21.System.commands.Chat.CommandSYSTEM;
import de.heining21.System.commands.Chat.CommandTC;
import de.heining21.System.commands.Chat.CommandTEAM;
import de.heining21.System.commands.Chat.CommandTS;
import de.heining21.System.commands.Chat.CommandUMFRAGE;
import de.heining21.System.commands.Chat.CommandVOTE;
import de.heining21.System.commands.Game.CommandAFK;
import de.heining21.System.commands.Game.CommandARMOURREPAIR;
import de.heining21.System.commands.Game.CommandBUTCHER;
import de.heining21.System.commands.Game.CommandCLEARENDERCHEST;
import de.heining21.System.commands.Game.CommandCLEARINV;
import de.heining21.System.commands.Game.CommandCLEARLAG;
import de.heining21.System.commands.Game.CommandENCHANT;
import de.heining21.System.commands.Game.CommandENDERCHEST;
import de.heining21.System.commands.Game.CommandFEED;
import de.heining21.System.commands.Game.CommandFIX;
import de.heining21.System.commands.Game.CommandFLY;
import de.heining21.System.commands.Game.CommandFLYSPEED;
import de.heining21.System.commands.Game.CommandGIVEALL;
import de.heining21.System.commands.Game.CommandGM;
import de.heining21.System.commands.Game.CommandHAT;
import de.heining21.System.commands.Game.CommandHEAL;
import de.heining21.System.commands.Game.CommandHOME;
import de.heining21.System.commands.Game.CommandINVSEE;
import de.heining21.System.commands.Game.CommandITEMDB;
import de.heining21.System.commands.Game.CommandKILL;
import de.heining21.System.commands.Game.CommandKIT;
import de.heining21.System.commands.Game.CommandMOBEVENT;
import de.heining21.System.commands.Game.CommandMONEY;
import de.heining21.System.commands.Game.CommandMORE;
import de.heining21.System.commands.Game.CommandMOTD;
import de.heining21.System.commands.Game.CommandPTIME;
import de.heining21.System.commands.Game.CommandRELOAD;
import de.heining21.System.commands.Game.CommandRENAME;
import de.heining21.System.commands.Game.CommandSPAWN;
import de.heining21.System.commands.Game.CommandSPAWNER;
import de.heining21.System.commands.Game.CommandSPEED;
import de.heining21.System.commands.Game.CommandSTACK;
import de.heining21.System.commands.Game.CommandSTACKMODE;
import de.heining21.System.commands.Game.CommandSTOP;
import de.heining21.System.commands.Game.CommandSUDO;
import de.heining21.System.commands.Game.CommandSUPPORT;
import de.heining21.System.commands.Game.CommandTAB;
import de.heining21.System.commands.Game.CommandTIME;
import de.heining21.System.commands.Game.CommandTP;
import de.heining21.System.commands.Game.CommandTPA;
import de.heining21.System.commands.Game.CommandTPACCEPT;
import de.heining21.System.commands.Game.CommandTPALL;
import de.heining21.System.commands.Game.CommandTPDENY;
import de.heining21.System.commands.Game.CommandTPHERE;
import de.heining21.System.commands.Game.CommandVANISH;
import de.heining21.System.commands.Game.CommandVORBAU;
import de.heining21.System.commands.Game.CommandWALKSPEED;
import de.heining21.System.commands.Game.CommandWARP;
import de.heining21.System.commands.Game.CommandWARTUNG;
import de.heining21.System.commands.Game.CommandWB;
import de.heining21.System.commands.Game.CommandWEATHER;
import de.heining21.System.commands.Game.CommandWHITELIST;
import de.heining21.System.commands.Game.CommandWHOIS;
import de.heining21.System.commands.Game.CommandXP;
import de.heining21.System.utils.UTIL_CreateFiles;
import de.heining21.System.utils.UUIDCache;

public class Main extends JavaPlugin implements Listener, CommandExecutor {
	
	public int taskid = -1;
	public int msg = 0;
	public int status = -1;
	public static String Modt1;
	public static String Motd2;
	public static String TabHeader;
	public static String TabFooter;
	
	public void onEnable() {
		if (Bukkit.getPluginManager().isPluginEnabled("ProtocolLib") && Bukkit.getPluginManager().isPluginEnabled("FFA") && Bukkit.getPluginManager().isPluginEnabled("PermissionsEx") && Bukkit.getPluginManager().isPluginEnabled("Votifier")) {
			Bukkit.getConsoleSender().sendMessage("�7�l[]=====<�6�lSystem�7�l>=====[]");
			Bukkit.getConsoleSender().sendMessage(" �a=> Plugin wurde aktiviert.");
			Bukkit.getConsoleSender().sendMessage("�b <System von heining21 & Higgs_01>");
			Bukkit.getConsoleSender().sendMessage("�7�l[]=====<�6�lSystem�7�l>=====[]");
			
			CreateFiles();
			UUIDCache.addUsersToUUIDCache();
			loadCommands();
			loadEvents();
			
			loadUtils();
			loadMotd();

			UTIL_CreateFiles.createConfig();

			EventANTIWOERTER.createBlacklist();
			EventANTIWOERTER.startScanner();
		} else {
			Bukkit.getConsoleSender().sendMessage("�4Das System wurde nicht aktiviert");
			Bukkit.getConsoleSender().sendMessage("�cGrund: Nicht alle benoetigten Plugins installiert.");
			Bukkit.getConsoleSender().sendMessage("�5�l => Benoetigte: FFA, ProtocolLib, Votifier, PermissionsEx");
			return;
		}
	}
	
	/*
	 * Der Motd und der Tab werden geladen
	 */
	public static void loadMotd() {
		ProtocolLib.createModtFile();

		Modt1 = ProtocolLib.cfg.getString("MotdLine1").replace("&", "�");
		Motd2 = ProtocolLib.cfg.getString("ModtLine2").replace("&", "�");
		TabHeader = ProtocolLib.cfg.getString("Tab.Header").replace("&", "�");
		TabFooter = ProtocolLib.cfg.getString("Tab.Footer").replace("&", "�");

		ProtocolLib.loadPlayerHoverList();
	}
	
	/*
	 * Alle Befehle werden geladen
	 */
	public void loadCommands() {
		getCommand("say").setExecutor(new CommandSAY(this));
		getCommand("bc").setExecutor(new CommandBC(this));
		getCommand("ja").setExecutor(new CommandJA(this));
		getCommand("nein").setExecutor(new CommandNEIN(this));
		getCommand("umfrage").setExecutor(new CommandUMFRAGE(this));
		getCommand("otc").setExecutor(new CommandOTC(this));
		getCommand("tc").setExecutor(new CommandTC(this));
		getCommand("random").setExecutor(new CommandRANDOM(this));
		getCommand("ts").setExecutor(new CommandTS());
		getCommand("skype").setExecutor(new CommandSKYPE());
		getCommand("team").setExecutor(new CommandTEAM());
		getCommand("cc").setExecutor(new CommandCC());
		getCommand("online").setExecutor(new CommandONLINE());
		getCommand("vote").setExecutor(new CommandVOTE());
		getCommand("gm").setExecutor(new CommandGM());
		getCommand("heal").setExecutor(new CommandHEAL(this));
		getCommand("fly").setExecutor(new CommandFLY());
		getCommand("kill").setExecutor(new CommandKILL());
		getCommand("tp").setExecutor(new CommandTP());
		getCommand("spawn").setExecutor(new CommandSPAWN(this));
		getCommand("tphere").setExecutor(new CommandTPHERE());
		getCommand("speed").setExecutor(new CommandFLYSPEED());
		getCommand("tpall").setExecutor(new CommandTPALL());
		getCommand("walkspeed").setExecutor(new CommandWALKSPEED());
		getCommand("flyspeed").setExecutor(new CommandFLYSPEED());
		getCommand("speed").setExecutor(new CommandSPEED());
		getCommand("xp").setExecutor(new CommandXP());
		getCommand("feed").setExecutor(new CommandFEED());
		getCommand("globalmute").setExecutor(new CommandGLOBALMUTE());
		getCommand("rename").setExecutor(new CommandRENAME());
		getCommand("relore").setExecutor(new CommandRENAME());
		getCommand("ban").setExecutor(new CommandBAN());
		getCommand("kick").setExecutor(new CommandKICK());
		getCommand("warn").setExecutor(new CommandWARN());
		getCommand("warns").setExecutor(new CommandWARN());
		getCommand("unban").setExecutor(new CommandUNBAN());
		getCommand("delwarn").setExecutor(new CommandDELWARN());
		getCommand("clearlag").setExecutor(new CommandCLEARLAG(this));
		getCommand("wb").setExecutor(new CommandWB());
		getCommand("kickall").setExecutor(new CommandKICKALL(this));
		getCommand("enderchest").setExecutor(new CommandENDERCHEST());
		getCommand("clearenderchest").setExecutor(new CommandCLEARENDERCHEST());
		getCommand("clearinv").setExecutor(new CommandCLEARINV());
		getCommand("invsee").setExecutor(new CommandINVSEE());
		getCommand("kit").setExecutor(new CommandKIT());
		getCommand("more").setExecutor(new CommandMORE());
		getCommand("giveall").setExecutor(new CommandGIVEALL(this));
		getCommand("setspawn").setExecutor(new CommandSPAWN(this));
		getCommand("support").setExecutor(new CommandSUPPORT());
		getCommand("money").setExecutor(new CommandMONEY());
		getCommand("pay").setExecutor(new CommandMONEY());
		getCommand("eco").setExecutor(new CommandMONEY());
		getCommand("warp").setExecutor(new CommandWARP());
		getCommand("setwarp").setExecutor(new CommandWARP());
		getCommand("delwarp").setExecutor(new CommandWARP());
		getCommand("gmlist").setExecutor(new CommandGMLIST());
		getCommand("countdown").setExecutor(new CommandCOUNTDOWN(this));
		getCommand("help").setExecutor(new CommandBLOCKER());
		getCommand("pl").setExecutor(new CommandBLOCKER());
		getCommand("me").setExecutor(new CommandBLOCKER());
		getCommand("day").setExecutor(new CommandTIME());
		getCommand("night").setExecutor(new CommandTIME());
		getCommand("itemdb").setExecutor(new CommandITEMDB());
		getCommand("mobevent").setExecutor(new CommandMOBEVENT(this));
		getCommand("sudo").setExecutor(new CommandSUDO());
		getCommand("motd").setExecutor(new CommandMOTD());
		getCommand("tempban").setExecutor(new CommandTEMPBAN());
		getCommand("baninfo").setExecutor(new CommandTEMPBAN());
		getCommand("system").setExecutor(new CommandSYSTEM());
		getCommand("mute").setExecutor(new CommandMUTE());
		getCommand("msg").setExecutor(new CommandMSG());
		getCommand("r").setExecutor(new CommandMSG());
		getCommand("home").setExecutor(new CommandHOME(this));
		getCommand("sethome").setExecutor(new CommandHOME(this));
		getCommand("delhome").setExecutor(new CommandHOME(this));
		getCommand("spawner").setExecutor(new CommandSPAWNER());
		getCommand("spawner").setTabCompleter(new EventAUTO_COMPLETE());
		getCommand("sun").setExecutor(new CommandWEATHER());
		getCommand("rain").setExecutor(new CommandWEATHER());
		getCommand("coinshop").setExecutor(new CommandCOINSHOP(this));
		getCommand("wartung").setExecutor(new CommandWARTUNG());
		getCommand("fix").setExecutor(new CommandFIX());
		getCommand("pex").setExecutor(new CommandPEX());
		getCommand("armourrepair").setExecutor(new CommandARMOURREPAIR());
		getCommand("tab").setExecutor(new CommandTAB());
		getCommand("whois").setExecutor(new CommandWHOIS());
		getCommand("butcher").setExecutor(new CommandBUTCHER());
		getCommand("ptime").setExecutor(new CommandPTIME());
		getCommand("hat").setExecutor(new CommandHAT());
		getCommand("stackmode").setExecutor(new CommandSTACKMODE());
		getCommand("tpa").setExecutor(new CommandTPA(this));
		getCommand("tpaccept").setExecutor(new CommandTPACCEPT(this));
		getCommand("tpdeny").setExecutor(new CommandTPDENY());
		getCommand("whitelist").setExecutor(new CommandWHITELIST(this));
		getCommand("stack").setExecutor(new CommandSTACK());
		getCommand("report").setExecutor(new CommandREPORT());
		getCommand("enchant").setExecutor(new CommandENCHANT());
		getCommand("vanish").setExecutor(new CommandVANISH());
		getCommand("vorbau").setExecutor(new CommandVORBAU());
		getCommand("reload").setExecutor(new CommandRELOAD(this));
		getCommand("afk").setExecutor(new CommandAFK());

	}

	/*
	 * Alle Events werden registriert
	 */
	public void loadEvents() {
		getServer().getPluginManager().registerEvents(new EventJOIN_LEAVE(this), this);
		getServer().getPluginManager().registerEvents(new EventLOGINBANCHECK(this), this);
		getServer().getPluginManager().registerEvents(new EventANTIWOERTER(), this);
		getServer().getPluginManager().registerEvents(new EventUNBEKANNTERBEFEHL(), this);
		getServer().getPluginManager().registerEvents(new CommandSTOP(this), this);
		getServer().getPluginManager().registerEvents(new CommandVOTE(), this);
		getServer().getPluginManager().registerEvents(new CommandSPAWN(this), this);
		getServer().getPluginManager().registerEvents(new FunktionenWARP(this), this);
		getServer().getPluginManager().registerEvents(new EventJUMPPAD(), this);
		getServer().getPluginManager().registerEvents(new EventOP_APFEL(), this);
		getServer().getPluginManager().registerEvents(new UUIDCache(), this);
		getServer().getPluginManager().registerEvents(new CommandHOME(this), this);
		getServer().getPluginManager().registerEvents(new EventCHATNACHRICHTEN(), this);
		getServer().getPluginManager().registerEvents(new CommandCOINSHOP(this), this);
		getServer().getPluginManager().registerEvents(new EventFREESCHILDER(), this);
		getServer().getPluginManager().registerEvents(new EventBUGFIX(), this);
		getServer().getPluginManager().registerEvents(new CommandWHITELIST(this), this);
		getServer().getPluginManager().registerEvents(new CommandVANISH(), this);
		getServer().getPluginManager().registerEvents(new CommandAFK(), this);
		getServer().getPluginManager().registerEvents(new CommandSTACKMODE(), this);
	}
	
	/*
	 * Die Nachrichten
	 */
	
	/*
	 * Alle anderen Dinge werden erstellt/geladen
	 */
	
	private void loadUtils() {
		new FunktionenWARP(this);
		new ProtocolLib(this);
		new FunktionenAUTOMSG(this);

		UUIDCache.addUsersToUUIDCache();
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(this, ListenerPriority.HIGH, Arrays.asList(new PacketType[] { PacketType.Status.Server.OUT_SERVER_INFO }), new ListenerOptions[] { ListenerOptions.ASYNC }) {
							public void onPacketSending(PacketEvent e) {
								ProtocolLib.handleServerPingAsync(e,
										(WrappedServerPing) e.getPacket()
												.getServerPings().read(0));
							}
						});
		ProtocolLib.createModtFile();
		CommandSPAWNER.setUpList();
		FunktionenAUTOMSG.startMsg();
	}

	public void onDisable() {
		Bukkit.getScheduler().cancelTask(this.taskid);
		Bukkit.getScheduler().cancelTask(this.status);
		Bukkit.getScheduler().cancelAllTasks();
	}

	/*
	 * Alle Dateien werden erstellt
	 */
	public void CreateFiles() {
		CommandCOINSHOP.CreateDir();
		CommandCOINSHOP.CreateFile();
		CommandSPAWN.CreateFile();
		CommandMOBEVENT.CreateFile();
	}

	/*
	 * MySQL laden - f�r sp�ter
	 */
	public void registerMySQL() {
		if (!MySQL.file.exists()) {
			MySQL.cfg.set("MySQL.user", "benutzer");
			MySQL.cfg.set("MySQL.password", "passwort");
			MySQL.cfg.set("MySQL.host", "localhost");
			MySQL.cfg.set("MySQL.database", "database");
			MySQL.cfg.set("MySQL.port", "3306");
			try {
				MySQL.cfg.save(MySQL.file);
			} catch (IOException localIOException) {
			}
		}
	}
}
