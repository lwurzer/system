package de.heining21.System.Money;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.utils.UUIDCache;

public class MoneyUtils {

	public static void addUserMoney(Player p, int amount) {

		File file = new File(path, UUIDCache.getUserUUID(p) + ".yml");
		FileConfiguration pData = YamlConfiguration.loadConfiguration(file);

		int temp = pData.getInt("Money");
		temp = temp + amount;
		pData.set("Money", temp);

		try {
			pData.save(file);
		} catch (IOException e) {

		}

	}

	private static void checkOrdner() {
		File file = new File(path);
		if (!file.isDirectory()) {

			file.mkdirs();

		}
	}
	public static File getUserFile(Player p){
		File file = null;
		
		UUID uuid = UUIDCache.getUserUUID(p);
		file = new File(path, uuid + ".yml");
		
		
		return file;
	}

	public static void createNewUserFile(Player p) {
		UUIDCache.addUUIDToCache(p);
		File file = new File(path, UUIDCache.getUserUUID(p) + ".yml");
		FileConfiguration pData = YamlConfiguration.loadConfiguration(file);

		if (!file.exists()) {
			checkOrdner();
			pData.addDefault("Name", p.getName());
			pData.addDefault("Money", 500);
			pData.addDefault("Mute.isMuted", false);
			pData.addDefault("Mute.Zeit", 0);
			pData.addDefault("PvP.Kills", 0);
			pData.addDefault("PvP.Deaths", 0);
			
			pData.options().copyDefaults(true);
			try {
				pData.save(file);
				Bukkit.getLogger().info(
						"[System] Player Data: " + p.getName() + " erstellt.");
			} catch (IOException e) {

			}

		}
	}

	public static int getUserMoney(Player p) {
		File file = new File(path, UUIDCache.getUserUUID(p) + ".yml");
		FileConfiguration pData = YamlConfiguration.loadConfiguration(file);

		int Money = pData.getInt("Money");

		return Money;

	}

	public static void payToUser(Player sender, Player client, int amount) {

		File files = new File(path, UUIDCache.getUserUUID(sender) + ".yml");
		File filec = new File(path, UUIDCache.getUserUUID(client) + ".yml");
		// Sender
		FileConfiguration pData1 = YamlConfiguration.loadConfiguration(files);
		// Client
		FileConfiguration pData2 = YamlConfiguration.loadConfiguration(filec);

		if (sender.getName().equalsIgnoreCase(client.getName())) {

			sender.sendMessage(Data.p
					+ "�4Fehler: Du kannst dir selbst kein Geld zahlen.");
			return;

		}
		if (amount <= 0) {
			sender.sendMessage(Data.p + "Der Wert muss gr�sser als 0 sein");
			return;
		}

		if (!UserHasMoney(sender, amount)) {
			return;
		}
		int psender = pData1.getInt("Money");
		psender = psender - amount;
		pData1.set("Money", psender);

		int pclient = pData2.getInt("Money");
		pclient = pclient + amount;
		pData2.set("Money", pclient);

		try {
			pData1.save(files);
			pData2.save(filec);
		} catch (IOException e) {

		}

	}

	public static void resetUserMoney(Player p) {
		File file = new File(path, UUIDCache.getUserUUID(p) + ".yml");
		FileConfiguration pData = YamlConfiguration.loadConfiguration(file);

		pData.set("Money", 500);

		try {
			pData.save(file);
		} catch (IOException e) {

		}

	}

	public static void setUserMoney(Player p, int amount) {

		if (amount < 0) {
			return;
		}

		File file = new File(path, UUIDCache.getUserUUID(p) + ".yml");
		FileConfiguration pData = YamlConfiguration.loadConfiguration(file);

		pData.set("Money", amount);
		try {
			pData.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void takeUserMoney(Player p, int amount) {

		File file = new File(path, UUIDCache.getUserUUID(p) + ".yml");
		FileConfiguration pData = YamlConfiguration.loadConfiguration(file);
		if (!UserHasMoney(p, amount)) {
			return;
		}
		int temp = pData.getInt("Money");
		temp = temp - amount;
		pData.set("Money", temp);

		try {
			pData.save(file);
		} catch (IOException e) {

		}
	}

	public static boolean UserHasMoney(Player p, int amount) {

		File file = new File(path, UUIDCache.getUserUUID(p) + ".yml");
		FileConfiguration pData = YamlConfiguration.loadConfiguration(file);

		if (pData.getInt("Money") >= amount) {
			return true;
		}
		return false;
	}
	

	static String path = "plugins/System/UserData";
}