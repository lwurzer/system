package de.heining21.System.MySQL;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class MySQL extends Database {
	public static File file = new File("plugins/System", "MySQL.yml");

	public static FileConfiguration cfg = YamlConfiguration
			.loadConfiguration(file);

	public static String hostname = cfg.getString("MySQL.host");
	public static String user = cfg.getString("MySQL.user");
	public static String password = cfg.getString("MySQL.password");
	public static String port = cfg.getString("MySQL.port");
	public static String database = cfg.getString("MySQL.database");

	/**
	 * Creates a new MySQL instance
	 * 
	 * @param plugin
	 *            Plugin instance
	 * @param hostname
	 *            Name of the host
	 * @param port
	 *            Port number
	 * @param database
	 *            Database name
	 * @param username
	 *            Username
	 * @param password
	 *            Password
	 */
	public MySQL(Plugin plugin, String hostname, String port, String database,
			String username, String password) {
		super(plugin);
		MySQL.hostname = hostname;
		MySQL.port = port;
		MySQL.database = database;
		user = username;
		MySQL.password = password;
	}

	public Connection openConnection() throws SQLException,
			ClassNotFoundException {
		if (checkConnection()) {
			return connection;
		}
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mysql://" + hostname
				+ ":" + port + "/" + database, user, password);
		return connection;
	}
}
