package de.heining21.System.ServerListe;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedServerPing;

import de.heining21.System.Main;
import de.heining21.System.commands.Game.CommandWARTUNG;

public class ProtocolLib {

	static String pfad = "plugins/System";
	public static File file = new File(pfad, "modt_tab_players.yml");
	public static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

	static List<String> entrys;
	static List<WrappedGameProfile> profiles = new ArrayList<WrappedGameProfile>();

	static void checkPath(String path) {
		File check = new File(path);
		if (!check.isDirectory()) {
			check.mkdirs();
		}
	}
	
	public static void createModtFile() {
		if (!file.exists()) {
			System.out.println("[System] Motd_Tab-file wird erstellt");
			
			checkPath(pfad);
			
			ArrayList<String> texts = new ArrayList<String>();
			texts.add("&d&d&l--&5&l[ &b&lCraftPlace Netzwerk &5&l]&d&l--&r&d");
			texts.add("      &6* TS: CraftPlace.DE *");
			texts.add("&d&d&l------------------------&r&d");
			texts.add("&7CraftPlace Server Status:");
			texts.add(" - &8PvP &7(&2Online&7)");
			texts.add(" - &8FFA &7(&2Online&7)");
			
			cfg.set("MotdLine1", "&aStandard Nachricht des Plugins.");
			cfg.set("ModtLine2", "&cVon heining21, higgs_01");
			cfg.set("Tab.Header", "&aHEADER <Setzen mit /tab header>");
			cfg.set("Tab.Footer", "&aFOOTER <Setzen mit /tab footer>");
			cfg.set("Player_hover", texts);
			try {
				cfg.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void loadPlayerHoverList() {
		entrys = cfg.getStringList("Player_hover");
		try {
			for (String s : entrys) {
				WrappedGameProfile WPG = new WrappedGameProfile(UUID.randomUUID(), s.replace("&", "§"));
				profiles.add(WPG);
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	public static void handleServerPingAsync(PacketEvent e,
			WrappedServerPing ping) {
		((WrappedServerPing) e.getPacket().getServerPings().read(0)).setPlayers(profiles);
		if (CommandWARTUNG.Wartung()) {
			ping.setMotD("§c------------------§4§lWartung§r§c------------------§r\n§c          "
					+ " §6Craft§ePlace§4 befindet sich in Wartung!");
			
			ping.setVersionProtocol(1);
			ping.setVersionName("§4§lWartung!§r §70§8/§70");
		} else {
			ping.setMotD(Main.Modt1 + "\n§r" + Main.Motd2);
		}
	}

	public ProtocolLib(Main main) {
	}
}
