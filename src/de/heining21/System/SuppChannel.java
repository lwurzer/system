package de.heining21.System;

import java.util.ArrayList;

import org.bukkit.entity.Player;

public class SuppChannel {

	public static void addPlayerToChat(Player p, Player sup) {

		if (waiting.contains(p) && !waiting.isEmpty()) {

			waiting.remove(p);
			inChat.add(p);
			inChat.add(sup);

			if (c1IsFree()) {
				suppChannel1p1.add(p);
				suppChannel1p2.add(sup);
			} else if (c2IsFree()) {
				suppChannel2p1.add(p);
				suppChannel2p2.add(sup);

			} else if (c3IsFree()) {
				suppChannel3p1.add(p);
				suppChannel3p2.add(sup);

			} else if (c4IsFree()) {
				suppChannel4p1.add(p);
				suppChannel4p2.add(sup);

			} else {
				sup.sendMessage(Data.supportp
						+ "Es ist im Moment kein Channel verf�gbar");
				p.sendMessage(Data.supportp
						+ "Es ist im Moment kein Channel verf�gbar");
			}

			p.sendMessage(Data.supportp + "Du bist nun im Support-Chat mit �e" + sup.getName() + "�a.");
			p.sendMessage(Data.supportp + "In den Globalen Chat schreiben: �a@<Nachricht>");
			sup.sendMessage(Data.supportp + "Du Supportest nun �e" + p.getName() + "�a.");
			sup.sendMessage(Data.supportp + "In den Globalen Chat schreiben: �a@<Nachricht> �7�l| �r�aSupport-Chat verlassen: /Support leave");

		} else {
			return;
		}

	}

	public static void addPlayerToSup(Player p) {

		waiting.add(p);
		p.sendMessage(Data.supportp + "Du wurdest in die Warteschlange f�r den Support-Chat gesetzt. Es wird dir gleich jemand helfen.");

	}

	public static boolean c1IsFree() {

		if (suppChannel1p1.isEmpty() && suppChannel1p2.isEmpty()) {
			return true;
		} else {
			return false;
		}
		
	}

	public static boolean c2IsFree() {

		if (suppChannel2p1.isEmpty() && suppChannel2p2.isEmpty()) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean c3IsFree() {

		if (suppChannel3p1.isEmpty() && suppChannel3p2.isEmpty()) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean c4IsFree() {

		if (suppChannel4p1.isEmpty() && suppChannel4p2.isEmpty()) {
			return true;
		} else {
			return false;
		}

	}

	public static void removeSupChn(Player p) {
		if (inChat.contains(p)) {

			if (suppChannel1p1.contains(p) || suppChannel1p2.contains(p)) {

				suppChannel1p2.get(0).sendMessage(Data.supportp + "�cDer Support-Chat wurde geschlossen.");
				suppChannel1p1.get(0).sendMessage(Data.supportp + "�cDer Support-Chat wurde geschlossen.");

				inChat.remove(suppChannel1p1.get(0));
				inChat.remove(suppChannel1p2.get(0));
				suppChannel1p1.remove(0);
				suppChannel1p2.remove(0);

			}
			if (suppChannel2p1.contains(p) || suppChannel2p2.contains(p)) {

				suppChannel2p2.get(0).sendMessage(Data.supportp + "�cDer Support-Chat wurde geschlossen.");
				suppChannel2p1.get(0).sendMessage(Data.supportp + "�cDer Support-Chat wurde geschlossen.");

				inChat.remove(suppChannel2p1.get(0));
				inChat.remove(suppChannel2p2.get(0));
				suppChannel2p1.remove(0);
				suppChannel2p2.remove(0);

			}
			if (suppChannel3p1.contains(p) || suppChannel3p2.contains(p)) {

				suppChannel3p2.get(0).sendMessage(Data.supportp + "�cDer Support-Chat wurde geschlossen.");
				suppChannel3p1.get(0).sendMessage(Data.supportp + "�cDer Support-Chat wurde geschlossen.");

				inChat.remove(suppChannel3p1.get(0));
				inChat.remove(suppChannel3p2.get(0));
				suppChannel3p1.remove(0);
				suppChannel3p2.remove(0);

			}
			if (suppChannel4p1.contains(p) || suppChannel4p2.contains(p)) {

				suppChannel4p2.get(0).sendMessage(Data.supportp + "�cDer Support-Chat wurde geschlossen.");
				suppChannel4p1.get(0).sendMessage(Data.supportp + "�cDer Support-Chat wurde geschlossen.");

				inChat.remove(suppChannel4p1.get(0));
				inChat.remove(suppChannel4p2.get(0));
				suppChannel4p1.remove(0);
				suppChannel4p2.remove(0);

			}

		} else {
			return;
		}

	}

	public static ArrayList<Player> waiting = new ArrayList<Player>();
	public static ArrayList<Player> inChat = new ArrayList<Player>();
	public static ArrayList<Player> suppChannel1p1 = new ArrayList<Player>();

	public static ArrayList<Player> suppChannel1p2 = new ArrayList<Player>();

	public static ArrayList<Player> suppChannel2p1 = new ArrayList<Player>();

	public static ArrayList<Player> suppChannel2p2 = new ArrayList<Player>();

	public static ArrayList<Player> suppChannel3p1 = new ArrayList<Player>();

	public static ArrayList<Player> suppChannel3p2 = new ArrayList<Player>();

	public static ArrayList<Player> suppChannel4p1 = new ArrayList<Player>();

	public static ArrayList<Player> suppChannel4p2 = new ArrayList<Player>();
}
