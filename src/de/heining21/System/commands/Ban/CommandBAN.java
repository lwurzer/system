package de.heining21.System.commands.Ban;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.utils.UUIDCache;

public class CommandBAN implements CommandExecutor {

	public static long getBanTime(String pName) {

		if (isBanned(pName)) {
			String UUID = UUIDCache.getUserUUIDOffline(pName).toString();
			if (speere.getLong("ban." + UUID + ".zeit") == 0) {

				return 0;
			}
			long time = speere.getLong("ban." + UUID + ".zeit");
			long restTime = (time - System.currentTimeMillis()) / 1000;

			return restTime;

		} else {
			return 0;
		}

	}

	public static Boolean isBanned(String pName) {

		String UUID = UUIDCache.getUserUUIDOffline(pName).toString();
		boolean banned = false;
		if (speere.getBoolean("ban." + UUID + ".isBanned")) {
			banned = true;
			return banned;
		} else {
			return banned;
		}

	}

	public static File sperre = new File("plugins/System/Bans", "Ban.yml");

	public static FileConfiguration speere = YamlConfiguration
			.loadConfiguration(sperre);

	public boolean onCommand(final CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("ban") || cmd.getName().equalsIgnoreCase("sperre")) {
			if (sender.hasPermission("sytem.ban")) {
				if (args.length <= 1) {
					sender.sendMessage(Data.p + "�eVerwendung: �a/ban | sperre <Spieler> <Grund>");
				} else {
					StringBuilder sb = new StringBuilder();
					sb.append(args[1]);
					for (int i = 2; i < args.length; i++) {

						sb.append(args[i]);
						sb.append(" ");
					}
					String grund = sb.toString();
					if (Bukkit.getPlayer(args[0]) != null) {
						Player p2 = Bukkit.getPlayer(args[0]);
						if (sender == Bukkit.getPlayer(args[0])) {
							sender.sendMessage(Data.p
									+ "�cDu kannst dich nicht selbst bannen!");
						} else if (args[0].equalsIgnoreCase("heining21")
								|| args[0].equalsIgnoreCase("higgs_01")) {
							sender.sendMessage(Data.pfett
									+ "�4�lDu darfst die Programmierer (heining21, higgs_01) nicht bannen.");
						} else {
							if (p2.hasPermission("system.ban.ignore")) {
								sender.sendMessage(Data.p
										+ "�cDu kannst diesen Spieler nicht bannen, weil er Ban-Ignore-Rechte hat!");
								return true;
							}
							String UUID = UUIDCache.getUserUUIDOffline(args[0])
									.toString();
							speere.set("ban." + UUID + ".grund", grund);
							speere.set("ban." + UUID + ".name", args[0]);
							speere.set("ban." + UUID + ".zeit", "0");
							speere.set("ban." + UUID + ".isBanned", true);
							try {
								speere.save(sperre);
							} catch (IOException e) {
								e.printStackTrace();
							}
							Bukkit.getPlayer(args[0])
									.kickPlayer(
											"�4Du wurdest permanent Gebannt! �9Grund: "
													+ ChatColor
															.translateAlternateColorCodes(
																	'&',
																	new StringBuilder()
																			.append(ChatColor.GOLD)
																			.append(grund)
																			.toString()));
							Bukkit.broadcastMessage(Data.p + "�9Der Spieler �c"
									+ args[0] + " �9wurde von �1"
									+ sender.getName()
									+ " �9Permanent Gebannt!");
							Bukkit.broadcastMessage("�9Grund: "
									+ ChatColor
											.translateAlternateColorCodes(
													'&',
													new StringBuilder()
															.append(ChatColor.DARK_AQUA)
															.append(grund)
															.toString()));
						}
					} else {
						if (args[0].equalsIgnoreCase("heining21")
								|| args[0].equalsIgnoreCase("higgs_01")) {
							sender.sendMessage(Data.pfett
									+ "�4�lDu darfst die Programmierer (heining21, higgs_01) nicht bannen.");
							return true;
						}
						speere.set("ban." + args[0], grund);
						try {
							speere.save(sperre);
						} catch (IOException e) {
							e.printStackTrace();
						}
						Bukkit.broadcastMessage(Data.p + "�9Der Spieler �c"
								+ args[0] + " �9wurde von �1"
								+ sender.getName() + " �9Permanent Gebannt!");
						sender.sendMessage("�9Grund: "
								+ ChatColor.translateAlternateColorCodes(
										'&',
										new StringBuilder()
												.append(ChatColor.DARK_AQUA)
												.append(grund).toString()));
					}
				}
			} else {
				sender.sendMessage(Data.KeineRechte);
			}
		}
		return true;
	}
}
