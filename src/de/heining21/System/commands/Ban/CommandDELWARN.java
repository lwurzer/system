package de.heining21.System.commands.Ban;

import java.io.IOException;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.heining21.System.Data;
import de.heining21.System.utils.UUIDCache;

public class CommandDELWARN implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd,
			String cmdLabel, String[] args) {
		if (sender.hasPermission("system.delwarn")) {
			if (args.length < 2) {
				sender.sendMessage(Data.p + "�eVerwendung: �a/Delwarn | Cwarn <Spieler> <Anzahl:All>");
			} else if (args.length == 2) {
				if (args[1].equalsIgnoreCase("all")) {
					UUID uuid = UUIDCache.getUserUUIDOffline(args[0]);
					int warns = CommandWARN.warnings.getInt("warns." + uuid);
					if (warns == 0) {
						sender.sendMessage("�cFehler: Der Spieler �4" + args[0]
								+ " �chat keine Warns.");
					} else {
						CommandWARN.warnings.set("warns." + uuid,
								Integer.valueOf(0));
						try {
							CommandWARN.warnings.save(CommandWARN.warn);
						} catch (IOException e2) {
							e2.printStackTrace();
						}
						sender.sendMessage(Data.p + "�9Du hast dem Spieler �c"
								+ args[0] + " �9alle �cWarns �agel�scht");
					}
				} else {
					UUID uuid = UUIDCache.getUserUUIDOffline(args[0]);
					int anzahl = 0;
					try {
						anzahl = Integer.parseInt(args[1]);
					} catch (NumberFormatException e3) {
						sender.sendMessage("�cFehler: �4" + args[1]
								+ " �cist keine Zahl");
						return true;
					}
					int warns = CommandWARN.warnings.getInt("warns." + uuid);
					if (warns == 0) {
						sender.sendMessage("�cFehler: Der Spieler �4" + args[0]
								+ " �chat keine �cWarns");
					} else if (anzahl < 0) {
						sender.sendMessage("�cFehler: Du kannst diesem Spieler keine Warns abziehen");
					} else if (anzahl > warns) {
						sender.sendMessage(Data.p + "�9Der Spieler �c"
								+ args[0] + " �9hat nur �4" + warns
								+ " �9Warns.");
					} else {
						warns -= anzahl;
						CommandWARN.warnings.set("warns." + uuid,
								Integer.valueOf(warns));
						try {
							CommandWARN.warnings.save(CommandWARN.warn);
						} catch (IOException e4) {
							e4.printStackTrace();
						}
						sender.sendMessage(Data.p + "�9Du hast dem Spieler �c"
								+ args[0] + " �4" + anzahl
								+ " �9Warn(s) gel�scht");
					}
				}
			} else {
				sender.sendMessage(Data.KeineRechte);
			}
		}
		return true;
	}
}
