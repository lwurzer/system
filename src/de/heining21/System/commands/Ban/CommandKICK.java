package de.heining21.System.commands.Ban;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandKICK implements CommandExecutor {

	private String reason = "Insult!";

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("kick")
				|| cmd.getName().equalsIgnoreCase("kicken")) {
			if (sender.hasPermission("system.kick")) {
				if (args.length >= 2) {
					Player opfer = Bukkit.getPlayer(args[0]);
					reason = "";
					for (int i = 1; i < args.length; i++) {
						reason = reason + " " + args[i];
					}
					reason = ChatColor.translateAlternateColorCodes('&', reason);
					if (opfer != null) {
						if (opfer.hasPermission("system.kick.ignore")) {
							sender.sendMessage("�cFehler: Du darfst niemanden vom Team kicken!");
						} else {
							opfer.kickPlayer(Data.p + "�9Du wurdest von �1"
									+ sender.getName()
									+ " �9Gekickt! �9Grund:�c " + reason);
							Bukkit.broadcastMessage(Data.p + "�c "
									+ opfer.getName() + "�9 wurde von�1 "
									+ sender.getName() + " �9Gekickt.");
							Bukkit.broadcastMessage("�9Grund:�1 " + reason);
						}
					} else {
						sender.sendMessage(Data.p + "�cDieser Spieler ist nicht Online!");
					}
				} else {
					sender.sendMessage(Data.p + "�eVerwendung: �a/kick | kicken <Spieler> <Grund>");
				}
			} else {
				sender.sendMessage(Data.KeineRechte);
			}
		}
		return true;
	}
}