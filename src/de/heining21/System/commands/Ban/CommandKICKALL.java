package de.heining21.System.commands.Ban;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandKICKALL implements CommandExecutor, Listener {
	public Main plugin;

	public CommandKICKALL(Main plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	public boolean onCommand(final CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cs instanceof Player) {
			if (cmd.getName().equalsIgnoreCase("kickall")
					&& cs.hasPermission("System.kickall")) {
				Bukkit.broadcastMessage(Data.pfett
						+ "�e�lAlle Spieler werden in �5�l20 �e�lSekunden vom Server ausgeloggt!");
				playSound();
				Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin,
						new Runnable() {
							public void run() {
								Bukkit.broadcastMessage(Data.pfett
										+ "�e�lAlle Spieler werden in �5�l10 �e�lSekunden vom Server ausgeloggt!");
								playSound();
								Bukkit.getScheduler().scheduleAsyncDelayedTask(
										plugin, new Runnable() {
											public void run() {
												Bukkit.broadcastMessage(Data.pfett
														+ "�e�lAlle Spieler werden in �5�l5 �e�lSekunden vom Server ausgeloggt!");
												playSound();
												Bukkit.getScheduler()
														.scheduleAsyncDelayedTask(
																plugin,
																new Runnable() {
																	public void run() {
																		Bukkit.broadcastMessage(Data.pfett
																				+ "�e�lAlle Spieler werden in �5�l3�e�l Sekunden vom Server ausgeloggt!");
																		playSound();
																		Bukkit.getScheduler()
																				.scheduleAsyncDelayedTask(
																						plugin,
																						new Runnable() {
																							public void run() {
																								Bukkit.broadcastMessage(Data.pfett
																										+ "�e�lAlle Spieler werden in �5�l2�e�l Sekunden vom Server ausgeloggt!");
																								playSound();
																								Bukkit.getScheduler()
																										.scheduleAsyncDelayedTask(
																												plugin,
																												new Runnable() {
																													public void run() {
																														Bukkit.broadcastMessage(Data.pfett
																																+ "�e�lAlle Spieler werden in �5�l1�e�l Sekunden vom Server ausgeloggt!");
																														playSound();
																														Bukkit.getScheduler()
																																.scheduleSyncDelayedTask(
																																		plugin,
																																		new Runnable() {
																																			public void run() {
																																				for (Player all : plugin
																																						.getServer()
																																						.getOnlinePlayers()) {
																																					all.kickPlayer(Data.pfett
																																							+ "�e�lAlle Spieler wurden vom Server ausgeloggt! �c�lVersuche gleich wieder zu joinen!");
																																					Bukkit.dispatchCommand(
																																							Bukkit.getConsoleSender(),
																																							"whitelist on");
																																				}
																																			}
																																		},
																																		30L);
																													}
																												},
																												50L);
																							}
																						},
																						70L);
																	}
																}, 80L);
											}
										}, 100L);
							}
						}, 200L);
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}

	public void playSound() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.playSound(p.getLocation(), Sound.DIG_STONE, 50F, 1F);
		}
	}
}
