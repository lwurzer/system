package de.heining21.System.commands.Ban;

import java.io.IOException;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.heining21.System.Data;
import de.heining21.System.utils.UUIDCache;
import de.heining21.System.utils.UtilARRAYStoSTRING;
import de.heining21.System.utils.UtilBROADCAST;

public class CommandTEMPBAN implements CommandExecutor {

	public static void tempBanPlayer(String pName, int minutes, String grund,
			CommandSender cs, String Zeit) {
		// Current Time
		Date start = new Date(System.currentTimeMillis());

		// Time when ban ends
		long end = System.currentTimeMillis() + 1000 * 60 * minutes;

		grund = grund.replace("&", "�");
		start.getTime();
		

		// Time left in format
		
		
		

		String UUID = UUIDCache.getUserUUIDOffline(pName).toString();

		CommandBAN.speere.set("ban." + UUID + ".grund", grund);
		CommandBAN.speere.set("ban." + UUID + ".zeit", end);
		CommandBAN.speere.set("ban." + UUID + ".name", pName);
		CommandBAN.speere.set("ban." + UUID + ".isBanned", true);

		try {
			CommandBAN.speere.save(CommandBAN.sperre);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (Bukkit.getPlayerExact(pName) != null) {
			Bukkit.getPlayerExact(pName).kickPlayer(
					ChatColor.RED + "Du wurdest wegen �5'" + grund
							+ "'�r f�r �5" + Zeit + "�r gebannt.");
			UtilBROADCAST.broadCastMessage(Data.p + "�5" + pName
					+ "�r wurde wegen �5'" + grund + "'�r f�r �5" + Zeit
					+ "�r gebannt");
		}
		cs.sendMessage(Data.p + "�5" + pName + "�r wurde gebannt!");
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("tempban")) {

			if (!cs.hasPermission("System.tempban")) {

				cs.sendMessage(Data.KeineRechte);
				return true;

			}
			if (args.length < 3) {

				cs.sendMessage(Data.ZuWenigArgs);
				return true;
			}

			String Name = args[0].toLowerCase();

			String time = args[1];
			if (time.endsWith("m") || time.endsWith("h") || time.endsWith("d")) {
				
				String unit = time.substring(time.length() - 1);
				int value = Integer.parseInt(time.substring(0,
						time.length() - 1));
				
				if (unit.equals("m")) {

					String Zeit = value + " Minuten";

					tempBanPlayer(Name, value,
							UtilARRAYStoSTRING.arrayToString(args, 2), cs, Zeit);
					return true;

				}
				if (unit.equals("h")) {
					String Zeit = value + " Stunden";
					tempBanPlayer(Name, value * 60,
							UtilARRAYStoSTRING.arrayToString(args, 2), cs, Zeit);
					return true;
				}
				if (unit.equals("d")) {
					String Zeit = value + " Tage";
					tempBanPlayer(Name, value * 60 * 24,
							UtilARRAYStoSTRING.arrayToString(args, 2), cs, Zeit);
					return true;
				}

			} else {
				cs.sendMessage(Data.p + "�cBitte gebe eine g�ltige Zeiteinheit an: (1m / 1h / 1d)");
				return true;
			}

		} else if (cmd.getName().equalsIgnoreCase("baninfo")) {

			if (!cs.hasPermission("System.ban.info")) {
				cs.sendMessage(Data.KeineRechte);
				return true;
			}
			if (args.length < 1) {

				cs.sendMessage(Data.ZuWenigArgs);
				return true;
			} else {

				boolean isBanned = CommandBAN.isBanned(args[0]);

				if (!isBanned) {

					cs.sendMessage("�7----------�5Ban-Info f�r " + args[0]
							+ "�7----------");
					cs.sendMessage("�7Gebannt: �5NEIN");
					cs.sendMessage("�7----------�5Ban-Info f�r " + args[0]
							+ "�7----------");
					return true;

				}

				String grund = CommandBAN.speere.getString(
						"ban." + args[0] + ".grund").replace("&", "�");
				int resttime = (int) CommandBAN.getBanTime(args[0]);

				int hr = resttime / 3600;
				int rem = resttime % 3600;
				int mn = rem / 60;
				int sec = rem % 60;
				String hrStr = (hr < 10 ? "0" : "") + hr;
				String mnStr = (mn < 10 ? "0" : "") + mn;
				String secStr = (sec < 10 ? "0" : "") + sec;

				String time;

				if (resttime <= 0) {

					time = "�4permanent";

				} else {
					time = hrStr + ":" + mnStr + ":" + secStr;
				}

				cs.sendMessage("�7----------�5Ban-Info f�r " + args[0]
						+ "�7----------");
				cs.sendMessage("�7Gebannt: �5JA");
				cs.sendMessage("�7Zeit: �5" + time);
				cs.sendMessage("�7Grund: �5" + grund);
				cs.sendMessage("�7----------�5Ban-Info f�r " + args[0]
						+ "�7----------");

				return true;

			}

		}

		return false;
	}

}
