package de.heining21.System.commands.Ban;

import java.util.Date;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.heining21.System.Data;
import de.heining21.System.utils.UtilARRAYStoSTRING;

public class CommandTimeBan implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("tempban")) {

			new Date();

			if (!cs.hasPermission("System.tempban")) {
				cs.sendMessage(Data.KeineRechte);
				return true;
			}
			if (args.length < 3) {
				cs.sendMessage(Data.ZuWenigArgs);
				return true;
			}
			UtilARRAYStoSTRING.arrayToString(args, 2);

		}

		return false;
	}

}
