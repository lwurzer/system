package de.heining21.System.commands.Ban;

import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.heining21.System.Data;
import de.heining21.System.utils.UUIDCache;

public class CommandUNBAN implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender.hasPermission("system.unban")) {
			if (args.length == 0) {
				sender.sendMessage("�eVerwendung: �a/Unban | DelSpeere | Pardon | DelBan <Spieler>");
			} else if (args.length == 1) {
				String UUID = UUIDCache.getUserUUIDOffline(args[0]).toString();
				if (CommandBAN.speere.contains("ban." + UUID)) {
					CommandBAN.speere.set("ban." + UUID, null);
					CommandBAN.speere.set("ban." + UUID + ".zeit", null);
					try {
						CommandBAN.speere.save(CommandBAN.sperre);
					} catch (IOException e) {
						e.printStackTrace();
					}
					sender.sendMessage(Data.p + "�eDu hast den Spieler �c"
							+ args[0] + " �eentbannt!");
				} else {
					sender.sendMessage(Data.p + "�cDer Spieler ist nicht gebannt!");
				}
			}
		} else {
			sender.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}
