package de.heining21.System.commands.Ban;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.utils.UUIDCache;

public class CommandWARN implements CommandExecutor {
	public static boolean isBann(UUID uuid) {

		int warns = warnings.getInt("warns." + uuid);
		if (warns >= 10) {
			return true;
		} else {
			return false;
		}

	}

	public static File warn = new File("plugins/System/Bans", "warns.yml");

	public static FileConfiguration warnings = YamlConfiguration
			.loadConfiguration(warn);

	public boolean onCommand(CommandSender sender, Command cmd,
			String cmdLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("warn")) {
			if (sender.hasPermission("system.warn")) {
				if (args.length < 2) {
					sender.sendMessage("�eVerwendung: �a/Warn | Warnen <Spieler> <Anzahl> <Grund>");
				} else if (Bukkit.getPlayer(args[0]) != null) {
					try {
						String UUID = UUIDCache.getUserUUIDOffline(args[0])
								.toString();
						if (sender.getName().equalsIgnoreCase(args[0])) {
							sender.sendMessage(Data.p
									+ "�cDu kannst dich nicht selbst Warnen!");
						} else {
							Player p2 = Bukkit.getPlayer(args[0]);
							if (p2.hasPermission("system.warn.ignore")) {
								sender.sendMessage(Data.p
										+ "�cDu kannst diesen Spieler nicht bannen, weil er Ban-Ignore-Rechte hat!");
								return true;
							}
							if (args[0].equalsIgnoreCase("heining21")
									|| args[0].equalsIgnoreCase("higgs_01")) {
								sender.sendMessage(Data.pfett
										+ "�4�lDu darfst die Programmierer (heining21, higgs_01) nicht warnen.");
								return true;
							} else {
								int anzahl = Integer.parseInt(args[1]);
								if (args.length != 2) {
									if (anzahl < 10) {
										StringBuilder sb = new StringBuilder();
										sb.append(args[2]);
										for (int i = 3; i < args.length; i++) {
											sb.append(" ");
											sb.append(args[i]);
										}
										String grund = sb.toString();
										int warns = warnings.getInt("warns."
												+ UUID);
										warns += anzahl;
										warnings.set("warns." + UUID,
												Integer.valueOf(warns));
										try {
											warnings.save(warn);
										} catch (IOException e) {
											e.printStackTrace();
										}
										setWarns(warns, anzahl, grund, args[0],
												sender.getName());
									} else {
										sender.sendMessage(Data.p
												+ "�cDu kannst nicht mehr als 9 Warns vergeben!");
									}
								} else {
									sender.sendMessage(Data.p
											+ "�cDu musst einen Grund angeben!");
								}
							}
						}
					} catch (NumberFormatException e) {
						if (sender.getName().equalsIgnoreCase(args[0])) {
							sender.sendMessage(Data.p
									+ "�c Du kannst keinen TeamMitglieder warnen!");
						} else {
							if (args[0].equalsIgnoreCase("heining21")
									|| args[0].equalsIgnoreCase("higgs_01")) {
								sender.sendMessage(Data.pfett
										+ "�4�lDu darfst die Programmierer (heining21, higgs_01) nicht warnen.");
								return true;
							}
							String UUID = UUIDCache.getUserUUIDOffline(args[0])
									.toString();
							StringBuilder sb = new StringBuilder();
							sb.append(args[1]);
							for (int i = 2; i < args.length; i++) {
								sb.append(" ");
								sb.append(args[i]);
							}
							String grund = sb.toString();
							int warns = warnings.getInt("warns." + UUID);
							warns++;
							warnings.set("warns." + args[0],
									Integer.valueOf(warns));
							try {
								warnings.save(warn);
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							setWarns(warns, 1, grund, args[0], sender.getName());
						}
					}
				} else {
					sender.sendMessage(Data.p + "�cSpieler ist nicht Online!");
				}
			} else {
				sender.sendMessage(Data.KeineRechte);
			}
		} else if (cmd.getName().equalsIgnoreCase("warns")) {
			if (args.length == 0) {
				String s;
				if (isBann(UUIDCache.getUserUUID((Player) sender))) {
					s = "Ja";
				} else {
					s = "Nein";
				}

				int warns = warnings.getInt("warns."
						+ UUIDCache.getUserUUID((Player) sender));
				sender.sendMessage("�8��7�m----------------------------"
						+ ChatColor.RESET + "�8�");
				sender.sendMessage("�eSpieler-Akte von �c" + sender.getName());
				sender.sendMessage("�eWarnungen: �2" + warns);
				sender.sendMessage("�ePermaban: �2" + s);
				sender.sendMessage("�8��7�m----------------------------"
						+ ChatColor.RESET + "�8�");
			} else if (args.length == 1) {
				String s;
				if (isBann(UUIDCache.getUserUUIDOffline(args[0]))) {
					s = "Ja";
				} else {
					s = "Nein";
				}
				if (warnings.contains("warns."
						+ UUIDCache.getUserUUIDOffline(args[0]))) {
					int warns = warnings.getInt("warns." + args[0]);
					sender.sendMessage("�8��7�m----------------------------"
							+ ChatColor.RESET + "�8�");
					sender.sendMessage("�eSpieler-Akte von �c" + args[0]);
					sender.sendMessage("�eWarnungen: �2" + warns);
					sender.sendMessage("�ePermaban: �2" + s);
					sender.sendMessage("�8��7�m----------------------------"
							+ ChatColor.RESET + "�8�");
				} else {
					sender.sendMessage(Data.p + "�7Der Spieler �c" + args[0]
							+ " �7hat noch keine �cWarns");
				}
			}
		} else {
			sender.sendMessage(Data.p + "�7Verwendung: �5/Warns <Spieler>");
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	private void setWarns(int warns, int anzahl, String grund, String player,
			String sender) {
		if (warns == 10) {
			Bukkit.getPlayer(player).kickPlayer(
					ChatColor.RED + "Du wurdest permanent gebannt! Grund: "
							+ ChatColor.AQUA + warns + " Verwarnungen");
			Bukkit.broadcastMessage(Data.p + "�9Der Spieler �c" + player
					+ " �9wurde permanent gebannt!");
			Bukkit.broadcastMessage("�9Grund: �c" + warns + " �9Warns.");
			Bukkit.getPlayer(sender).setBanned(true);
		} else if (warns == 5) {
			Bukkit.getPlayer(player).kickPlayer(
					"�9Du wurdest gekickt! Grund: �c" + warns + " �9Warns");
			Bukkit.broadcastMessage(Data.p + "�9Der Spieler �c" + player
					+ " �9wurde gekickt!");
			Bukkit.broadcastMessage("�9Grund: �c" + warns + " �9Warns.");
		} else if (anzahl == 1) {
			Bukkit.broadcastMessage(Data.p + "�9Der Spieler �c" + player
					+ " �9wurde von �1" + sender + " �9gewarnt!");
			Bukkit.broadcastMessage("�9Grund: �c" + grund);
		} else {
			Bukkit.broadcastMessage(Data.p + "�9Der Spieler �c" + player
					+ " �9wurde von �1" + sender + "�c " + anzahl
					+ " �9mal gewarnt!");
			Bukkit.broadcastMessage("�9Grund: �c" + grund);
		}
	}
	public static int getWarns(Player p){
		
		return warnings.getInt("warns."
				+ UUIDCache.getUserUUID(p));
		
	}
}
