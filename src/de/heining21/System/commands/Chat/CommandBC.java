package de.heining21.System.commands.Chat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandBC implements CommandExecutor {
	public Main plugin;

	public CommandBC(Main plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (args.length == 0) {
			cs.sendMessage("�eVerwendung: �a/bc <Nachricht>");
		}
		if (cs.hasPermission("System.bc")) {
			if (args.length != 0) {
				String message = "";
				for (String arg : args) {
					message = message + " " + arg;
					message = message.replace('&', '�');
				}
				for (Player all : plugin.getServer().getOnlinePlayers()) {
					all.sendMessage(cs.getName().replace(cs.getName(), "�7�l[�6�lCraft�5�lPlace�7�l]�r�e " + message));
				}
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}
