package de.heining21.System.commands.Chat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.heining21.Helfer.TitleUtils;
import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandCOINSHOP implements CommandExecutor, Listener {
	
	public static void CreateDir() {
		File check = new File(path);
		if (!check.isDirectory()) {
			check.mkdirs();
		}
	}

	public static void CreateFile() {
		try {
			CreateDir();
			file.createNewFile();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
	public static String path = "plugins/System/Coinshop";

	static File file = new File(path, "K�ufe.yml");
	static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

	public Main plugin;
	public CommandCOINSHOP(Main plugin) {
		this.plugin = plugin;
	}
	
	public Inventory inv�bersicht = null;
	public Inventory invr�nge = null;
	public Inventory invitems = null;
	public Inventory invmaster = null;
	public Inventory invlegende = null;
	
	final Calendar c = Calendar.getInstance();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Player p = (Player) cs;
		
		if (cmd.getName().equalsIgnoreCase("coinshop")) {
			
			if (args.length == 0) {
			
				inv�bersicht = p.getPlayer().getServer().createInventory(null, 9, "�a�lCoinShop");
				
				ItemStack r�nge = new ItemStack(Material.DIAMOND, 1);
				ItemMeta metar�nge = r�nge.getItemMeta();
				metar�nge.setDisplayName("�2�lR�nge");
				ArrayList r�ngearray = new ArrayList();
				r�ngearray.add("�cVerf�gbar:");
				r�ngearray.add("- �eMaster");
				r�ngearray.add("- �eLegende");
				metar�nge.setLore(r�ngearray);
				r�nge.setItemMeta(metar�nge);
				
				ItemStack items = new ItemStack(Material.CHEST, 1);
				ItemMeta metaitems = items.getItemMeta();
				metaitems.setDisplayName("�5�lItems");
				ArrayList itemsarray = new ArrayList();
				itemsarray.add("�cVerf�gbar:");
				itemsarray.add("- �eFullSchwert");
				itemsarray.add("- �eFullR�stung");
				itemsarray.add("- �eFullSpitzhacke");
				itemsarray.add("- �eFullSchaufel");
				itemsarray.add("- �eFullAxt");
				metaitems.setLore(itemsarray);
				items.setItemMeta(metaitems);
				
				ItemStack zur�ck = new ItemStack(Material.RED_ROSE);
				ItemMeta metazur�ck = zur�ck.getItemMeta();
				metazur�ck.setDisplayName("�c�lZur�ck");
				zur�ck.setItemMeta(metazur�ck);
				
				ItemStack coins = new ItemStack(Material.GOLD_BLOCK, 1, (short) 3);
				ItemMeta metacoins = coins.getItemMeta();
				metacoins.setDisplayName("�4�lCoins");
				coins.setItemMeta(metacoins);
				
				inv�bersicht.setItem(0, r�nge);
				inv�bersicht.setItem(1, items);
				inv�bersicht.setItem(8, zur�ck);
				inv�bersicht.setItem(5, coins);
				
				p.openInventory(inv�bersicht);
			
			} else if (args[0].equalsIgnoreCase("r�nge")) {
				
				invr�nge = p.getPlayer().getServer().createInventory(null, 9, "�2�lR�nge");
				
				ItemStack master = new ItemStack(Material.GOLD_BLOCK, 1);
				ItemMeta metamaster = master.getItemMeta();
				metamaster.setDisplayName("�7�l[�e�lMaster�7�l] �r�f" + p.getName());
				master.setItemMeta(metamaster);
				ArrayList masterarray = new ArrayList();
				masterarray.add("�a�lVerf�gbar");
				masterarray.add("�bKlick f�r Infos und Kauf");
				metamaster.setLore(masterarray);
				master.setItemMeta(metamaster);
				
				ItemStack legende = new ItemStack(Material.DIAMOND_BLOCK, 1);
				ItemMeta metalegende = legende.getItemMeta();
				metalegende.setDisplayName("�7�l[�a�lLegende�7�l] �r�f" + p.getName());
				ArrayList legendearray = new ArrayList();
				legendearray.add("�a�lVerf�gbar");
				legendearray.add("�bKlick f�r Infos und Kauf");
				metalegende.setLore(legendearray);
				legende.setItemMeta(metalegende);
				
				ItemStack zur�ck = new ItemStack(Material.RED_ROSE);
				ItemMeta metazur�ck = zur�ck.getItemMeta();
				metazur�ck.setDisplayName("�c�lZur�ck");
				zur�ck.setItemMeta(metazur�ck);
				
				invr�nge.setItem(0, master);
				invr�nge.setItem(2, legende);
				invr�nge.setItem(8, zur�ck);
				
				p.openInventory(invr�nge);
				
			
		} else if (args[0].equalsIgnoreCase("master")) {
			
			invmaster = p.getPlayer().getServer().createInventory(null, 9, "�2�lMaster");
			
			ItemStack master1monat = new ItemStack(Material.ARROW, 1);
			ItemMeta metamaster1monat = master1monat.getItemMeta();
			metamaster1monat.setDisplayName("�7�l[�e�lMaster�7�l] �bf�r 1 Monat");
			master1monat.setItemMeta(metamaster1monat);
			
			ItemStack master2monat = new ItemStack(Material.STICK, 1);
			ItemMeta metamaster2monat = master2monat.getItemMeta();
			metamaster2monat.setDisplayName("�7�l[�e�lMaster�7�l] �bf�r 6 Monate");
			master2monat.setItemMeta(metamaster2monat);
			
			ItemStack master3monat = new ItemStack(Material.STONE_SWORD, 1);
			ItemMeta metamaster3monat = master3monat.getItemMeta();
			metamaster3monat.setDisplayName("�7�l[�e�lMaster�7�l] �bf�r 1 Jahr");
			master3monat.setItemMeta(metamaster3monat);
			
			ItemStack masterinfo = new ItemStack(Material.TORCH, 1);
			ItemMeta metamasterinfo = masterinfo.getItemMeta();
			metamasterinfo.setDisplayName("�7�l[�e�lMaster�7�l] �cInformation");
			masterinfo.setItemMeta(metamasterinfo);
			
			ItemStack zur�ck = new ItemStack(Material.RED_ROSE);
			ItemMeta metazur�ck = zur�ck.getItemMeta();
			metazur�ck.setDisplayName("�c�lZur�ck");
			zur�ck.setItemMeta(metazur�ck);
			
			invmaster.setItem(0, master1monat);
			invmaster.setItem(1, master2monat);
			invmaster.setItem(2, master3monat);
			invmaster.setItem(6, masterinfo);
			invmaster.setItem(8, zur�ck);
			
			p.openInventory(invmaster);
			
		}
			
		}
		return true;
	}
	
	@EventHandler
	public void onKlick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		
		if (e.getInventory().getTitle().contains(ChatColor.stripColor("CoinShop"))) {
			e.setCancelled(true);
			
			if (e.getCurrentItem().getType() == Material.DIAMOND) {
				p.closeInventory();
				Bukkit.dispatchCommand(p, "coinshop r�nge");
				
			} else if (e.getCurrentItem().getType() == Material.CHEST) {
				p.closeInventory();
				Bukkit.dispatchCommand(p, "coinshop items");
				
			} else if (e.getCurrentItem().getType() == Material.RED_ROSE) {
				p.closeInventory();
				
			} else if (e.getCurrentItem().getType() == Material.SKULL_ITEM) {
				p.closeInventory();
				Bukkit.dispatchCommand(p, "coinshop coins");
				
			}
		}
		
		if (e.getInventory().getTitle().contains(ChatColor.stripColor("R�nge"))) {
			e.setCancelled(true);
			
			if (e.getCurrentItem().getType() == Material.GOLD_BLOCK) {
				p.closeInventory();
				Bukkit.dispatchCommand(p, "coinshop master");
				
			} else if (e.getCurrentItem().getType() == Material.DIAMOND_BLOCK) {
				p.closeInventory();
				Bukkit.dispatchCommand(p, "coinshop legende");
				
			} else if (e.getCurrentItem().getType() == Material.RED_ROSE) {
				p.closeInventory();
				Bukkit.dispatchCommand(p, "coinshop");
			}
		}
		
		if (e.getInventory().getTitle().contains(ChatColor.stripColor("Master"))) {
			e.setCancelled(true);
			
			if (e.getCurrentItem().getType() == Material.ARROW) {
				p.closeInventory();
				
				
				
				p.sendMessage(Data.pfett + "�c�lDanke f�r deinen Einkauf. Du besitzt jetzt f�r 1 Monat �4�lMASTER�c�l.");
				TitleUtils.sendTimings(p, 20, 150, 20);
				TitleUtils.sendTitle(p, "�cEinkauf wurde get�tigt");
				TitleUtils.sendSubTitle(p, "�7(Rang im TS => /SKYPE)");
				
			} else if (e.getCurrentItem().getType() == Material.STICK) {
				p.closeInventory();
				Bukkit.dispatchCommand(p, "coinshop legende");
				
			} else if (e.getCurrentItem().getType() == Material.RED_ROSE) {
				p.closeInventory();
				Bukkit.dispatchCommand(p, "coinshop");
			}
		}
		
	}
}
