package de.heining21.System.commands.Chat;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Main;
import de.heining21.System.utils.UtilBROADCAST;

public class CommandCOUNTDOWN implements CommandExecutor {
	
	private Main plugin;

	public CommandCOUNTDOWN(Main main){
		this.plugin = main;
	}
	int time;

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("countdown") && cs.hasPermission("System.countdown")) {
			final Player p = (Player) cs;
			if (args.length == 0) {
				p.sendMessage(Data.p
						+ "�7Verwendung: �5/countdown | cd <10-1800> �8(Angaben in Sekunden)");
				p.sendMessage("�5�lINFO:");
				p.sendMessage("60 = 1 Min");
				p.sendMessage("300 = 5 Min");
				p.sendMessage("600 = 10 Min");
				p.sendMessage("900 = 15 Min");
				p.sendMessage("1200 = 20 Min");
				p.sendMessage("1500 = 25 Min");
				p.sendMessage("1800 = 30 Min");
			}

			if (args.length == 1) {
				try{
					time = Integer.valueOf(args[0]);
					
					Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
						
						public void run() {
							
							time--;
							if(time == 0){
								
								UtilBROADCAST.broadCastMessage(Data.p + "Der Countdown ist abgelaufen");
								for(Player pOnline : Bukkit.getOnlinePlayers()){
									
									pOnline.playSound(p.getLocation(), Sound.EXPLODE, 2, 2);
									
								}
								
							}
							if(time == 1800){
								
								UtilBROADCAST.broadCastMessage(Data.p + "Es bleiben noch �630 Minuten�r.");
								
							}else
							if(time == 1200){
								
								UtilBROADCAST.broadCastMessage(Data.p + "Es bleiben noch �610 Minuten�r.");
								
							}else
							if(time == 600){
								UtilBROADCAST.broadCastMessage(Data.p + "Es bleiben noch �610 Minuten�r.");
							}else
							if(time == 300){
								UtilBROADCAST.broadCastMessage(Data.p + "Es bleiben noch �65 Minuten�r.");
							}else
							if(time == 60){
								UtilBROADCAST.broadCastMessage(Data.p + "Es bleibt noch �61 Minute�r.");
							}else
							if(time == 30){
								
								UtilBROADCAST.broadCastMessage(Data.p + "Es bleiben noch �630 Sekunden�r.");
							}
							if(time <= 10){
								
								if(time == 10){
									
									UtilBROADCAST.broadCastMessage(Data.p + "Es bleiben noch �610 Sekunden�r.");
									
								}else
								if(time == 5){
									UtilBROADCAST.broadCastMessage(Data.p + "Es bleiben noch �65 Sekunden�r.");
								}else
									if(time == 4){
										UtilBROADCAST.broadCastMessage(Data.p + "Es bleiben noch �64 Sekunden�r.");
									}
								if(time == 3){
									UtilBROADCAST.broadCastMessage(Data.p + "Es bleiben noch �63 Sekunden�r.");
								}
								if(time == 2){
									UtilBROADCAST.broadCastMessage(Data.p + "Es bleiben noch �62 Sekunden�r.");
								}
								if(time == 1){
									UtilBROADCAST.broadCastMessage(Data.p + "Es bleibt noch �61 Sekunde�r.");
								}
								
							}
							
						}
					}, 0L, 20L);
					
					
					
				}catch(NumberFormatException e){
					
					p.sendMessage(Data.p + "�4Fehler: �cSekunden muss eine Zahl sein");
					return true;
					
				}
				
				
			}
		}
		return true;
	}
}
