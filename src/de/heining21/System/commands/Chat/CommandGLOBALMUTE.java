package de.heining21.System.commands.Chat;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;

import de.heining21.System.Data;

@SuppressWarnings("deprecation")
public class CommandGLOBALMUTE implements CommandExecutor, Listener {

	boolean globalMuteOn = false;

	@EventHandler
	public void onChat(PlayerChatEvent e) {
		if (!e.getPlayer().hasPermission("System.globalmute.ignore") && globalMuteOn == true) {
			e.getPlayer().sendMessage(Data.p + "§cGlobalmute ist aktiv.");
			e.setCancelled(true);
		}
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Player p = (Player) cs;
		if (p.hasPermission("System.Globalmute")) {
			if (globalMuteOn == true) {
				globalMuteOn = false;
				Bukkit.broadcastMessage(Data.p + "§nGlobalmute wurde deaktiviert.");
			} else {
				globalMuteOn = true;
				Bukkit.broadcastMessage(Data.p + "§nGlobalmute wurde aktiviert.");
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}