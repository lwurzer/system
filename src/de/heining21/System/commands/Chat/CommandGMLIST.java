package de.heining21.System.commands.Chat;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandGMLIST implements CommandExecutor {

	String gmPlayersNames = "";

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("gmlist") && cs.hasPermission("System.gmlist")) {

			String gmPlayersNames = "";

			for (Player p1 : Bukkit.getServer().getOnlinePlayers()) {

				if (p1.getGameMode() == GameMode.CREATIVE) {

					gmPlayersNames = gmPlayersNames + ", " + p1.getName();
				}

				cs.sendMessage(Data.p + "§aFolgende Spieler haben Creative:");
				cs.sendMessage("§7" + gmPlayersNames);

				return true;

			}
		} else {
			cs.sendMessage(Data.KeineRechte + Data.rang);
		}
		return true;
	}
}