package de.heining21.System.commands.Chat;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.utils.FUNK_msg;
import de.heining21.System.utils.UtilARRAYStoSTRING;

public class CommandMSG implements CommandExecutor {

	public static ArrayList<String> lastSend = new ArrayList<String>();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("msg")) {

			if (cs instanceof ConsoleCommandSender) {

				cs.sendMessage(Data.p + "�cDu kannst das nicht tun.");
				return true;

			}
			if (args.length <= 1) {

				cs.sendMessage(Data.ZuWenigArgs);
				cs.sendMessage(Data.p + "�eVerwendung: �a/MSG <Spieler> <Nachricht>");
				return true;

			} else {

				Player sender = (Player) cs;

				String recName = args[0];
				String msg = UtilARRAYStoSTRING.arrayToString(args, 1);
				msg = msg.replace("&", "�");
				FUNK_msg.sendMessage(sender, recName, msg);
				return true;

			}

		}
		if (cmd.getName().equalsIgnoreCase("r")) {

			if (cs instanceof ConsoleCommandSender) {
				cs.sendMessage(Data.p + "�cDu kannst das nicht tun.");
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(Data.ZuWenigArgs);
				cs.sendMessage(Data.p + "�eVerwende:�a /r <Nachricht>");
				return true;
			}
			String Nachricht = UtilARRAYStoSTRING.arrayToString(args, 0);
			FUNK_msg.rToPlayer((Player) cs, Nachricht);
			return true;
		}

		return false;
	}

}
