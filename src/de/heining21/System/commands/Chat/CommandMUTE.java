package de.heining21.System.commands.Chat;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.utils.UUIDCache;
import de.heining21.System.utils.UtilARRAYStoSTRING;
import de.heining21.System.utils.UtilBROADCAST;

public class CommandMUTE implements CommandExecutor {

	public static long getMuteTime(Player p) {

		File file = new File(Data.path, UUIDCache.getUserUUID(p)
				+ ".yml");

		long time;
		if (isMuted(p)) {
			time = 0;
			return time;
		} else {
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			long end = cfg.getLong("Mute.Zeit");
			long restTime = (end - System.currentTimeMillis()) / 1000;

			return restTime;

		}

	}

	public static boolean isMuted(Player p) {

		boolean muted;
		File file = new File(Data.path, UUIDCache.getUserUUID(p)
				+ ".yml");

		if (!file.exists()) {
			muted = false;
			return muted;

		} else {

			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			muted = cfg.getBoolean("Mute.isMuted");
			return muted;

		}

	}

	void mutePlayer(String name, int minutes, String grund, CommandSender cs,
			String Zeit) {

		long end = System.currentTimeMillis() + 1000 * 60 * minutes;

		grund = grund.replace("&", "�");
		// Get time left or bassed in seconds

		

		// Time left in format
		
		

		File file = new File(Data.path, UUIDCache.getUserUUIDOffline(name)
				+ ".yml");

		if (!file.exists()) {
			cs.sendMessage(Data.p
					+ "�cDieser Benutzer war nie auf dem Server.");
			return;
		} else {

			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			cfg.set("Mute.isMuted", true);
			cfg.set("Mute.Grund", grund);
			cfg.set("Mute.Zeit", end);
			try {
				cfg.save(file);

				if (Bukkit.getPlayerExact(name) != null) {

					Player p = Bukkit.getPlayerExact(name);
					p.sendMessage(Data.p + "Du wurdest wegen �a" + grund + "�r�e f�r �a" + Zeit + "�r�e gemuted.");
					UtilBROADCAST.broadCastMessage(Data.p+ "�c" + name + "�e wurde wegen �a" + grund + "�r�e f�r �a" + Zeit + "�e gemuted.");

				}
				cs.sendMessage(Data.p + "�c" + name + "�r�e wurde gemuted");

			} catch (IOException e) {
				return;
			}

		}

	}

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("mute")) {

			if (!cs.hasPermission("System.mute")) {

				cs.sendMessage(Data.KeineRechte);
				return true;

			}
			if (args.length == 0) {
				cs.sendMessage(Data.p + "�eVerwendung: �a/Mute <Spieler> <Zeit> <Grund>");
			}
			if (args.length <= 2) {
				cs.sendMessage(Data.ZuWenigArgs);
				return true;
			} else {

				try {
					Player pMuted = Bukkit.getPlayer(args[0]);

					File file = new File(Data.path,
							UUIDCache.getUserUUID(pMuted) + ".yml");
					YamlConfiguration.loadConfiguration(file);

					String Name = args[0].toLowerCase();

					String time = args[1];
					if (time.endsWith("m") || time.endsWith("h")
							|| time.endsWith("d")) {

						String unit = time.substring(time.length() - 1);
						int value = Integer.parseInt(time.substring(0,
								time.length() - 1));

						if (unit.equals("m")) {

							String Zeit = value + " Minuten";

							mutePlayer(Name, value,
									UtilARRAYStoSTRING.arrayToString(args, 2),
									cs, Zeit);
							return true;

						}
						if (unit.equals("h")) {
							String Zeit = value + " Stunden";
							mutePlayer(Name, value * 60,
									UtilARRAYStoSTRING.arrayToString(args, 2),
									cs, Zeit);
							return true;
						}
						if (unit.equals("d")) {
							String Zeit = value + " Tage";
							mutePlayer(Name, value * 60 * 24,
									UtilARRAYStoSTRING.arrayToString(args, 2),
									cs, Zeit);
							return true;
						}

					}
				} catch (NullPointerException e) {
					cs.sendMessage(Data.UserNotExists);
					return true;
				}

			}

		}

		return false;
	}

}
