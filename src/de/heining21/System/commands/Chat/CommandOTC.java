package de.heining21.System.commands.Chat;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandOTC implements CommandExecutor {
	public Main plugin;

	public CommandOTC(Main plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cs.hasPermission("System.otc")) {
			if (args.length != 0) {
				String message = "";
				for (String arg : args) {
					message = message + " " + arg;
				}
				if (cs.hasPermission("System.otc")) {
					message = message.replace('&', '�');
				}
				for (Player all : Bukkit.getOnlinePlayers()) {
					if (all.hasPermission("System.otc")) {
						all.sendMessage("�8�l��4�lOWNER�6�lTeamChat�8�l� �4�l" + cs.getName().replace("CONSOLE", "Konsole") + "�7�l:�c�l" + message);
					}
				}
			} else {
				cs.sendMessage("�eVerwendung: �a/OTC <Nachricht>");
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}


