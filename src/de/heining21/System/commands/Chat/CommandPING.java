package de.heining21.System.commands.Chat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandPING implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,String[] args) {
		Player p = (Player) cs;
		
		if (cmd.getName().equalsIgnoreCase("ping")) {
			int ping = ((CraftPlayer) p).getHandle().ping;
			p.sendMessage(Data.p + "Dein Ping betr�gt: �6" + ping);
		}
		
		return true;
	}
}
