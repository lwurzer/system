package de.heining21.System.commands.Chat;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandRANDOM implements CommandExecutor {
	public Main plugin;

	public CommandRANDOM(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Random r = new Random();
		Player[] onlinePlayer = (Player[]) Bukkit.getServer().getOnlinePlayers().toArray(new Player[0]);
		
		String spielernameRandomPlayer = onlinePlayer[r.nextInt(onlinePlayer.length)].getName();
		if (cmd.getName().equalsIgnoreCase("random") && cs.hasPermission("System.random")) {
			Bukkit.broadcastMessage(Data.pfett + "�e�lDer Spieler �a�l" + spielernameRandomPlayer + " �e�lwurde zuf�llig ausgew�hlt.");
			playEffect();
			playSound();
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}
	
	@SuppressWarnings("deprecation")
	public void playEffect() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 0);
		}
	}
	
	public void playSound() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.playSound(p.getLocation(), Sound.FIREWORK_TWINKLE2, 50F, 1F);
		}
	}
}
