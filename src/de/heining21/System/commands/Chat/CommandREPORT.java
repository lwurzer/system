package de.heining21.System.commands.Chat;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandREPORT implements CommandExecutor {
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("report")) {
			
			if (args.length == 0) {
				p.sendMessage(Data.p + "Verwendung: �a/Report <Spieler> <Hack>");
				p.sendMessage("�cAchtung! => Nur den Namen des Hack's angeben. (z.B. FlyHack, SpeedHack, KillAura, etc.)");
				return true;
			}
			
			if (args[0].equalsIgnoreCase("heining21") || args[0].equalsIgnoreCase("higgs_01")) {
				p.sendMessage(Data.p + "�cDu kannst den Developer vom System nicht reporten. (" + args[0] + ")");
				return true;
			}
			
			if (args.length == 2) {
				Player p2 = p.getServer().getPlayer(args[0]);
				if (p2 != null) {
					for (Player online : Bukkit.getOnlinePlayers()) {
						if (online.hasPermission("System.report")) {
							online.sendMessage(Data.p + "�cDer Spieler �4" + p2.getName() + " �cwurde von " + p.getName() + " reportet. GRUND: �4" + args[1]);
						}
					}
					p.sendMessage(Data.p + "Danke f�r deinen Report. Das Team wird sich schnellst m�glich darum k�mmern.");
					
				} else {
					p.sendMessage(Data.p + "�cDer Spieler �4" + args[0] + " �cist nicht online.");
				}
			} else {
				p.sendMessage(Data.p + "Verwendung: �a/report <Spieler> <HackClient>");
			}
		}
		return true;
	}
}
