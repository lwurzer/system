package de.heining21.System.commands.Chat;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import de.heining21.System.Data;

public class CommandSOCIALSPY implements CommandExecutor, Listener {

	public static ArrayList<String> stalker = new ArrayList<String>();

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (!cs.hasPermission("System.socialspy")) {
			cs.sendMessage(Data.KeineRechte);
			return true;
		} else {

			if (!(cs instanceof Player)) {
				cs.sendMessage(Data.p + "�cDu kannst das nicht tun.");
				return true;
			} else {

				if (stalker.contains(cs.getName())) {

					stalker.remove(cs.getName());
					cs.sendMessage(Data.p + "Socialspy wurde �aDeaktiviert.");
					return true;

				} else {

					stalker.add(cs.getName());
					cs.sendMessage(Data.p + "Socialspy wurde �aAktiviert");
					return true;

				}

			}

		}
	}

	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent e) {

		String[] msg = e.getMessage().split(" ");

		if (msg[0].contains("msg") && msg.length >= 2) {

			Player sender = e.getPlayer();
			String p = msg[1];

			StringBuffer result = new StringBuffer();
			for (String element : msg) {
				result.append(element + " ");
			}
			String Nachricht = result.toString();
			Nachricht = Nachricht.replace("/msg ", "");
			Nachricht = Nachricht.replace(p + " ", "");

			for (Player pRec : Bukkit.getOnlinePlayers()) {

				if (pRec.hasPermission("System.socialspy")) {

					pRec.sendMessage(Data.Socialspy + sender.getName()
							+ " �8>�7 " + p + "�8: �7" + Nachricht);

				}

			}

		} else {
			return;
		}

	}

}
