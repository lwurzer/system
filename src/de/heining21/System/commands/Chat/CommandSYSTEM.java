package de.heining21.System.commands.Chat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.heining21.System.Data;

public class CommandSYSTEM implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		
//		Player p = (Player) cs;
		
		int max = 3;

		if (cmd.getName().equalsIgnoreCase("system")) {

			if (args.length == 0) {

				cs.sendMessage("�7----------�5Help System�7 1/" + max + "----------");
				cs.sendMessage("�7/gm  :�5 Setzt den Gamemode f�r einen Spieler");
				cs.sendMessage("�7/ja  :�5 Stimme ab! ");
				cs.sendMessage("�7/nein  :�5 Stimme ab!");
				cs.sendMessage("�7/online  :�5 Zeige alle Spieler an, die online sind");
				cs.sendMessage("�7/rang  :�5 Infos der R�nge");
				cs.sendMessage("�7/skype  :�5 Skypenamen der Owner");
				cs.sendMessage("�7/system  :�5 Dieses Fenster");
				cs.sendMessage("�8/system 2  f�r die n�chste Seite.");
				cs.sendMessage("�7----------�5Help System�7 1/" + max + "----------");
				return true;
			} else {
				int seite;
				try {
					seite = Integer.parseInt(args[0]);
				} catch (NumberFormatException e) {

					cs.sendMessage(Data.p
							+ "�4Fehler: �cSeite muss eine Zahl sein.");
					return true;

				}
				if (seite == 2) {

					cs.sendMessage("�7----------�5Help System�7 2/" + max + "----------");
					cs.sendMessage("�7/ts  :�5 Zeigt die IP des ts an");
					cs.sendMessage("�7/vote  :�5 Alle infos um zu Voten ");
					cs.sendMessage("�7/itemdb  :�5 ID des Items in der Hand");
					cs.sendMessage("�7/kit  :�5 Kits");
					cs.sendMessage("�7/minigames  :�5 Minigames");
					cs.sendMessage("�7/money  :�5 Dein kontotstand");
					cs.sendMessage("�7/pay  :�5 Zahle einem Spieler");
					cs.sendMessage("�8/system 3  f�r die n�chste Seite.");
					cs.sendMessage("�7----------�5Help System�7 2/" + max + "----------");
					return true;

				} else if (seite == 3) {

					cs.sendMessage("�7----------�5Help System�7 3/" + max + "----------");
					cs.sendMessage("�7/motd  :�5 Zeigt den MOTD an");
					cs.sendMessage("�7/perk  :�5 Perks ");
					cs.sendMessage("�7/rename  :�5 �ndere den Namen des Items");
					cs.sendMessage("�7/shop  :�5 Shop");
					cs.sendMessage("�7/support  :�5 Fordere Support an");
					cs.sendMessage("�7/warp  :�5 Warps");
					cs.sendMessage("�7/wb  :�5 �fnet eine Werkbank");
					cs.sendMessage("�7----------�5Help System�7 3/" + max + "----------");
					return true;
				}

			}

		}

		return false;
	}

}
