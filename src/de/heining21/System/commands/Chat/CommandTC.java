package de.heining21.System.commands.Chat;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandTC implements CommandExecutor {
	public Main plugin;

	public CommandTC(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cs.hasPermission("System.tc")) {
			if (args.length != 0) {
				String message = "";
				for (String arg : args) {
					message = message + " " + arg;
				}
				if (cs.hasPermission("System.tc")) {
					message = message.replace('&', '�');
				}
				for (Player all : Bukkit.getOnlinePlayers()) {
					if (all.hasPermission("System.tc")) {
						all.sendMessage("�7[�bTeamChat�7] �6" + cs.getName().replace("CONSOLE", "Konsole") + "�7:�e" + message);
					}
				}
			} else {
				cs.sendMessage("�eVerwendung: �a/TC | TeamChat <Nachricht>");
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}
