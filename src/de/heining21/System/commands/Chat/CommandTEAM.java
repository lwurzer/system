package de.heining21.System.commands.Chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class CommandTEAM implements CommandExecutor {

	public static String isOnline(String string) {
		Player target = Bukkit.getServer().getPlayer(string);
		if (target != null) {
			string = ChatColor.GREEN + string;
		} else {
			string = ChatColor.GRAY + string;
		}
		return string;
	}

	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("team")) {
			if (Bukkit.getPluginManager().isPluginEnabled("PermissionsEx")) {
				PermissionManager pex = PermissionsEx.getPermissionManager();
				String owner = "";
				String teamleiter = "";
				String dev = "";
				String testdev = "";
				String admin = "";
				String mod = "";
				String sup = "";
				String cs = "";
				String archi = "";
				
				for (PermissionUser pu : pex.getGroup("Owner").getUsers()) {
					if (!pu.getName().contains("-")) {
						owner = owner + isOnline(pu.getName()) + " �b�l| ";
					}
				}
				for (PermissionUser pu : pex.getGroup("teamleiter").getUsers()) {
					if (!pu.getName().contains("-")) {
						teamleiter = teamleiter + isOnline(pu.getName()) + " �b�l| ";
					}
				}
				for (PermissionUser pu : pex.getGroup("Admin").getUsers()) {
					if (!pu.getName().contains("-")) {
						admin = admin + isOnline(pu.getName()) + " �b�l| ";
					}
				}
				for (PermissionUser pu : pex.getGroup("Developer").getUsers()) {
					if (!pu.getName().contains("-")) {
						dev = dev + isOnline(pu.getName()) + " �b�l| ";
					}
				}
				for (PermissionUser pu : pex.getGroup("Test-Dev").getUsers()) {
					if (!pu.getName().contains("-")) {
						testdev = testdev + isOnline(pu.getName()) + " �b�l| ";
					}
				}
				for (PermissionUser pu : pex.getGroup("Moderator").getUsers()) {
					if (!pu.getName().contains("-")) {
						mod = mod + isOnline(pu.getName()) + " �b�l| ";
					}
				}
				for (PermissionUser pu : pex.getGroup("Supporter").getUsers()) {
					if (!pu.getName().contains("-")) {
						sup = sup + isOnline(pu.getName()) + " �b�l| ";
					}
				}
				for (PermissionUser pu : pex.getGroup("CS").getUsers()) {
					if (!pu.getName().contains("-")) {
						cs = cs + isOnline(pu.getName()) + " �b�l| ";
					}
				}
				for (PermissionUser pu : pex.getGroup("Architekt").getUsers()) {
					if (!pu.getName().contains("-")) {
						archi = archi + isOnline(pu.getName()) + " �b�l| ";
					}
				}
				sender.sendMessage("�2��a�m------------" + ChatColor.RESET + " �6CraftPlace-Team �a�m------------" + ChatColor.RESET + "�2�");
				if (owner != "") {
					sender.sendMessage("�7[�4Owner�7]�4: "
							+ owner.substring(0, owner.length() - 7));
				}
				if (teamleiter != "") {
					sender.sendMessage("�7[�cTeamLeiter�7]�c: " + teamleiter.substring(0, teamleiter.length() - 7));
				}
				if (admin != "") {
					sender.sendMessage("�7[�cAdmin�7]�c: " + admin.substring(0, admin.length() - 7));
				}
				if (mod != "") {
					sender.sendMessage("�7[�5Moderator�7]�5: "
							+ mod.substring(0, mod.length() - 7));
				}
				if (dev != "") {
					sender.sendMessage("�7[�3Developer�7]�3: "
							+ dev.substring(0, dev.length() - 7));
				}
				if (testdev != "") {
					sender.sendMessage("�7[�bTestDev�7]�b: "
							+ testdev.substring(0, testdev.length() - 7));
				}
				if (archi != "") {
					sender.sendMessage("�7[�dArchitekt�7]�d: "
							+ archi.substring(0, archi.length() - 7));
				}
				if (sup != "") {
					sender.sendMessage("�7[�2Supporter�7]�2: "
							+ sup.substring(0, sup.length() - 7));
				}
				if (cs != "") {
					sender.sendMessage("�7-�2CS�7-�a: "
							+ cs.substring(0, cs.length() - 7));
				}
				sender.sendMessage("�2��a�m------------" + ChatColor.RESET + " �6CraftPlace-Team �a�m------------" + ChatColor.RESET + "�2�");
			} else {
				sender.sendMessage("�cFehler: Bitte dringend bei heining21 melden! => PEX-Error");
			}
		}
		return true;
	}
}
