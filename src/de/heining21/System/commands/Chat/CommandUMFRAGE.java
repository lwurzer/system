package de.heining21.System.commands.Chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandUMFRAGE implements CommandExecutor {
	public Main plugin;

	public CommandUMFRAGE(Main plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Player p = (Player) cs;
		if (p.hasPermission("System.Umfrage")) {
			if (args.length != 0) {
				if (!Data.umfrage) {
					String umfrage = "";
					for (String arg : args) {
						umfrage = umfrage + arg + " ";
					}

					Bukkit.broadcastMessage("�8��7�m--------------" + ChatColor.RESET + " �5Umfrage �7�m--------------" + ChatColor.RESET + "�8�");
					Bukkit.broadcastMessage("  �a" + cs.getName() + " �ehat eine Umfrage gestartet!");
					Bukkit.broadcastMessage("");
					Bukkit.broadcastMessage(" �8>> �2" + umfrage + ChatColor.RESET + "�8<<");
					Bukkit.broadcastMessage("");
					Bukkit.broadcastMessage("      �c> �eVote jetzt mit " + ChatColor.BOLD + "�2/Ja " + ChatColor.RESET + "�eoder " + ChatColor.BOLD + "�4/Nein�e." + ChatColor.RESET + ChatColor.RED + "<");
					Bukkit.broadcastMessage("�8��7�m--------------" + ChatColor.RESET + " �5Umfrage �7�m--------------" + ChatColor.RESET + "�8�");
					Data.umfrage = true;

					Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin,
							new Runnable() {
								public void run() {
									Bukkit.broadcastMessage("�8>> �eDie Umfrage wird in �215 �eSekunden geschlossen �8<<");

									Bukkit.getScheduler()
											.scheduleAsyncDelayedTask(plugin,
													new Runnable() {
														public void run() {
															Bukkit.broadcastMessage("�8>> �eDie Umfrage wird in �210 �eSekunden geschlossen �8<<");

															Bukkit.getScheduler()
																	.scheduleAsyncDelayedTask(
																			plugin,
																			new Runnable() {
																				public void run() {
																					Bukkit.broadcastMessage("�8>> �eDie Umfrage wird in �25 �eSekunden geschlossen �8<<");

																					Bukkit.getScheduler()
																							.scheduleAsyncDelayedTask(
																									plugin,
																									new Runnable() {

																										public void run() {

																											Bukkit.broadcastMessage("�8��7�m--------------" + ChatColor.RESET + " �5Umfrage �7�m--------------" + ChatColor.RESET + "�8�");
																											Bukkit.broadcastMessage("�eDie Umfrage wurde geschlossen!");
																											Bukkit.broadcastMessage("");
																											Bukkit.broadcastMessage("�5Ergebnis:");
																											Bukkit.broadcastMessage("�eStimmen f�r �2Ja�f: �c" + Data.ja);
																											Bukkit.broadcastMessage("�eStimmen f�r �4Nein�f: �c" + Data.nein);
																											Bukkit.broadcastMessage("");
																											Bukkit.broadcastMessage("�8��7�m--------------" + ChatColor.RESET + " �5Umfrage �7�m--------------" + ChatColor.RESET + "�8�");

																											Data.ja = 0;
																											Data.nein = 0;
																											Data.voted.clear();
																											Data.umfrage = false;

																										}
																									},
																									100L);
																				}
																			},
																			100L);
														}
													}, 100L);
								}
							}, 100L);

				} else {
					p.sendMessage(Data.p + "�cEs l�uft gerade eine Umfrage. Bitte warte kurz.");
				}
			} else {
				p.sendMessage("�eVerwendung: �a/Umfrage <Frage>");
			}
		} else {
			p.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}