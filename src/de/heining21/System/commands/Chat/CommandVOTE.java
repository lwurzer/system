package de.heining21.System.commands.Chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import com.vexsoftware.votifier.model.VotifierEvent;

import de.heining21.System.Data;

public class CommandVOTE implements CommandExecutor, Listener {
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("vote")) {
			cs.sendMessage("�8��7�m--------------" + ChatColor.RESET
					+ " �5Vote �7�m--------------" + ChatColor.RESET + "�8�");
			cs.sendMessage("�c1. �3�ffne den Vote-Link: �6Vote.CraftPlace.de");
			cs.sendMessage("�c2. �3Gib deinen Namen und den Sicherheits-Code ein");
			cs.sendMessage("�c3. �3Warte und sei Online, wenn du gevotet hast!");
			cs.sendMessage("�c4. �3Du willst doppelte Vote-Belohung?");
			cs.sendMessage("�7=> �6Vote2.CraftPlace.de");
			cs.sendMessage("�8��7�m--------------" + ChatColor.RESET
					+ " �5Vote �7�m--------------" + ChatColor.RESET + "�8�");
		}
		return true;
	}

	@EventHandler
	public void onVote(VotifierEvent e) {
		Player p = Bukkit.getPlayer(e.getVote().getUsername());
		
		// VoteItems
		
		ItemStack Vote1 = new ItemStack(Material.DIAMOND_HELMET, 1);
		ItemStack Vote2 = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
		ItemStack Vote3 = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
		ItemStack Vote4 = new ItemStack(Material.DIAMOND_BOOTS, 1);
		ItemStack Vote5 = new ItemStack(Material.EXP_BOTTLE, 64);
		ItemStack Vote6 = new ItemStack(Material.EXP_BOTTLE, 64);
		ItemStack Vote7 = new ItemStack(Material.IRON_BLOCK, 1);
		ItemStack Vote8 = new ItemStack(Material.ENCHANTED_BOOK, 1);
		Vote8.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		
		// VoteItems
		
		Bukkit.broadcastMessage(Data.p + " �fDer Spieler �5" + e.getVote().getUsername() + " �fhat f�r den Server gevotet.");
		p.getInventory().addItem(Vote1);
		p.getInventory().addItem(Vote2);
		p.getInventory().addItem(Vote3);
		p.getInventory().addItem(Vote4);
		p.getInventory().addItem(Vote5);
		p.getInventory().addItem(Vote6);
		p.getInventory().addItem(Vote7);
		p.getInventory().addItem(Vote8);
		
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco add " + p.getName() + " 500");
	}
}