package de.heining21.System.commands.Game;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import de.heining21.System.Data;

@SuppressWarnings("deprecation")
public class CommandAFK implements CommandExecutor, Listener {
	
	public static ArrayList<Player> afk = new ArrayList<Player>();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("afk")) {
			if (p.hasPermission("System.afk")) {
				
				if (afk.contains(p)) {
					afk.remove(p);
					for (Player online : Bukkit.getOnlinePlayers()) {
						if (online.hasPermission("System.afk")) {
							online.sendMessage(Data.p + "Der Spieler �a" + p.getName() + " �eist nicht mehr AFK.");
						}
					}
				} else {
					afk.add(p);
					for (Player online : Bukkit.getOnlinePlayers()) {
						if (online.hasPermission("System.afk")) {
							online.sendMessage(Data.p + "Der Spieler �a" + p.getName() + " �eist jetzt AFK.");
						}
					}
				}
			}
		}
		return true;
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = (Player) e.getPlayer();
		if (afk.contains(p)) {
			if (e.getFrom().getBlockX() == e.getTo().getBlockX() && e.getFrom().getBlockY() == e.getTo().getBlockY() && e.getFrom().getBlockZ() == e.getTo().getBlockZ()) {
				afk.remove(p);
				for (Player online : Bukkit.getOnlinePlayers()) {
					if (online.hasPermission("System.afk")) {
						online.sendMessage(Data.p + "Der Spieler �a" + p.getName() + " �eist nicht mehr AFK.");
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onChat(PlayerChatEvent e) {
		Player p = (Player) e.getPlayer();
		if (afk.contains(p)) {
			afk.remove(p);
			for (Player online : Bukkit.getOnlinePlayers()) {
				if (online.hasPermission("System.afk")) {
					online.sendMessage(Data.p + "Der Spieler �a" + p.getName() + " �eist nicht mehr AFK.");
				}
			}
		}
	}
	
}
