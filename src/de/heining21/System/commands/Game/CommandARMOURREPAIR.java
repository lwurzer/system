package de.heining21.System.commands.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandARMOURREPAIR implements CommandExecutor {
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("armourrepair")) {
			
			if (p.getInventory().getArmorContents() == null) {
				p.sendMessage(Data.p + "�cDu hast keine R�stung an.");
				return true;
			}
			
			if (p.getInventory().getHelmet() != null) {
				p.getInventory().getHelmet().setDurability((short) 0);
			}
			
			if (p.getInventory().getChestplate() != null) {
			p.getInventory().getChestplate().setDurability((short) 0);
			}
			
			if (p.getInventory().getLeggings() != null) {
			p.getInventory().getLeggings().setDurability((short) 0);
			}
			
			if (p.getInventory().getBoots() != null) {
			p.getInventory().getBoots().setDurability((short) 0);
			}
			
			p.sendMessage(Data.p + "�aDu hast erfolgreich deine R�stung repariert.");
		}
		return true;
	}
}
