package de.heining21.System.commands.Game;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.CaveSpider;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.MagmaCube;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Silverfish;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Slime;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Witch;
import org.bukkit.entity.Wither;
import org.bukkit.entity.Zombie;

import de.heining21.System.Data;

public class CommandBUTCHER implements CommandExecutor{

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("butcher")){
			
			if(cs instanceof ConsoleCommandSender){
				
				cs.sendMessage(Data.ConsoleCantDO);
				return true;
				
			}
			if(!cs.hasPermission("System.butcher")){
				
				cs.sendMessage(Data.KeineRechte);
				return true;
				
			}
			Player p = (Player)cs;
			World world = p.getWorld();
			
			int count = 0;
			
			for(Entity e : world.getEntities()){
				
				if(e instanceof Skeleton    || 
					e instanceof Zombie     ||	
					e instanceof Blaze      ||
					e instanceof Spider     ||
					e instanceof CaveSpider ||
					e instanceof Enderman   ||
					e instanceof Creeper    ||
					e instanceof EnderDragon||
					e instanceof Ghast      ||
					e instanceof MagmaCube  ||
					e instanceof Silverfish ||
					e instanceof Slime      ||
					e instanceof Witch      ||
					e instanceof Wither     ||
					e instanceof PigZombie){
					
					e.remove();
					count++;
				}
					
				
			}
			p.sendMessage(Data.p + "�eAlle �6" + count + " �eMobs wurde entfernt!");
			return true;
			
			
		}
		
		return false;
	}
	
	
}
