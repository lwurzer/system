package de.heining21.System.commands.Game;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandCLEARENDERCHEST implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("clearenderchest") && cs.hasPermission("System.clearenderchest")) {
			Player p = (Player) cs;
			if (args.length == 0) {
				p.sendMessage("�eVerwendung: �a/ClearEnderChest | ce | cec | ClearE | ClearEchest <Spieler>");
			}
			if (args.length == 1) {
				try {
					Player p2 = p.getServer().getPlayer(args[0]);
					p2.getEnderChest().clear();
					p.playSound(p.getLocation(), Sound.LEVEL_UP, 50F, 1F);
					p.sendMessage(Data.p + "�eDu hast die Enderchest von �a" + p2.getName() + "�e gecleart.");
					
				} catch (NullPointerException e) {

					cs.sendMessage(Data.UserNotExists);

				}
			}
		} else {
			cs.sendMessage(Data.KeineRechte);

		}
		return true;
	}
}