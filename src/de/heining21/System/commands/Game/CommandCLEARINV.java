package de.heining21.System.commands.Game;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandCLEARINV implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("clearinv")
				&& cs.hasPermission("System.clearinv")) {
			Player p = (Player) cs;
			if (args.length == 0) {
				p.getInventory().clear();
				p.playSound(p.getLocation(), Sound.LEVEL_UP, 50F, 1F);
				p.sendMessage(Data.p + "Du hast dein Inventar gecleart.");
			}
			if (args.length == 1) {
				try {
					if (!p.hasPermission("System.clearinv.others")) {
						p.sendMessage(Data.KeineRechte);
						return true;
					}
					Player p2 = p.getServer().getPlayer(args[0]);
					p2.getInventory().clear();
					p.playSound(p.getLocation(), Sound.LEVEL_UP, 50F, 1F);
					p2.playSound(p.getLocation(), Sound.LEVEL_UP, 50F, 1F);
					p.sendMessage(Data.p + "�eDu hast das Inventar von �a"
							+ p2.getName() + "�e gecleart.");
					p2.sendMessage(Data.p + "�eDein Inventar wurde von �a"
							+ p.getName() + "�e gecleart.");
				} catch (NullPointerException e) {

					cs.sendMessage(Data.UserNotExists);
					return true;

				}
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
			return true;
		}
		return true;
	}
}