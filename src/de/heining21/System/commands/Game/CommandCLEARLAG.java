package de.heining21.System.commands.Game;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandCLEARLAG implements CommandExecutor {

	private Main plugin;

	int shed;

	int time = 10;

	public CommandCLEARLAG(Main main) {
		plugin = main;
	}

	public boolean onCommand(final CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("clearlag")
				&& cs.hasPermission("System.clearlag")) {
			if (!(cs instanceof Player)) {

				cs.sendMessage(Data.p + "�cDu kannst das nicht tun");

			}
			final Player p = (Player) cs;

			Bukkit.broadcastMessage(Data.p + "�e�lIn 10 Sek werden alle Items vom Boden entfernt.");

			shed = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin,
					new Runnable() {

						public void run() {

							time = time - 1;

							if (time == 5) {

								Bukkit.broadcastMessage(Data.p + "�e�lIn 5 Sek werden alle Items vom Boden entfernt.");
							}
							if (time == 0) {

								List<Entity> entity = Bukkit.getWorld(
										p.getWorld().getName()).getEntities();
								for (int i = 0; i < entity.size(); i++) {

									Entity e = entity.get(i);

									if (e instanceof Item
											|| e instanceof ExperienceOrb) {

										e.remove();

									}

								}

								Bukkit.broadcastMessage(Data.p
										+ "�eAlle Laggs wurden von �a"
										+ cs.getName() + "�e entfernt.");

								time = 10;

								Bukkit.getScheduler().cancelTask(shed);
							}

						}
					}, 20L, 20L);

		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}