package de.heining21.System.commands.Game;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.heining21.System.Data;

public class CommandENCHANT implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Player p = (Player) cs;
		if (p.hasPermission("System.enchant")) {
			if (args.length == 0) {
				p.sendMessage(Data.p + "Verwendung: �a/Enchant <ID, List> <Level>");
				return true;
			}
			
			if (args.length == 1) {
				
				if (args[0].equalsIgnoreCase("list")) {
					p.sendMessage("�7[]========< �eEnchantment-Liste �7>========[]");
					p.sendMessage("�bR�stung:");
					p.sendMessage("  �7- <ID> <NAME> <H�CHSTE VERZAUBERUNG>");
					p.sendMessage("  �7- <0> <Schutz> <1-4>");
					p.sendMessage("  �7- <1> <FeuerSchutz> <1-4>");
					p.sendMessage("  �7- <2> <FederFall> <1-4>");
					p.sendMessage("  �7- <3> <ExplosionsSchutz> <1-4>");
					p.sendMessage("  �7- <4> <Schussicher> <1-4>");
					p.sendMessage("  �7- <5> <Atmung> <1-3>");
					p.sendMessage("  �7- <6> <Wasseraffinit�t> <1>");
					p.sendMessage("  �7- <7> <Dornen> <1-3>");
					p.sendMessage("  �7- <8> <Wasserl�ufer> <1-3>");
					p.sendMessage("");
					p.sendMessage("�bWaffe:");
					p.sendMessage("  �7- <16> <Sch�rfe> <1-5>");
					p.sendMessage("  �7- <17> <Bann> <1-5>");
					p.sendMessage("  �7- <18> <Nemesis der Gliederf��er> <1-5>");
					p.sendMessage("  �7- <19> <R�cksto�> <1-2>");
					p.sendMessage("  �7- <20> <Verbrennung> <1-2>");
					p.sendMessage("  �7- <21> <Pl�nderung> <1-5>");
					p.sendMessage("");
					p.sendMessage("�bWerkzeug:");
					p.sendMessage("  �7- <32> <Effizienz> <1-5>");
					p.sendMessage("  �7- <33> <Behutsamkeit> <1>");
					p.sendMessage("  �7- <34> <Haltbarkeit> <1-3>");
					p.sendMessage("  �7- <35> <Gl�ck> <1-3>");
					p.sendMessage("");
					p.sendMessage("�bBogen:");
					p.sendMessage("  �7- <48> <St�rke> <1-5>");
					p.sendMessage("  �7- <49> <Schlag> <1-3>");
					p.sendMessage("  �7- <50> <Flamme> <1-3>");
					p.sendMessage("  �7- <51> <Undenlichkeit> <1>");
					p.sendMessage("");
					p.sendMessage("�bAngel:");
					p.sendMessage("  �7- <61> <Gl�ck des Meeres> <1>");
					p.sendMessage("  �7- <62> <K�der> <1-3>");
					p.sendMessage("�7[]========< �eEnchantment-Liste �7>========[]");
					return true;
				} else {
					p.sendMessage(Data.p + "Verwendung: �a/Enchant �c<ID, List> �a<Level>");
					return true;
				}
			}
			
			if (args.length == 2) {
				
				if (p.getItemInHand().getType() == Material.AIR) {
					p.sendMessage(Data.p + "�cDu hast kein Item in der Hand.");
					return true;
				}
				
				int enchantment = Integer.parseInt(args[0]);
				int level;
				try {
					level = Integer.parseInt(args[1]);
					if (level >= 6) {
						p.sendMessage(Data.p + "�cDas Level darf h�stens 5 betragen.");
						return true;
					}
				} catch(NumberFormatException e) {
					p.sendMessage(Data.p + "�cDas Level muss eine Zahl sein.");
					return true;
				}
				@SuppressWarnings("deprecation")
				Enchantment ench = Enchantment.getById(enchantment);
				if (ench == null) {
					p.sendMessage(Data.p + "�cDas Enchantment �4" + enchantment + " �cist nicht g�ltig.");
					return true;
				}
				
				ItemStack item = p.getItemInHand();
				ItemMeta meta = item.getItemMeta();
				meta.addEnchant(ench, level, true);
				item.setItemMeta(meta);
				p.setItemInHand(item);
				p.sendMessage(Data.p + "Du hast das Item in deiner Hand verzaubert.");
				p.sendMessage("    �7- Enchantment-ID: " + enchantment);
				p.sendMessage("    �7- Level: " + level);
			}
		}
		return true;
	}
}
