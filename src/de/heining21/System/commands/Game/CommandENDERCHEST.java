package de.heining21.System.commands.Game;

import java.util.ArrayList;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import de.heining21.System.Data;

public class CommandENDERCHEST implements CommandExecutor {
	
	ArrayList<String> schauen = new ArrayList<String>();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("enderchest")
				&& cs.hasPermission("System.enderchest")) {
			Player p = (Player) cs;
			if (args.length == 0) {
				p.closeInventory();
				p.openInventory(p.getEnderChest());
				p.playSound(p.getLocation(), Sound.DIG_WOOD, 50F, 1F);
				p.sendMessage(Data.p + "�eDu hast deine Enderchest ge�ffnet.");
			} else {
				cs.sendMessage(Data.KeineRechte);
			}
			
			if (cs.hasPermission("System.enderchest.other")) {
				if (args.length == 1) {
					Player p2 = p.getServer().getPlayer(args[0]);
					p2.closeInventory();
					if (!p.hasPermission("System.enderchest.editother")) {
						schauen.add(p.getName());
					}
					p2.openInventory(p2.getEnderChest());
					p.playSound(p.getLocation(), Sound.DIG_WOOD, 50F, 1F);
					p.sendMessage(Data.p + "�eDu hast die Enderchest von �a" + p2.getName() + "�e ge�ffnet.");
					p2.sendMessage(Data.p + "�eDeine Enderchest wurde von �a" + p.getName() + " �ege�ffnet.");
				}
			} else {
				cs.sendMessage(Data.KeineRechte + Data.rang);
			}
		}
		return true;
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = (Player) e.getPlayer();
		if (schauen.contains(p.getName())) {
			schauen.remove(p.getName());
		}
	}
	
	@EventHandler
	public void onEchestKlick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		
		if (schauen.contains(p.getName())) {
			e.setCancelled(true);
		}		
	}	
}