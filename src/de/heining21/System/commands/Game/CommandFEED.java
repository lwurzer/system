package de.heining21.System.commands.Game;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandFEED implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("feed")
				&& cs.hasPermission("System.feed")) {
			Player p = (Player) cs;
			if (args.length == 0) {
				p.setFoodLevel(20);
				p.sendMessage(Data.p + "�eDein Hunger wurde gestillt.");
				p.playSound(p.getLocation(), Sound.EAT, 50F, 1F);
			}
			if (args.length == 1) {

				try {
					if (!p.hasPermission("System.feed.others")) {
						p.sendMessage(Data.KeineRechte);
						return true;
					}

					Player p2 = p.getServer().getPlayer(args[0]);
					p2.setFoodLevel(20);
					p.sendMessage(Data.p + "�eDu hast den Hunger von �a"
							+ p2.getName() + " �egestillt.");
					p.playSound(p.getLocation(), Sound.EAT, 50F, 1F);
					p2.sendMessage(Data.p + "�eDein Hunger wurde von �a"
							+ p.getName() + " �egestillt.");
					p2.playSound(p.getLocation(), Sound.EAT, 50F, 1F);
				} catch (NullPointerException e) {
					cs.sendMessage(Data.UserNotExists);
				}

			}
		} else {
			cs.sendMessage(Data.KeineRechte + Data.rang);
		}
		return true;
	}
}