package de.heining21.System.commands.Game;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.heining21.System.Data;

public class CommandFIX implements CommandExecutor {
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("fix") && p.hasPermission("System.fix")) {
			if (args.length == 0) {
				p.sendMessage(Data.p + "�eVerwendung: �a/Fix | Repair <Hand, All>");
			}
			
			if (args.length == 1) {
			
				if (args[0].equalsIgnoreCase("hand")) {
					if (p.getItemInHand().getDurability() == 0) {
						p.sendMessage(Data.p + "�cDieses Item ist schon repariert.");
					} else {
						if (p.getItemInHand() != new ItemStack(Material.AIR)) {
							p.getItemInHand().setDurability((short) 0);
							p.sendMessage(Data.p + "�aDu hast das Item in deiner �eHand �arepariert.");
						} else {
							p.sendMessage(Data.p + "�cDu hast nichts in der Hand.");
						}
					}
				}
				
				if (args[0].equalsIgnoreCase("all")) {
					System.out.println("Z1");
					for (ItemStack invcontains : p.getInventory().getContents()) {
						invcontains.setDurability((short) 0);
					}

					System.out.println("Z2");
					p.getInventory().getHelmet().setDurability((short) 0);
					p.getInventory().getChestplate().setDurability((short) 0);
					p.getInventory().getLeggings().setDurability((short) 0);
					p.getInventory().getBoots().setDurability((short) 0);
					System.out.println("Z3");
					
					p.sendMessage(Data.p + "�aDu hast alle Items in deinem �eInventar �arepariert.");
				}
			}
		} else {
			p.sendMessage(Data.KeineRechte);
		}
		return true;
	}
	
	
}
