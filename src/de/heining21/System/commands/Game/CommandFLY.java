package de.heining21.System.commands.Game;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandFLY implements CommandExecutor {

	public static ArrayList<String> isflying = new ArrayList<String>();

	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("fly")) {
			if (p.hasPermission("System.fly")) {
				if (args.length == 0) {
					
					if (!isflying.contains(p.getName())) {
						isflying.add(p.getName());
						p.setAllowFlight(true);
						p.setFlying(true);
						p.sendMessage(Data.p + "Dein Fly wurde aktiviert.");
						
					} else {
						isflying.remove(p.getName());
						p.setAllowFlight(false);
						p.setFlying(false);
						p.sendMessage(Data.p + "Dein Fly wurde deaktiviert.");
					}
					
				}
				
				if (args.length == 1) {

					Player p2 = p.getServer().getPlayer(args[0]);
					
					if (!isflying.contains(p2.getName())) {
						isflying.add(p2.getName());
						p2.setAllowFlight(true);
						p2.setFlying(true);
						p2.sendMessage(Data.p + "Dein Fly wurde von �a" + p.getName() + "�e aktiviert.");
						p.sendMessage(Data.p + "Fly von �a" + p2.getName() + "�e aktiviert.");
						
					} else {
						
						isflying.remove(p2.getName());
						p2.setAllowFlight(false);
						p2.setFlying(false);
						p2.sendMessage(Data.p + "Dein Fly wurde von �a" + p.getName() + "�e deaktiviert.");
						p.sendMessage(Data.p + "Fly von �a" + p2.getName() + "�e deaktiviert.");
					}
					
				}
				
				
			} else {
				p.sendMessage(Data.KeineRechte);
			}
		}
		return true;
	}
}