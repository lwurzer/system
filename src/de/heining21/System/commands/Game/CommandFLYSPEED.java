package de.heining21.System.commands.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandFLYSPEED implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("flyspeed")
				&& cs.hasPermission("System.speed")) {
			Player p = (Player) cs;
			if (args.length == 0) {
				cs.sendMessage("�eVerwendung: �a/FlySpeed <1,2,3,4,5,6,7,8,9,10>");
			} else if (args[0].equalsIgnoreCase("0")) {
				p.setFlySpeed(0F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a0�e.");
			} else if (args[0].equalsIgnoreCase("1")) {
				p.setFlySpeed(0.1F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a1�e.");
			} else if (args[0].equalsIgnoreCase("2")) {
				p.setFlySpeed(0.2F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a2�e.");
			} else if (args[0].equalsIgnoreCase("3")) {
				p.setFlySpeed(0.3F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a3�e.");
			} else if (args[0].equalsIgnoreCase("4")) {
				p.setFlySpeed(0.4F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a4�e.");
			} else if (args[0].equalsIgnoreCase("5")) {
				p.setFlySpeed(0.5F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a5�e.");
			} else if (args[0].equalsIgnoreCase("6")) {
				p.setFlySpeed(0.6F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a6�e.");
			} else if (args[0].equalsIgnoreCase("7")) {
				p.setFlySpeed(0.7F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a7�e.");
			} else if (args[0].equalsIgnoreCase("8")) {
				p.setFlySpeed(0.8F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a8�e.");
			} else if (args[0].equalsIgnoreCase("9")) {
				p.setFlySpeed(0.9F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a9�e.");
			} else if (args[0].equalsIgnoreCase("10")) {
				p.setFlySpeed(1F);
				cs.sendMessage(Data.p + "�eDein Fly-Speed ist nun �a10�e.");
			} else if (args.length != 0) {
				cs.sendMessage(Data.p + "�cFalsche Verwendung dieses Commands!");
				cs.sendMessage(Data.p + "�eRICHIGE Verwendung: �a/FlySpeed <1,2,3,4,5,6,7,8,9,10>");
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}