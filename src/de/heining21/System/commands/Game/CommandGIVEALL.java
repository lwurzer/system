package de.heining21.System.commands.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandGIVEALL implements CommandExecutor {
	public Main plugin;

	public CommandGIVEALL(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("giveall")
				&& cs.hasPermission("System.giveall")) {
			for (Player all : plugin.getServer().getOnlinePlayers()) {
				if (p.getItemInHand().getAmount() == 0) {
					p.sendMessage(Data.p + "�eDu kannst niemanden �aLuft �evergeben!");
				} else {
					all.sendMessage(Data.p + "�eAllen Spielern wurde �a"
							+ p.getItemInHand().getAmount()
							+ "�e mal das Item �2"
							+ p.getItemInHand().getType() + "�e gegeben.");
					all.getInventory().addItem(p.getItemInHand());
				}
			}
		} else {
			p.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}