package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandGM implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("gm") && cs.hasPermission("System.gm")) {
			Player p = (Player) cs;
			if (args.length == 0) {
				cs.sendMessage("�eVerwendung: �a/gm <0, 1, 2> <Spieler>");
			} else if (args.length == 2) {

				if (Bukkit.getPlayer(args[1]) != null) {
					Player p2 = Bukkit.getPlayer(args[1]);

					if (!cs.hasPermission("System.gm.others")) {
						cs.sendMessage(Data.KeineRechte);
						return true;
					}

					if (args[0].equalsIgnoreCase("0")) {
						p2.setGameMode(GameMode.SURVIVAL);

						p2.playSound(p.getLocation(), Sound.LEVEL_UP, 50.0F,
								1.0F);
						p2.sendMessage(Data.p + "�eDein GameMode wurde auf �a"
								+ p2.getGameMode() + " �egesetzt!");
						p.sendMessage(Data.p + "Du hast den GameMode von �a" + p2.getName() + " �eauf �a" + p2.getGameMode() + "�e ge�ndert.");
					} else if (args[0].equalsIgnoreCase("1")) {
						p2.setGameMode(GameMode.CREATIVE);
						p2.sendMessage(Data.p + "�eDein GameMode wurde auf �a"
								+ p2.getGameMode() + " �egesetzt!");
						p.sendMessage(Data.p + "Du hast den GameMode von �a" + p2.getName() + " �eauf �a" + p2.getGameMode() + "�e ge�ndert.");
						p2.playSound(p.getLocation(), Sound.LEVEL_UP, 50.0F,
								1.0F);
					} else if (args[0].equalsIgnoreCase("2")) {
						p2.setGameMode(GameMode.ADVENTURE);
						p2.sendMessage(Data.p + "�eDein GameMode wurde auf �a"
								+ p2.getGameMode() + " �egesetzt!");
						p.sendMessage(Data.p + "Du hast den GameMode von �a" + p2.getName() + " �eauf �a" + p2.getGameMode() + "�e ge�ndert.");
						p2.playSound(p.getLocation(), Sound.LEVEL_UP, 50.0F,
								1.0F);
					}

				} else {
					cs.sendMessage(Data.UserNotExists);
				}

			} else if (args[0].equalsIgnoreCase("0")) {
				p.setGameMode(GameMode.SURVIVAL);
				cs.sendMessage(Data.p + "�eDein GameMode wurde auf �a"
						+ p.getGameMode() + " �egesetzt!");
				p.playSound(p.getLocation(), Sound.LEVEL_UP, 50.0F, 1.0F);
			} else if (args[0].equalsIgnoreCase("1")) {
				p.setGameMode(GameMode.CREATIVE);
				cs.sendMessage(Data.p + "�eDein GameMode wurde auf �a"
						+ p.getGameMode() + " �egesetzt!");
				p.playSound(p.getLocation(), Sound.LEVEL_UP, 50.0F, 1.0F);
			} else if (args[0].equalsIgnoreCase("2")) {
				p.setGameMode(GameMode.ADVENTURE);
				cs.sendMessage(Data.p + "�eDein GameMode wurde auf �a"
						+ p.getGameMode() + " �egesetzt!");
				p.playSound(p.getLocation(), Sound.LEVEL_UP, 50.0F, 1.0F);
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}
