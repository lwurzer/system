package de.heining21.System.commands.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandHAT implements CommandExecutor {

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("hat")) {
			if (p.hasPermission("System.hat")) {
				if (p.getItemInHand().getTypeId() != 0) {
					
					if (p.getInventory().getHelmet().getType() != null) {
						p.getInventory().addItem(p.getInventory().getHelmet());
						p.getInventory().setHelmet(p.getItemInHand());
						p.getInventory().remove(p.getItemInHand());
						p.sendMessage(Data.p + "Du hast deinen Kopf auf �a" + p.getItemInHand() + " �egesetzt.");
						
					} else {
					
						p.sendMessage(Data.p + "Du hast deinen Kopf auf �a" + p.getItemInHand() + " �egesetzt.");
						p.getInventory().setHelmet(p.getItemInHand());
						p.getInventory().remove(p.getItemInHand());
					}
					
				} else {
					p.sendMessage(Data.p + "�cDu kannst keine Luft aufsetzen.");
				}
			}
		}
		return true;
	}
}
