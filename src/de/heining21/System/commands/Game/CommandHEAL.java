package de.heining21.System.commands.Game;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandHEAL implements CommandExecutor {
	public Main plugin;

	public CommandHEAL(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(final CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("heal")) {
			if (cs.hasPermission("System.heal")) {
				Player p = (Player) cs;
				if (args.length == 0) {
					p.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 1, 100));
					p.setFoodLevel(20);
					p.setFireTicks(0);
					cs.sendMessage(Data.p + "�eDu wurdest erfolgreich �ageheilt�e.");
					p.playSound(p.getLocation(), Sound.LEVEL_UP, 50F, 1F);
				}
				if (cs.hasPermission("System.healother")) {
					if (args.length == 1) {
						
						try {
							Player p1 = (Player) cs;
							Player p2 = p1.getServer().getPlayer(args[0]);
							p2.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 1, 100));
							p2.setFoodLevel(20);
							p2.sendMessage(Data.p + "�eDu wurdest von�a " + p.getName() + " �egeheilt.");
							p2.playSound(p.getLocation(), Sound.LEVEL_UP, 50F, 1F);
							p.sendMessage(Data.p + "Du hast�a " + p2.getName() + " �eerfolgreich geheilt.");
						} catch (NullPointerException e) {
							cs.sendMessage(Data.UserNotExists);
						}

					}
				}
			} else {
				cs.sendMessage(Data.KeineRechte + Data.rang);
			}
		}
		return true;
	}
}