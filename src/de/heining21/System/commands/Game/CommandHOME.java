package de.heining21.System.commands.Game;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import de.heining21.System.Data;
import de.heining21.System.Main;
import de.heining21.System.Money.MoneyUtils;

public class CommandHOME implements CommandExecutor, Listener{

	
	private Main plugin;

	public CommandHOME(Main main){
		this.plugin = main;
	}
	 static ArrayList<String> pToHome = new ArrayList<String>();
	 
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		
		
		
		///Home [name]
		if(cmd.getName().equalsIgnoreCase("home")){
			
			if(cs instanceof ConsoleCommandSender){
				cs.sendMessage(Data.p + "�4Fehler: �cDu kannst das nicht tun");
				return true;
			}
			final String home;
			final Player p = (Player)cs;
			
			File file = MoneyUtils.getUserFile(p);
			new YamlConfiguration();
			final FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			
			if(args.length == 0) {
				home = "home";
			}else{
				home = args[0].toLowerCase();
			}
			
			ConfigurationSection publicSection = cfg.getConfigurationSection("homes");
			StringBuilder homes = new StringBuilder();
			int lenght = 0;
			//Public Warps
			try{
				for (String home1 : publicSection.getKeys(false)) {
					if (!homes.toString().isEmpty()) homes.append(", ");
					homes.append(home1);
					lenght++;
				}
			}catch(NullPointerException e){
				p.sendMessage(Data.p + "Du hast noch keine Homes gesetzt. Nutze �6/sethome");
				return true;
			}
			
			
			
			
			if(homes.toString().isEmpty()){
				
				
				
			}
			
			if(args.length == 0){
				if(lenght > 1){
					
					
					p.sendMessage(Data.p + "Du hast folgende Homes: �6" + homes.toString());
					p.sendMessage(Data.p + "�7Benutze: /home <name>");
					return true;
					
				}
				
				
			pToHome.add(p.getName());
			p.sendMessage(Data.p + "Du wirst in �63 Sekunden�e teleportiert. Bewege dich nicht!");
				
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

					public void run() {
						
						if(pToHome.contains(p.getName())){
							Location loc = p.getLocation();
							
							loc.setX(cfg.getDouble("homes." + home + ".x"));
							loc.setY(cfg.getDouble("homes." + home + ".y"));
							loc.setZ(cfg.getDouble("homes." + home + ".z"));
							loc.setYaw(cfg.getLong("homes." + home + ".yaw"));
							loc.setPitch(cfg.getLong("homes." + home + ".pitch"));
							World world = Bukkit.getWorld(cfg.getString("homes." + home + ".world"));
							loc.setWorld(world);
							
							p.teleport(loc);
							pToHome.remove(p.getName());
							
							p.sendMessage(Data.p + "Du wurdest zum �6Home �eteleportiert.");
							return;
						}
						
					}
				}, 60L);
				return true;
				
				
			}else if(args.length == 1){
				
				if(lenght == 0){
					
					p.sendMessage(Data.p + "Du hast noch keine Homes gesetzt. Nutze �6/sethome");
					return true;
					
				}
				
				
				
				if(!homes.toString().contains(ChatColor.stripColor(home))){
					
					p.sendMessage(Data.p + "Du hast dieses Home nicht gesetzt");
					p.sendMessage(Data.p + "Du hast folgende Homes: �6" + homes.toString());
					p.sendMessage(Data.p + "�7Benutze: /sethome " + home);
					
					return true;
					
				}else{
					
					p.sendMessage(Data.p + "Du wirst in �63 Sekunden�e teleportiert. Bewege dich nicht!");
					pToHome.add(p.getName());
					
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						
						public void run() {
							
							if(pToHome.contains(p.getName())){
								Location loc = p.getLocation();
								
								loc.setX(cfg.getDouble("homes." + home + ".x"));
								loc.setY(cfg.getDouble("homes." + home + ".y"));
								loc.setZ(cfg.getDouble("homes." + home + ".z"));
								loc.setYaw(cfg.getLong("homes." + home + ".yaw"));
								loc.setPitch(cfg.getLong("homes." + home + ".pitch"));
								World world = Bukkit.getWorld(cfg.getString("homes." + home + ".world"));
								loc.setWorld(world);
								
								p.teleport(loc);
								pToHome.remove(p.getName());
								
								p.sendMessage(Data.p + "Du wurdest zum Home (�6" + home + "�e) teleportiert");
								return;
							}
							
						}
					}, 60L);
					return true;
					
					
				}
				
			}
			
			
			
			
			
		}
		//sethome <name>
		if(cmd.getName().equalsIgnoreCase("sethome")){
			
			if(cs instanceof ConsoleCommandSender){
				cs.sendMessage(Data.p + "�4Fehler: �cDu kannst das nicht tun");
				return true;
			}
			
			Player p = (Player)cs;
			File file = MoneyUtils.getUserFile(p);
			new YamlConfiguration();
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			
			
			
			ConfigurationSection publicSection = cfg.getConfigurationSection("homes");
			StringBuilder homes = new StringBuilder();
			int lenght = 0;
			try{
				for (String home1 : publicSection.getKeys(false)) {
					if (!homes.toString().isEmpty()) homes.append(", ");
					homes.append(home1);
					lenght++;
				}
			}catch(NullPointerException e){
				
			}
			
			
			
			if(getMaxHomes(p) < lenght + 1 && getMaxHomes(p) != 0){
				p.sendMessage(Data.p + "Du hast das Limit erreicht");
				return true;
			}
			
			String home;
			
			
			
			
			if(args.length == 0){
				home = "home";
			}else{
				home = args[0].toLowerCase();
			}
			
			Location loc = p.getLocation();
			
			double X = loc.getX();
			double Y = loc.getY();
			double Z = loc.getZ();
			float Yaw = loc.getYaw();
			float Pitch = loc.getPitch();
			String worldname = loc.getWorld().getName();
			
			
			cfg.set("homes." + home + ".world", worldname);
			cfg.set("homes." + home + ".x", X);
			cfg.set("homes." + home + ".y", Y);
			cfg.set("homes." + home + ".z", Z);
			cfg.set("homes." + home + ".yaw", Yaw);
			cfg.set("homes." + home + ".pitch", Pitch);
			
			try {
				cfg.save(file);
				p.sendMessage(Data.p + "Das Home �6" + home + "�e wurde erfolgreich gesetzt!");
				return true;
				
				
				
			} catch (IOException e) {
				p.sendMessage(Data.p + "�4Fehler: �c Wende dich an einen Developer oder Admin.");
				return true;
			}
			
		}
		//delhome <name>
		if(cmd.getName().equalsIgnoreCase("delhome")){
			
			if(cs instanceof ConsoleCommandSender){
				cs.sendMessage(Data.p + "�4Fehler: �cDu kannst das nicht tun");
				return true;
			}
			String home;
			Player p = (Player)cs;
			
			File file = MoneyUtils.getUserFile(p);
			new YamlConfiguration();
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			
			ConfigurationSection publicSection = cfg.getConfigurationSection("homes");
			StringBuilder homes = new StringBuilder();
			@SuppressWarnings("unused")
			int lenght = 0;
			//Public Warps
			
			for (String home1 : publicSection.getKeys(false)) {
				if (!homes.toString().isEmpty()) homes.append(", ");
				homes.append(home1);
				lenght++;
			}
			if(args.length == 0){
				home = "home";
			}else{
				home = args[0].toLowerCase();
			}
			//try{
				
				if(!publicSection.contains(home)){
					
					p.sendMessage(Data.p + "Dieses Home existiert nicht. Erstelle es mit");
						p.sendMessage(Data.p + "/sethome " + home);
						p.sendMessage(Data.p + "Du hast folgende Homes: �6" + homes.toString());
					return true;
					
				}
			//}catch(NullPointerException e){
			//	p.sendMessage(Data.p + "Dieses Home existiert nicht. Erstelle es mit");
			//	p.sendMessage(Data.p + "/sethome " + home);
			//	p.sendMessage(Data.p + "Du hast folgende Homes: �6" + homes.toString());
			//	return true;
			//}
			
				
				
				
			
			cfg.set("homes." + home + ".world", null);
			cfg.set("homes." + home + ".x", null);
			cfg.set("homes." + home + ".y", null);
			cfg.set("homes." + home + ".z", null);
			cfg.set("homes." + home + ".yaw", null);
			cfg.set("homes." + home + ".pitch", null);
			cfg.set("homes." + home, null);
			try {
				cfg.save(file);
				p.sendMessage(Data.p + "Home erfolgreich gel�scht.");
				return true;
			} catch (IOException e) {
			
			}
			
		}
		return false;
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		
		if(e.getFrom().getX() == e.getTo().getX() && e.getFrom().getY() == e.getTo().getY() && e.getFrom().getZ() == e.getTo().getZ()){
			
			return;
			
		}
		
		if(pToHome.contains(e.getPlayer().getName())){
			
			pToHome.remove(e.getPlayer().getName());
			e.getPlayer().sendMessage(Data.p + "Der Teleport wurde abgebrochen, da du dich bewegt hast.");
			
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public static void setupConfig(){
		
		File file = new File("plugins/System", "config.yml");
		
		if(!file.exists()){
			
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			
			cfg.options().copyDefaults(true);
			for(PermissionGroup group : PermissionsEx.getPermissionManager().getGroups()){
				
				cfg.addDefault(group.getName() + ".homes.max", 3);
				
			}
			try {
				cfg.save(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	public static int getMaxHomes(Player p){
		
		File file = new File("plugins/System", "config.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		if(p.hasPermission("System.Home.Unlimited")){
			return 0;
		}
		
		@SuppressWarnings("deprecation")
		String groupname = PermissionsEx.getPermissionManager().getUser(p).getGroups()[0].getName();
		
		int max = cfg.getInt(groupname + ".homes.max");
		
		return max;
	}
	
}
