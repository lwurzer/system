package de.heining21.System.commands.Game;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import de.heining21.System.Data;

public class CommandINVSEE implements CommandExecutor, Listener {
	
	private ArrayList<String> invcontains = new ArrayList<String>();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Player p = (Player) cs;
		Player p2 = Bukkit.getPlayer(args[0]);
		if (cmd.getName().equalsIgnoreCase("invsee")) {
			if (p.hasPermission("Systen.invsee")) {
				if (args.length == 0) {
					p.sendMessage(Data.p + "Verwendung: �a/invsee <Spieler>");
				}
				
				if (args.length == 1) {
					p.openInventory(p2.getInventory());
					invcontains.add(p.getName());
					p.sendMessage(Data.p + "Du hast von �a" + p2.getName() + " �edas Inventar ge�ffnet.");
					p.playSound(p.getLocation(), Sound.ANVIL_USE, 50F, 1F);
				}
			} else {
				p.sendMessage(Data.KeineRechte + Data.rang);
			}
		}
		return true;
	}
	
	@EventHandler
	public void onKlick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (invcontains.contains(p.getName())) {
			if (!p.hasPermission("System.invsee.canklick")) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		Player p = (Player) e.getPlayer();
		if (invcontains.contains(p.getName())) {
			invcontains.remove(p.getName());
		}
	}
}