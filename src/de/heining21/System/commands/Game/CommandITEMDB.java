package de.heining21.System.commands.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandITEMDB implements CommandExecutor {

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("itemdb") && cs.hasPermission("System.itemdb")) {
			Player p = (Player) cs;
			p.sendMessage(Data.p + "Block in deiner Hand: �a" + p.getItemInHand().getTypeId());
		}
		return true;
	}
}