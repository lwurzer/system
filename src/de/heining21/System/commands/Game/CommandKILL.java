package de.heining21.System.commands.Game;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandKILL implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("kill")
				&& cs.hasPermission("System.kill")) {
			Player p = (Player) cs;
			if (args.length == 0) {
				p.sendMessage("�eVerwendung: �a/Kill <Spieler>");
			}
			if (args.length == 1) {

				try {
					Player p2 = p.getServer().getPlayer(args[0]);
					p2.setHealth(0D);
					p.playSound(p.getLocation(), Sound.LEVEL_UP, 50F, 1F);
					p.sendMessage(Data.p + "�eDu hast �a" + p2.getName() + "�e via Command get�tet!");
					p2.playSound(p.getLocation(), Sound.LEVEL_UP, 50F, 1F);
					p2.sendMessage(Data.p + "�eDu wurdest von �a" + p.getName() + "�e via Command get�tet!");
					return true;
				} catch (NullPointerException e) {
					
					cs.sendMessage(Data.UserNotExists);
					return true;
				}
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
			return true;
		}
		return true;
	}
}