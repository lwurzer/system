package de.heining21.System.commands.Game;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.CreatureType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;

import de.heining21.System.Data;
import de.heining21.System.Main;

@SuppressWarnings("deprecation")
public class CommandMOBEVENT implements CommandExecutor {

	public static void CreateDir() {
		File check = new File(path);
		if (!check.isDirectory()) {
			check.mkdirs();
		}
	}

	public static void CreateFile() {
		try {
			CreateDir();
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String path = "plugins/System/Locations/Events";

	static File file = new File(path, "MobEvent.yml");
	static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

	public Main plugin;

	public CommandMOBEVENT(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		final Player p = (Player) cs;

		if (cmd.getName().equalsIgnoreCase("mobevent")
				&& cs.hasPermission("System.mobevent")) {
			if (args.length == 0) {

				p.sendMessage(Data.p
						+ "�eVerwendung: �a/MobEvent | ME <start, setspawn, setmobspawn1-20 >");

			} else if (args[0].equalsIgnoreCase("start")) {

				Bukkit.broadcastMessage(Data.pfett
						+ "In 2 Minuten startet das �a�lMobEvent�f.");
				Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin,
						new Runnable() {
							public void run() {
								Bukkit.broadcastMessage(Data.pfett
										+ "In 1:30 Minuten startet das �a�lMobEvent�f.");
								Bukkit.getScheduler().scheduleAsyncDelayedTask(
										plugin, new Runnable() {
											public void run() {
												Bukkit.broadcastMessage(Data.pfett
														+ "In 1 Minute startet das �a�lMobEvent�f.");
												Bukkit.getScheduler()
														.scheduleAsyncDelayedTask(
																plugin,
																new Runnable() {
																	public void run() {
																		Bukkit.broadcastMessage(Data.pfett
																				+ "In 30 Sekunden startet das �a�lMobEvent�f.");
																		Bukkit.dispatchCommand(
																				p,
																				"globalmute an");
																		Bukkit.getScheduler()
																				.scheduleAsyncDelayedTask(
																						plugin,
																						new Runnable() {
																							public void run() {
																								Bukkit.broadcastMessage(Data.pfett
																										+ "In 10 Sekunden startet das �a�lMobEvent�f.");
																								Bukkit.getScheduler()
																										.scheduleAsyncDelayedTask(
																												plugin,
																												new Runnable() {
																													public void run() {
																														Bukkit.broadcastMessage(Data.pfett
																																+ "Das �a�lMobEvent �e�lbeginnt.");
																														Bukkit.broadcastMessage("�c�l      ____  INFOS | REGELN   ____");
																														Bukkit.broadcastMessage("�b�lEs werden gleich viele Monster um dich Spawnen. Diese musst du versuchen zu t�ten und gleichzeitig darfst du nicht von der Haupt-Platform fallen.");
																														Bukkit.broadcastMessage("�3�lWer am l�ngsten oben bleiben kann ist der Gewinner.");
																														Bukkit.broadcastMessage("�c�l      ____  INFOS | REGELN   ____");

																														Location loc = p
																																.getLocation();

																														double x = cfg
																																.getDouble("Spawn.X");
																														double y = cfg
																																.getDouble("Spawn.Y");
																														double z = cfg
																																.getDouble("Spawn.Z");
																														float yaw = (float) cfg
																																.getDouble("Spawn.Yaw");
																														float pitch = (float) cfg
																																.getDouble("Spawn.Pitch");
																														String worldname = cfg
																																.getString("Spawn.World");

																														loc.setX(x);
																														loc.setY(y);
																														loc.setZ(z);
																														loc.setYaw(yaw);
																														loc.setPitch(pitch);
																														loc.setWorld(Bukkit
																																.getWorld(worldname));

																														for (Player all : p
																																.getServer()
																																.getOnlinePlayers()) {
																															all.teleport(loc);
																														}

																														Bukkit.getScheduler()
																																.scheduleAsyncDelayedTask(
																																		plugin,
																																		new Runnable() {
																																			public void run() {
																																				Bukkit.broadcastMessage(Data.pfett
																																						+ "Die ersten Mobs spawnen in �a�l30 Sekunden.");
																																				Bukkit.getScheduler()
																																						.scheduleAsyncDelayedTask(
																																								plugin,
																																								new Runnable() {
																																									public void run() {

																																										Location mobloc = new Location(
																																												Bukkit.getWorld("world"),
																																												cfg.getDouble("MobSpawn1.X"),
																																												cfg.getDouble("MobSpawn1.Y"),
																																												cfg.getDouble("MobSpawn1.Z"));

																																										Skeleton s = (Skeleton) p
																																												.getWorld()
																																												.spawnCreature(
																																														mobloc,
																																														CreatureType.SKELETON);
																																										s.setFireTicks(20 * 10);
																																									}
																																								},
																																								600L);
																																			}
																																		},
																																		200L);
																													}
																												},
																												200L);
																							}
																						},
																						400L);
																	}
																}, 600L);
											}
										}, 600L);
							}
						}, 600L);
			} else if (args[0].equalsIgnoreCase("setspawn")) {

				cfg.set("Spawn.X", p.getLocation().getX());
				cfg.set("Spawn.Y", p.getLocation().getY());
				cfg.set("Spawn.Z", p.getLocation().getZ());
				cfg.set("Spawn.Yaw", p.getLocation().getYaw());
				cfg.set("Spawn.Pitch", p.getLocation().getPitch());
				cfg.set("Spawn.World", p.getWorld().getName());

				try {
					cfg.save(file);
					p.sendMessage(Data.pfett
							+ "�e�lDu hast den �e�lMobEvent-Spawn �e�lgesetzt.");
				} catch (IOException e) {
					e.printStackTrace();
					p.sendMessage(Data.pfett
							+ "�4Error: Wende dich an einen Admin/Dev");
				}

			} else if (args[0].equalsIgnoreCase("setmobspawn1")) {

				cfg.set("MobSpawn1.X", p.getLocation().getX());
				cfg.set("MobSpawn1.Y", p.getLocation().getY());
				cfg.set("MobSpawn1.Z", p.getLocation().getZ());
				try {
					cfg.save(file);
					p.sendMessage(Data.pfett
							+ "�e�lDu hast den �a�lMob-Spawn-Punkt 1 �e�lgesetzt.");
				} catch (IOException e) {
					e.printStackTrace();
					p.sendMessage(Data.pfett
							+ "�cWende dich an einen Admin/Dev");
				}
			}
		}
		return true;
	}
}