package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Money.MoneyUtils;

public class CommandMONEY implements CommandExecutor {

	// initialize strings
	String pname = null;

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("money")) {
			if (args.length == 0) {
				if (!(cs instanceof Player)) {
					cs.sendMessage(Data.p
							+ "�eVerwendung: �a/Money <Spieler>");
					return true;
				} else if (cs instanceof Player) {
					Player p = (Player) cs;
					int money = MoneyUtils.getUserMoney(p);
					p.sendMessage("�a�l=================");
					p.sendMessage("�2�lKontoStand: �c" + money + "�");
					p.sendMessage("�a�l=================");
					return true;
				}
			} else if (args.length == 1) {

				try {
					Player p1 = Bukkit.getPlayer(pname);

					int money = MoneyUtils.getUserMoney(p1);
					cs.sendMessage("�a�l=================");
					cs.sendMessage("�2�lKontoStand von �b" + pname + " �c"
							+ money + "�");
					cs.sendMessage("�a�l=================");
					return true;

				} catch (NullPointerException e) {
					cs.sendMessage(Data.p
							+ "�cDer Spieler ist nicht online oder war noch nie auf den Server.");
					return true;
				}

			}
		}

		if (cmd.getName().equalsIgnoreCase("pay")) {
			if (cs instanceof Player) {
				if (args.length == 2) {

					Player pSend = (Player) cs;
					String pSendName = pSend.getName();

					try {

						Player pRec = Bukkit.getPlayer(args[0]);
						String pRecName = pRec.getName();
						int amount;

						if (pSendName.equalsIgnoreCase(pRecName)) {
							cs.sendMessage(Data.p
									+ "�cDu kannst dir selbst kein Geld geben.");
							return true;
						}

						try {
							amount = Integer.parseInt(args[1]);

							if (amount <= 0) {
								cs.sendMessage(Data.p
										+ "�cDer Betrag muss gr�sser als 0 sein.");
								return true;
							}
							MoneyUtils.payToUser(pSend, pRec, amount);

							pSend.sendMessage(Data.p
									+ "�eDu hast �a"
									+ amount
									+ "� �ean �a"
									+ pRecName
									+ "�e bezahlt. Dein Konostand betr�gt nun �a"
									+ MoneyUtils.getUserMoney(pSend)
									+ "��e.");
							pRec.sendMessage(Data.p
									+ "�eDu hast �a"
									+ amount
									+ "� �evon �a"
									+ pSendName
									+ "�e bekommen. Dein Kontostand betr�gt nun �a"
									+ MoneyUtils.getUserMoney(pRec)
									+ "��e.");
							return true;

						} catch (NumberFormatException e) {
							pSend.sendMessage(Data.p
									+ "�cDer Betrag muss eine Zahl sein.");
							return true;
						}

					} catch (NullPointerException e) {
						pSend.sendMessage(Data.p
								+ "�cDer Spieler ist nicht online oder war noch nie auf den Server.");
						return true;
					}

				} else if (args.length != 2) {

					cs.sendMessage(Data.p + "�cZu wenig Argumente.");
					return false;

				}
			} else {
				cs.sendMessage(Data.p + "�cDu kannst das nicht tun.");
				return true;
			}
		}
		if (cmd.getName().equals("eco")) {
			// /eco [set/add/reset/take]
			// /eco set spieler amount
			if (!cs.hasPermission("System.eco")) {
				cs.sendMessage(Data.KeineRechte);
				return true;
			}
			if (args.length == 0) {
				cs.sendMessage(Data.p + "�4Fehler: Zu Wenig Argumente");
				return false;
			}

			if (args.length == 1) {
				if (args[0].equalsIgnoreCase("set")) {
					cs.sendMessage(Data.p
							+ "�eVerwendung:�a /eco set <Spieler> <Anzahl>");
					return true;
				} else if (args[0].equalsIgnoreCase("add")) {
					cs.sendMessage(Data.p
							+ "�eVerwendung:�a /eco add <Spieler> <Anzahl>");
					return true;

				} else if (args[0].equalsIgnoreCase("reset")) {
					cs.sendMessage(Data.p + "�eVerwendung:�a /eco reset <Spieler>");
					return true;

				} else if (args[0].equalsIgnoreCase("take")) {
					cs.sendMessage(Data.p
							+ "�eVerwendung:�a /eco take <Spieler> <Anzahl>");
					return true;
				}

			} else if (args.length == 2) {
				// INFO RESET
				if (args[0].equalsIgnoreCase("reset")) {

					try {
						Player p = Bukkit.getPlayer(args[1]);
						MoneyUtils.resetUserMoney(p);
						cs.sendMessage(Data.p + "�eDas Konto von �a"
								+ p.getName() + "�e wurde auf �a"
								+ MoneyUtils.getUserMoney(p)
								+ "� �ezur�ckgesetzt.");
						p.sendMessage(Data.p + "�eDein Konto wurde von�a "
								+ cs.getName() + "�e auf �a"
								+ MoneyUtils.getUserMoney(p)
								+ "��e zur�ckgesetzt.");
						return true;
					} catch (NullPointerException e) {
						cs.sendMessage(Data.p
								+ "�cDer Spieler ist nicht online oder war noch nie auf den Server.");
						return true;
					}

				} else if (args[0].equalsIgnoreCase("set")) {
					cs.sendMessage(Data.p
							+ "�eVerwendung:�a /eco set <Spieler> <Anzahl>");
					return true;
				} else if (args[0].equalsIgnoreCase("add")) {
					cs.sendMessage(Data.p
							+ "�eVerwendung:�a /eco add <Spieler> <Anzahl>");
					return true;
				} else if (args[0].equalsIgnoreCase("take")) {
					cs.sendMessage(Data.p
							+ "�eVerwendung:�a /eco take <Spieler> <Anzahl>");
					return true;
				}
			} else if (args.length == 3) {
				if (args[0].equalsIgnoreCase("reset")) {
					cs.sendMessage(Data.p + "�cZu viele Argumente");
					return true;
				}
				// INFO ADD
				if (args[0].equalsIgnoreCase("add")) {

					try {
						int anzahl = Integer.parseInt(args[2]);
						Player p = Bukkit.getPlayer(args[1]);

						if (anzahl <= 0) {
							cs.sendMessage(Data.p
									+ "�cBetrag muss gr��er als 0 sein.");
							return true;
						}
						MoneyUtils.addUserMoney(p, anzahl);
						cs.sendMessage(Data.p + "�eDu hast �a" + p.getName()
								+ "�e zu seinem Konto�a " + anzahl
								+ "��e hinzugef�gt. Er hat nun �a"
								+ MoneyUtils.getUserMoney(p) + "��e.");
						p.sendMessage(Data.p
								+ "�a"
								+ cs.getName()
								+ "�e hat �a"
								+ anzahl
								+ "�e zu deinem Konto hinzugef�gt. Dein Kontostand betr�gt nun �a"
								+ MoneyUtils.getUserMoney(p) + "��e.");
						return true;

					} catch (NullPointerException e) {
						cs.sendMessage(Data.UserNotExists);
						return true;
					} catch (NumberFormatException e) {
						cs.sendMessage(Data.p
								+ "�cBetrag muss eine Zahl sein.");
						return true;
					}

				} else if (args[0].equalsIgnoreCase("set")) {

					try {

						int amount = Integer.parseInt(args[2]);
						Player p = Bukkit.getPlayer(args[1]);

						if (amount <= 0) {
							cs.sendMessage(Data.p
									+ "�cBetrag muss gr�sser als 0 sein.");
							return true;
						}

						MoneyUtils.setUserMoney(p, amount);
						cs.sendMessage(Data.p + "�eDer Kontostand von �a"
								+ p.getName() + "�e wurde auf �a" + amount
								+ "��e gesetzt.");
						p.sendMessage(Data.p + "�eDein Kontostand wurde von �a"
								+ cs.getName() + "�e auf �a" + amount
								+ "��e gesetzt.");
						return true;

					} catch (NullPointerException e) {
						cs.sendMessage(Data.UserNotExists);
						return true;
					} catch (NumberFormatException e) {
						cs.sendMessage(Data.p
								+ "�cBetrag muss eine Zahl sein.");
						return true;
					}

				} else if (args[0].equalsIgnoreCase("take")) {
					try {
						int amount = Integer.parseInt(args[2]);
						Player p = Bukkit.getPlayer(args[1]);

						if (amount <= 0) {
							cs.sendMessage(Data.p
									+ "�cBetrag muss gr�sser als 0 sein.");
							return true;
						}

						MoneyUtils.takeUserMoney(p, amount);
						cs.sendMessage(Data.p + "�eDu hast �a" + p.getName()
								+ "�e vom Konto �a" + amount
								+ "��e enfernt. Sein Kontostand betr�gt nun �a"
								+ MoneyUtils.getUserMoney(p) + "��e.");
						cs.sendMessage(Data.p
								+ "�a"
								+ cs.getName()
								+ "�2 hat �a"
								+ amount
								+ "��e von deinem Konto entfernt. Dein Kontostand betr�gt nun �a"
								+ MoneyUtils.getUserMoney(p) + "��e.");

						return true;

					} catch (NullPointerException e) {
						cs.sendMessage(Data.UserNotExists);
						return true;
					} catch (NumberFormatException e) {
						cs.sendMessage(Data.p
								+ "�cBetrag muss eine Zahl sein.");
						return true;
					}

				}
			}

		}

		return false;
	}

}
