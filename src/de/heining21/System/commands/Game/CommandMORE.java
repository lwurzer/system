package de.heining21.System.commands.Game;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandMORE implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("more")) {
			if (!cs.hasPermission("System.more")) {
				p.sendMessage(Data.KeineRechte);
			}
			if (p.getItemInHand().getType() == Material.AIR) {
				p.sendMessage(Data.p + "�cDu kannst keine Luft stacken.");
			} else {
				p.getItemInHand().setAmount(64);
				p.sendMessage(Data.p + "'Das Item in deiner Hand wurde zu 64 gemacht.");
			}
			
		}
		return true;
	}
}