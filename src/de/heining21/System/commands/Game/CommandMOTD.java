package de.heining21.System.commands.Game;

import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.heining21.System.Data;
import de.heining21.System.Main;
import de.heining21.System.ServerListe.ProtocolLib;

public class CommandMOTD implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("motd")) {

			if (args.length == 0) {
				if (!cs.hasPermission("System.motd")
						|| !cs.hasPermission("System.motd.admin")) {
					cs.sendMessage(Data.KeineRechte);
					return true;
				}
				
				if (cs.hasPermission("System.motd")) {
					cs.sendMessage(Data.ZuWenigArgs);
					cs.sendMessage("�eVerwendung: �a/Motd <get|set> <line1|line2> >");
					return true;
				}

			} else if (args.length == 1) {
				if (args[0].equalsIgnoreCase("get")) {

					cs.sendMessage(Data.p + "�eZeile1: �r" + Main.Modt1);
					cs.sendMessage(Data.p + "�eZeile2: �r" + Main.Motd2);
					return true;

				} else {
					if (!cs.hasPermission("System.motd")) {
						cs.sendMessage(Data.KeineRechte);
						return true;
					} else {
						cs.sendMessage(Data.ZuWenigArgs);
						cs.sendMessage("�eVerwendung: �a/Motd <get|set> <line1|line2> >");
						return true;
					}

				}
			} else if (args.length == 2) {

				if (args[0].equalsIgnoreCase("set")) {
					if (!cs.hasPermission("System.motd")) {
						cs.sendMessage(Data.KeineRechte);
						return true;
					} else {
						cs.sendMessage(Data.ZuWenigArgs);
						cs.sendMessage("�eVerwendung: �a/Motd set line1 <MOTD>");
						cs.sendMessage("�eVerwendung: �a/Motd set line2 <MOTD>");
						return true;
					}
				}

			} else if (args.length >= 3) {

				if (args[1].equalsIgnoreCase("line1")
						|| args[1].equalsIgnoreCase("line2")) {

					String Message;

					StringBuffer result = new StringBuffer();
					for (int i = 2; i < args.length; i++) {
						result.append(args[i]);
						result.append(" ");
					}
					Message = result.toString();

					if (args[1].equalsIgnoreCase("line1")) {

						ProtocolLib.cfg.set("MotdLine1", Message);
						try {
							ProtocolLib.cfg.save(ProtocolLib.file);
						} catch (IOException e) {

							e.printStackTrace();
						}
						Main.loadMotd();
						cs.sendMessage(Data.p + "�8Linie 1:�r " + Main.Modt1);
						return true;
					}
					if (args[1].equalsIgnoreCase("line2")) {

						ProtocolLib.cfg.set("ModtLine2", Message);
						try {
							ProtocolLib.cfg.save(ProtocolLib.file);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Main.loadMotd();
						cs.sendMessage(Data.p + "�8Linie 2:�r " + Main.Motd2);
						return true;

					}

					return true;
				} else {						
				cs.sendMessage("�eVerwendung: �a/Motd set line1 <MOTD>");
				cs.sendMessage("�eVerwendung: �a/Motd set line2 <MOTD>");
					return true;
				}

			}

		}

		return false;
	}

}
