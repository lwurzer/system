package de.heining21.System.commands.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandPTIME implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("ptime")) {
			if (p.hasPermission("System.ptime")) {
				if (args.length == 0) {
					p.sendMessage(Data.p + "�eVerwendung: �a/PTime <Day, Night>");
				}
				
				if (args.length == 1) {
					
					if (args[0].equalsIgnoreCase("day")) {
						p.setPlayerTime(0, true);
						p.sendMessage(Data.p + "Du hast deine eigene Zeit auf �aTag �eumge�ndert.");
					}
					
					if (args[0].equalsIgnoreCase("night")) {
						p.setPlayerTime(13000, true);
						p.sendMessage(Data.p + "Du hast deine eigene Zeit auf �aNacht �eumge�ndert.");
					}
					
					if (args[0].equalsIgnoreCase("reset")) {
						p.setPlayerTime(p.getWorld().getTime(), true);
						p.sendMessage(Data.p + "Du hast deine eigene Zeit auf �aGlobal �eumge�ndert.");
					}
					
				}
			} else {
				p.sendMessage(Data.KeineRechte + Data.rang);
			}
		}
		return true;
	}
}
