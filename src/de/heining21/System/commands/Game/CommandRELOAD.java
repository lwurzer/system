package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import de.heining21.Helfer.TitleUtils;
import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandRELOAD implements CommandExecutor {

	public Main plugin;

	public CommandRELOAD(Main plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		final Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("rl")) {
			if (p.hasPermission("System.reload")) {
				
				if (args.length == 0) {
					Bukkit.broadcastMessage("�4�l�m-----------------------------------");
					Bukkit.broadcastMessage("�c�lDer Server wird in 20 Sekunden reloaded.");
					Bukkit.broadcastMessage("�c�lEs wird gleich zu Lags kommen. Bitte den PvP-Bereich verlassen.");
					Bukkit.broadcastMessage("�4�l�m-----------------------------------");
					
					TitleUtils.sendTimings(p, 20, 40, 20);
					TitleUtils.sendTitle(p, "�f");
					TitleUtils.sendSubTitle(p, "�c> Reload in 20 Sekunden <");
					
					PlaySound();
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							Bukkit.broadcastMessage(Data.pfett + "�c�lDer Server wird in �4�l10 �c�lSekunden reloadet.");
							
							TitleUtils.sendTimings(p, 20, 40, 20);
							TitleUtils.sendTitle(p, "�f");
							TitleUtils.sendSubTitle(p, "�c> Reload in 10 Sekunden <");
							
							PlaySound();
							Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								public void run() {
									Bukkit.broadcastMessage(Data.pfett + "�4�lDer Server wird jetzt reloadet. => Lags");
									PlaySound();
									
									TitleUtils.sendTimings(p, 20, 40, 20);
									TitleUtils.sendTitle(p, "�f");
									TitleUtils.sendSubTitle(p, "�c> Jetzt wird Reloaded <");
									
									Bukkit.reload();
									Bukkit.broadcastMessage(Data.pfett + "�c�lAlle Plugins wurden erfolgreich neu geladen.");
									PlaySound();
								}
							}, 200L);
						}
					}, 200L);
				}
			} else {
				p.sendMessage(Data.KeineRechte);
			}
		}
		return true;
	}
	
	public void PlaySound() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.playSound(p.getLocation(), Sound.SHOOT_ARROW, 50F, 1F);
		}
	}
	
}
