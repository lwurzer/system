package de.heining21.System.commands.Game;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.heining21.System.Data;

public class CommandRENAME implements CommandExecutor {
	@SuppressWarnings("unchecked")
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		Player p = (Player) sender;
		if (cmd.getName().equals("rename")) {
			if (p.hasPermission("System.rename")) {
				if (sender instanceof Player) {
					if (p.getItemInHand() != null
							&& !p.getItemInHand().getType()
									.equals(Material.AIR)) {
						if (args.length == 1) {
							ItemStack itemStack = p.getItemInHand();
							
							String string = args[0];
							String spacedString = string.replaceAll("_", " ");
							String coloredString = ChatColor
									.translateAlternateColorCodes('&',
											spacedString);

							ItemMeta itemStackMeta = itemStack.getItemMeta();
							itemStackMeta.setDisplayName(coloredString);

							itemStack.setItemMeta(itemStackMeta);

							p.sendMessage(Data.p + "�fDas Item hei�t jetzt: "
									+ ChatColor.RESET + coloredString
									+ ChatColor.GOLD + "");
						} else {
							p.sendMessage("�eVerwendung: �a/Rename <Name>");
							p.sendMessage("�7Hinweis: F�r Leerzeichen benutze _");
						}
					} else {
						p.sendMessage(Data.p
								+ "�eDu musst ein Item in deiner Hand halten.");
					}
					return true;
				}
				sender.sendMessage("�cDu musst ein Spieler sein.");
			} else {
				p.sendMessage(Data.KeineRechte);
			}
		} else if (cmd.getName().equals("relore")) {
			if (sender.hasPermission("System.relore")) {
				if (sender instanceof Player) {
					if (p.getItemInHand() != null
							&& !p.getItemInHand().getType()
									.equals(Material.AIR)) {
						if (args.length == 1) {
							ItemStack itemStack = p.getItemInHand();

							String string = args[0];
							String colouredString = ChatColor
									.translateAlternateColorCodes('&', string);
							String spacedString = colouredString.replaceAll(
									"_", " ");
							String[] loreNotConverted = spacedString.split(";");

							@SuppressWarnings("rawtypes")
							ArrayList lore = new ArrayList();
							for (String s : loreNotConverted) {
								lore.add(s);
							}
							ItemMeta itemStackMeta = itemStack.getItemMeta();
							itemStackMeta.setLore(lore);

							itemStack.setItemMeta(itemStackMeta);

							p.sendMessage(Data.p
									+ "�eDie Lore hat einen neuen Namen bekommen.");
						} else {
							p.sendMessage("�7Verwendung: �5/Relore <Name>");
							p.sendMessage("�7Hinweis: F�r Leerzeichen benutze _");
						}
					} else {
						p.sendMessage(Data.p
								+ "�eDu musst ein Item in deiner Hand halten.");
					}
					return true;
				}
				sender.sendMessage("�cDu musst ein Spieler sein.");
			} else {
				p.sendMessage(Data.KeineRechte);
			}
		}
		return false;
	}
}
