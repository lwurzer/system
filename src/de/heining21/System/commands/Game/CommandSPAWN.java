package de.heining21.System.commands.Game;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandSPAWN implements CommandExecutor, Listener {

	public static void CreateDir() {
		File check = new File(path);
		if (!check.isDirectory()) {
			check.mkdirs();
		}
	}

	public static void CreateFile() {
		try {
			CreateDir();
			file.createNewFile();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
	public static ArrayList<String> tptospawn = new ArrayList<String>();
	public static String path = "plugins/System/Locations/Spawn";

	static File file = new File(path, "Spawn.yml");
	static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

	public Main plugin;
	public CommandSPAWN(Main plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	public boolean onCommand(final CommandSender cs, Command cmd, String label,
			String[] args) {
		
		final Player p = (Player) cs;
		
		if (cmd.getName().equalsIgnoreCase("spawn")) {
			
/*			if (p.getWorld() != Bukkit.getWorld("skypvp") || p.getWorld() != Bukkit.getWorld("world") || p.getWorld() != Bukkit.getWorld("world_nether") || p.getWorld() != Bukkit.getWorld("world_the_end") || p.getWorld() != Bukkit.getWorld("plotworld")) {
				p.sendMessage(Data.p + "�cIn der Welt �4" + p.getWorld().getName() + " �r�cist der Spawn-Command blockiert!");
				p.sendMessage(Data.p + "�7Bitte umgehend bei einem Moderator | Developer | Admin | Owner melden!");
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tc &4&lAchtung: &r&2" + p.getName() + " &4ist in einer unerlaubten Welt &7[" + p.getWorld().getName() + "&7] &4und versuchte zum Spawn zu kommen!");
				return true;
			}
*/		
			if(args.length == 1 && cs.hasPermission("System.Spawn.others")){
				
				if(Bukkit.getPlayerExact(args[0]) != null){
					
					Player p2 = Bukkit.getPlayerExact(args[0]);
					
					FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
					
					double x = cfg.getDouble("Location.X");
					double y = cfg.getDouble("Location.Y");
					double z = cfg.getDouble("Location.Z");
					float yaw = (float) cfg.getDouble("Location.Yaw");
					float pitch = (float) cfg.getDouble("Location.Pitch");
					String worldname = cfg.getString("Location.World");
					
					Location loc = p2.getLocation();
					loc.setX(x);
					loc.setY(y);
					loc.setZ(z);
					loc.setYaw(yaw);
					loc.setPitch(pitch);
					loc.setWorld(Bukkit.getWorld(worldname));
					
					p2.teleport(loc);
					p2.sendMessage(Data.pfett + "�e�lDu wurdest zum �2�lSpawn �a�lteleportiert.");

					p2.playEffect(p2.getLocation(), Effect.ENDER_SIGNAL, 0);
					p2.playSound(p2.getLocation(), Sound.ENDERMAN_TELEPORT, 50F, 1F);

					return true;
				}else{
					cs.sendMessage(Data.UserNotExists);
					return true;
				}
				
			}
			final String t = p.getName();
			p.getLocation();
			tptospawn.add(t);
			p.sendMessage(Data.pfett + "�eDu wirst in �a3 �eSekunden zum �2Spawn �eteleportiert.");
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin,
					new Runnable() {
						public void run() {
							if (tptospawn.contains(t)) {
								if (p.getWorld() == Bukkit.getWorld("world") || p.getWorld() == Bukkit.getWorld("plotworld") || p.getWorld() == Bukkit.getWorld("world_the_end") || p.getWorld() == Bukkit.getWorld("world_nether")) {
									
									FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

									double x = cfg.getDouble("PvP.X");
									double y = cfg.getDouble("PvP.Y");
									double z = cfg.getDouble("PvP.Z");
									float yaw = (float) cfg.getDouble("PvP.Yaw");
									float pitch = (float) cfg.getDouble("PvP.Pitch");
									String worldname = cfg.getString("PvP.World");
									
									Location loc = p.getLocation();
									loc.setX(x);
									loc.setY(y);
									loc.setZ(z);
									loc.setYaw(yaw);
									loc.setPitch(pitch);
									loc.setWorld(Bukkit.getWorld(worldname));
									
									p.teleport(loc);
									p.sendMessage(Data.p + "�eDu wurdest zum �2Spawn �eteleportiert.");

									p.playEffect(p.getLocation(),
											Effect.ENDER_SIGNAL, 0);
									p.playSound(p.getLocation(),
											Sound.ENDERMAN_TELEPORT, 50F, 1F);

									tptospawn.remove(t);
								}
								
								if (p.getWorld() == Bukkit.getWorld("skypvp")) {
									
									FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

									double x = cfg.getDouble("SkyPvP.X");
									double y = cfg.getDouble("SkyPvP.Y");
									double z = cfg.getDouble("SkyPvP.Z");
									float yaw = (float) cfg.getDouble("SkyPvP.Yaw");
									float pitch = (float) cfg.getDouble("SkyPvP.Pitch");
									String worldname = cfg.getString("SkyPvP.World");
									
									Location loc = p.getLocation();
									loc.setX(x);
									loc.setY(y);
									loc.setZ(z);
									loc.setYaw(yaw);
									loc.setPitch(pitch);
									loc.setWorld(Bukkit.getWorld(worldname));

									p.teleport(loc);
									p.sendMessage(Data.p + "�eDu wurdest zum �2Spawn �eteleportiert.");

									p.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 0);
									p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 50F, 1F);
									
									tptospawn.remove(t);
								}
								
							}
						}
					}, 60L);
		}
			
			
			
			if (cmd.getName().equalsIgnoreCase("setspawn")) {
				if (!cs.hasPermission("System.setspawn")) {
					cs.sendMessage(Data.KeineRechte);
					return true;
				}
				
				if (args.length == 0) {
					cs.sendMessage(Data.p + "�eVerwendung: �a/SetSpawn <PvP, SkyPvP>");
					return true;
				}
				
			if (args[0].equalsIgnoreCase("pvp")) {
				
				@SuppressWarnings("unused")
				final String t = p.getName();
				cfg.set("PvP.X", p.getLocation().getX());
				cfg.set("PvP.Y", p.getLocation().getY());
				cfg.set("PvP.Z", p.getLocation().getZ());
				cfg.set("PvP.Yaw", p.getLocation().getYaw());
				cfg.set("PvP.Pitch", p.getLocation().getPitch());
				cfg.set("PvP.World", p.getWorld().getName());
				
				try {
					cfg.save(file);
					p.sendMessage(Data.pfett + "�e�lDu hast den �2�lPVP-Spawn �e�lgesetzt.");
				} catch (IOException e) {
					e.printStackTrace();
					p.sendMessage(Data.pfett + "�cError: Wende dich an heining21.");
				}
			}
			
			if (args[0].equalsIgnoreCase("skypvp")) {
				
				@SuppressWarnings("unused")
				final String t = p.getName();
				cfg.set("SkyPvP.X", p.getLocation().getX());
				cfg.set("SkyPvP.Y", p.getLocation().getY());
				cfg.set("SkyPvP.Z", p.getLocation().getZ());
				cfg.set("SkyPvP.Yaw", p.getLocation().getYaw());
				cfg.set("SkyPvP.Pitch", p.getLocation().getPitch());
				cfg.set("SkyPvP.World", p.getWorld().getName());
				
				try {
					cfg.save(file);
					p.sendMessage(Data.pfett + "�e�lDu hast den �2�lSKYPVP-Spawn �e�lgesetzt.");
				} catch (IOException e) {
					e.printStackTrace();
					p.sendMessage(Data.pfett + "�cError: Wende dich an heining21.");
				}
			}
			
		}
		
		return true;
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if(e.getFrom().getBlockX() == e.getTo().getBlockX() && e.getFrom().getBlockY() == e.getTo().getBlockY() && e.getFrom().getBlockZ() == e.getTo().getBlockZ()){
			if (tptospawn.contains(p.getName())) {
				tptospawn.remove(p.getName());
				p.sendMessage(Data.pfett + "�e�lDer Teleport zum �2�lSpawn �e�lwurde abgebrochen.");
			}
			return;
			
		}
	}
}