package de.heining21.System.commands.Game;

import java.util.ArrayList;
import java.util.HashSet;

import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandSPAWNER implements CommandExecutor {
	static String[] Types = "ARROW, BAT, BLAZE, BOAT, CAVE_SPIDER, CHICKEN, COMPLEX_PART, COW, CREEPER, DROPPED_ITEM, EGG, ENDER_DRAGON, ENDER_PEARL, ENDERMAN, EXPERIENCE_ORB, FIREBALL, GHAST, GIANT, HORSE, IRON_GOLEM, MAGMA_CUBE, MINECART, MINECART_CHEST, MINECART_COMMAND, MINECART_FURNACE, MINECART_HOPPER, MINECART_MOB_SPAWNER, MINECART_TNT, MUSHROOM_COW, OCELOT, PIG, PIG_ZOMBIE, PRIMED_TNT, SHEEP, SILVERFISH, SKELETON, SLIME, SMALL_FIREBALL, SNOWBALL, SNOWMAN, SPIDER, SPLASH_POTION, SQUID, THROWN_EXP_BOTTLE, VILLAGER, WITCH, WITHER, WITHER_SKULL, WOLF, ZOMBIE"

	.split(", ");
	public static ArrayList<String> TypesArray = new ArrayList<String>();

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("spawner")) {
			if ((cs instanceof ConsoleCommandSender)) {
				cs.sendMessage(Data.p + "�4Du kannst das nicht tun");
				return true;
			}
			Player p = (Player) cs;
			if (!p.hasPermission("System.spawner")) {
				p.sendMessage(Data.KeineRechte);
				return true;
			}
			Block b = p.getTargetBlock((HashSet<Byte>) null, 10);
			if (b.getType().getId() == 52) {
				CreatureSpawner mobSpawner = (CreatureSpawner) b.getState();
				if (args.length == 0) {
					p.sendMessage(Data.p + "Es existieren folgende Mobs:");
					p.sendMessage("�7ARROW, BAT, BLAZE, BOAT, CAVE_SPIDER, CHICKEN, COW, CREEPER, DROPPED_ITEM, EGG, ENDER_DRAGON, ENDER_PEARL, ENDERMAN, EXPERIENCE_ORB, FIREBALL, GHAST, GIANT, HORSE, IRON_GOLEM, MAGMA_CUBE, MINECART, MINECART_CHEST, MINECART_COMMAND, MINECART_FURNACE, MINECART_HOPPER, MINECART_TNT, MUSHROOM_COW, OCELOT, PIG, PIG_ZOMBIE, SHEEP, SILVERFISH, SKELETON, SLIME, SMALL_FIREBALL, SNOWBALL, SNOWMAN, SPIDER, SPLASH_POTION, SQUID, VILLAGER, WITCH, WITHER, WITHER_SKULL, WOLF, ZOMBIE");

					return true;
				}
				if (args.length == 1) {
					String entityTypeString = args[0].toUpperCase();
					
					if(!p.hasPermission("System.spawner." + entityTypeString.toLowerCase())){
						
						p.sendMessage(Data.getFehler("Du hast nicht gen�gend Rechte, um den Spawner �4" + entityTypeString.toLowerCase() + "�c zu erstellen"));
						return true;
						
					}
					if (entityTypeString.equalsIgnoreCase("ARROW")) {
						mobSpawner.setSpawnedType(EntityType.ARROW);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("BAT")) {
						mobSpawner.setSpawnedType(EntityType.BAT);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("BOAT")) {
						mobSpawner.setSpawnedType(EntityType.BOAT);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("BLAZE")) {
						mobSpawner.setSpawnedType(EntityType.BLAZE);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("CAVE_SPIDER")) {
						mobSpawner.setSpawnedType(EntityType.CAVE_SPIDER);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("CHICKEN")) {
						mobSpawner.setSpawnedType(EntityType.CHICKEN);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("COW")) {
						mobSpawner.setSpawnedType(EntityType.COW);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("CREEPER")) {
						mobSpawner.setSpawnedType(EntityType.CREEPER);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("DROPPED_ITEM")) {
						mobSpawner.setSpawnedType(EntityType.DROPPED_ITEM);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("EGG")) {
						mobSpawner.setSpawnedType(EntityType.EGG);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("ENDER_DRAGON")) {
						mobSpawner.setSpawnedType(EntityType.ENDER_DRAGON);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("ENDERMAN")) {
						mobSpawner.setSpawnedType(EntityType.ENDERMAN);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("EXPERIENCE_ORB")) {
						mobSpawner.setSpawnedType(EntityType.EXPERIENCE_ORB);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("FIREBALL")) {
						mobSpawner.setSpawnedType(EntityType.FIREBALL);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("GHAST")) {
						mobSpawner.setSpawnedType(EntityType.GHAST);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("GIANT")) {
						mobSpawner.setSpawnedType(EntityType.GIANT);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("HORSE")) {
						mobSpawner.setSpawnedType(EntityType.HORSE);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("IRON_GOLEM")) {
						mobSpawner.setSpawnedType(EntityType.IRON_GOLEM);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("MAGMA_CUBE")) {
						mobSpawner.setSpawnedType(EntityType.MAGMA_CUBE);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("MINECART")) {
						mobSpawner.setSpawnedType(EntityType.MINECART);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("MINECART_CHEST")) {
						mobSpawner.setSpawnedType(EntityType.MINECART_CHEST);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("MINECART_COMMAND")) {
						mobSpawner.setSpawnedType(EntityType.MINECART_COMMAND);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("MINECART_FURNACE")) {
						mobSpawner.setSpawnedType(EntityType.MINECART_FURNACE);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("MINECART_HOPPER")) {
						mobSpawner.setSpawnedType(EntityType.MINECART_HOPPER);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("MINECART_TNT")) {
						mobSpawner.setSpawnedType(EntityType.MINECART_TNT);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("MUSHROOM_COW")) {
						mobSpawner.setSpawnedType(EntityType.MUSHROOM_COW);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("OCELOT")) {
						mobSpawner.setSpawnedType(EntityType.OCELOT);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("PIG")) {
						mobSpawner.setSpawnedType(EntityType.PIG);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("PIG_ZOMBIE")) {
						mobSpawner.setSpawnedType(EntityType.PIG_ZOMBIE);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("SHEEP")) {
						mobSpawner.setSpawnedType(EntityType.SHEEP);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("SILVERFISH")) {
						mobSpawner.setSpawnedType(EntityType.SILVERFISH);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("SKELETON")) {
						mobSpawner.setSpawnedType(EntityType.SKELETON);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("SLIME")) {
						mobSpawner.setSpawnedType(EntityType.SLIME);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("SMALL_FIREBALL")) {
						mobSpawner.setSpawnedType(EntityType.SMALL_FIREBALL);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("SNOWBALL")) {
						mobSpawner.setSpawnedType(EntityType.SNOWBALL);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("SNOWMAN")) {
						mobSpawner.setSpawnedType(EntityType.SNOWMAN);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("SPIDER")) {
						mobSpawner.setSpawnedType(EntityType.SPIDER);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("SQUID")) {
						mobSpawner.setSpawnedType(EntityType.SQUID);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("VILLAGER")) {
						mobSpawner.setSpawnedType(EntityType.VILLAGER);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("WITCH")) {
						mobSpawner.setSpawnedType(EntityType.WITCH);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("WITHER")) {
						mobSpawner.setSpawnedType(EntityType.WITHER);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("WITHER_SKULL")) {
						mobSpawner.setSpawnedType(EntityType.WITHER_SKULL);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("WOLF")) {
						mobSpawner.setSpawnedType(EntityType.WOLF);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
					if (entityTypeString.equalsIgnoreCase("ZOMBIE")) {
						mobSpawner.setSpawnedType(EntityType.ZOMBIE);
						mobSpawner.update();
						p.sendMessage(Data.p
								+ "Der Spawner wurde erfolgreich gesetzt");
						return true;
					}
				} else {
					p.sendMessage(Data.p + "Dieses Entity existiert nicht.");
					return true;
				}
			} else {
				p.sendMessage(Data.p + "Du musst einen Mob-Spawner anschauen");
				return true;
			}
		}
		return false;
	}

	public static void setUpList() {
		for (int i = 0; i < Types.length; i++) {
			TypesArray.add(Types[i]);
		}
	}
}
