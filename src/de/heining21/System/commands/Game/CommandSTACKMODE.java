package de.heining21.System.commands.Game;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import de.heining21.System.Data;

public class CommandSTACKMODE implements CommandExecutor, Listener {

	ArrayList<Player> stackenable = new ArrayList<Player>();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("stackmode")) {
			if (p.hasPermission("System.stackmode")) {
				
				if (stackenable.contains(p)) {
					stackenable.remove(p);
					p.sendMessage(Data.p + "Du hast den Stackmode deaktiviert.");
				} else {
					stackenable.add(p);
					p.sendMessage(Data.p + "Du hast den Stackmode aktiviert.");
				}
			}
		}
		return true;
	}
	
	@EventHandler
	public void onPlayerKlick(PlayerInteractEntityEvent e) {
		Player p = (Player) e.getPlayer();
		Player p2 = (Player) e.getRightClicked();
		if (e.getRightClicked().getType() == EntityType.PLAYER) {
			if (stackenable.contains(p)) {
				if (p.isSneaking()) {
					p.setPassenger(p2);
					p.sendMessage(Data.p + "Du hast �a" + p2.getName() + " �egestackt.");
					p2.sendMessage(Data.p + "Du wurdest von �a" + p.getName() + " �egestackt.");
				}
			}
		}
	}
}
