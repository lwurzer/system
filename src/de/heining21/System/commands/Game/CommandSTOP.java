package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandSTOP implements Listener {

	public Main plugin;

	public CommandSTOP(Main plugin) {
		this.plugin = plugin;
	}

	public void PlaySound() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.playSound(p.getLocation(), Sound.SHOOT_ARROW, 50F, 1F);
		}
	}

	@EventHandler
	public void Stop(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		String command = e.getMessage();
		if (command.equalsIgnoreCase("/stop")) {
			if (p.hasPermission("System.stop")) {
				e.setCancelled(true);

				Bukkit.broadcastMessage("�4�l�m-------------------------------------");
				Bukkit.broadcastMessage("�c�lDer Server wird in 20 Sekunden �4�lgestoppt.");
				Bukkit.broadcastMessage("�4�l�m-------------------------------------");
				PlaySound();
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin,
						new Runnable() {
							public void run() {
								Bukkit.broadcastMessage("�4�l�m-------------------------------------");
								Bukkit.broadcastMessage("�c�lDer Server wird in 10 Sekunden �4�lgestoppt.");
								Bukkit.broadcastMessage("�4�l�m-------------------------------------");
								PlaySound();
								Bukkit.getScheduler().scheduleSyncDelayedTask(
										plugin, new Runnable() {
											public void run() {
												for (Player all : plugin
														.getServer()
														.getOnlinePlayers()) {
													all.kickPlayer(Data.pfett
															+ "�4�lDer Server wurde gestoppt! �c�lVersuche gleich wieder zu joinen!");
												}
												Bukkit.getScheduler()
														.scheduleSyncDelayedTask(
																plugin,
																new Runnable() {
																	public void run() {
																		Bukkit.shutdown();
																		Bukkit.broadcastMessage(Data.p
																				+ "�4�lServer wurde gestoppt!");
																	}
																}, 20L);
											}
										}, 200L);
							}
						}, 200L);
			}
		}
	}
}
