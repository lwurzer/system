package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

import de.heining21.System.Data;

public class CommandSUDO implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("sudo")) {

			if (!cs.hasPermission("System.sudo")) {
				cs.sendMessage(Data.KeineRechte);
				return true;
			}
			if (cs instanceof ConsoleCommandSender) {

				cs.sendMessage("�cDu musst das nicht tun.");
				return true;

			}

			StringBuffer result = new StringBuffer();
			for (String arg : args) {
				result.append(arg);
				result.append(" ");
			}
			String mynewstring = result.toString();

			if (args.length == 0) {

				cs.sendMessage(Data.ZuWenigArgs);
				return true;

			} else {

				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), mynewstring);
				cs.sendMessage(Data.p + "Du hast erfolgreich einen Console-Befehl gesendet.");
				return true;

			}

		}

		return false;
	}

}
