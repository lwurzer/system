package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.SuppChannel;

public class CommandSUPPORT implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("Support")) {
			if (args.length == 0) {
				
				if (!(cs instanceof Player)) {
					return true;
				}
				if (SuppChannel.waiting.contains(cs)) {
					cs.sendMessage(Data.supportp
							+ "�cDu bist schon in der Warscheschlange.");
					return true;
				}
				SuppChannel.waiting.add((Player) cs);
				cs.sendMessage(Data.supportp
						+ "�eDu wurdest in die Warteschlange hinzugef�gt. Es wird gleich jemand kommen.");

				for (Player p1 : Bukkit.getServer().getOnlinePlayers()) {

					if (p1.hasPermission("System.support.supporter")) {
						p1.sendMessage(Data.supportp
								+ "�a"
								+ cs.getName()
								+ "�e wartet auf Support.");
						p1.sendMessage("�e=> �a/Support Join");
					}

				}
				return true;

			}
			if (args.length == 1 && cs.hasPermission("System.support.supporter")) {
				if (args[0].equalsIgnoreCase("join")) {
					if (!(cs instanceof Player)) {
						return true;
					}
					if (!cs.hasPermission("System.support.supporter")) {
						cs.sendMessage(Data.KeineRechte);
						return true;
					}
					if (SuppChannel.waiting.isEmpty()) {
						cs.sendMessage(Data.supportp
								+ "�cMomentan ist niemand in der Support-Chat-Warteschlange.");
						return true;
					}

					Player psup = (Player) cs;
					Player pwaiting = SuppChannel.waiting.get(0);
					SuppChannel.addPlayerToChat(pwaiting, psup);
					return true;
				}
				if (args[0].equalsIgnoreCase("leave") || args[0].equalsIgnoreCase("close") && cs.hasPermission("System.support.supporter")) {

					if (!cs.hasPermission("System.support.supporter")) {
						cs.sendMessage(Data.KeineRechte);
						return true;
					}

					if (SuppChannel.inChat.contains(cs)) {
						SuppChannel.removeSupChn((Player) cs);
						return true;
					} else if (SuppChannel.waiting.contains(cs)) {

						SuppChannel.waiting.remove(cs);

						for (Player ponline : Bukkit.getServer()
								.getOnlinePlayers()) {

							if (ponline.hasPermission("System.support.supporter")) {
								ponline.sendMessage(Data.supportp + "�a"
										+ cs.getName()
										+ "�ebraucht nun keine Hilfe mehr.");
							}

						}
						return true;

					} else {
						cs.sendMessage(Data.supportp
								+ "�eDu befindest dich gerade im Support.");

						return true;
					}
				}
			}

		}

		return false;
	}

}
