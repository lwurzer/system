package de.heining21.System.commands.Game;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.Helfer.TabUtils;
import de.heining21.System.Data;
import de.heining21.System.Main;
import de.heining21.System.ServerListe.ProtocolLib;
import de.heining21.System.utils.UtilARRAYStoSTRING;

public class CommandTAB implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("tab")) {
			if (!cs.hasPermission("System.tab")) {
				cs.sendMessage(Data.KeineRechte);
				return true;
			}
			if (args.length <= 1) {
				cs.sendMessage(Data.ZuWenigArgs);
				cs.sendMessage("�7Benutze: /tab <header | footer> <Text>");
				return true;
			}
			String type = args[0];
			String text = UtilARRAYStoSTRING.arrayToString(args, 1);

			String header = Main.TabHeader;
			String footer = Main.TabFooter;
			if (type.equalsIgnoreCase("header")) {
				header = text.replace("&", "�");
				Main.TabHeader = header;
				for (Player pOn : Bukkit.getOnlinePlayers()) {
					TabUtils.setTab(header, footer, pOn);
				}
				ProtocolLib.cfg.set("Tab.Header", text);
				try {
					ProtocolLib.cfg.save(ProtocolLib.file);
				} catch (IOException e) {
					e.printStackTrace();
				}
				cs.sendMessage(Data.p + "Der Text wurde Ge�ndert");
				return true;
			}
			if (type.equalsIgnoreCase("footer")) {
				footer = text.replace("&", "�");
				for (Player pOn : Bukkit.getOnlinePlayers()) {
					TabUtils.setTab(header, footer, pOn);
				}
				Main.TabFooter = footer;
				ProtocolLib.cfg.set("Tab.Footer", text);
				try {
					ProtocolLib.cfg.save(ProtocolLib.file);
				} catch (IOException e) {
					e.printStackTrace();
				}
				cs.sendMessage(Data.p + "Der Text wurde Ge�ndert");
				return true;
			}
			cs.sendMessage(Data.getFehler("Falsche verwendung"));
			cs.sendMessage("�7Benutze: /tab <header | footer> <Text>");
			return true;
		}
		return false;
	}
}
