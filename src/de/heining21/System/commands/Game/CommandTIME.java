package de.heining21.System.commands.Game;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandTIME implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("day")) {
			
			if (!cs.hasPermission("System.Time")) {
				cs.sendMessage(Data.KeineRechte);
				return true;
			} else {

				if (!(cs instanceof Player)) {

					cs.sendMessage(Data.p
							+ "�cDu kannst das nicht tun.");
					return true;

				} else {

					Player p = (Player) cs;
					World world = p.getWorld();
					world.setTime(0);
					p.sendMessage(Data.p + "Du hast die Zeit in Welt �a"
							+ world.getName() + "�e auf �a06:00�e gesetzt.");
					return true;

				}

			}

		}
		if (cmd.getName().equalsIgnoreCase("night")) {

			if (!cs.hasPermission("System.Time")) {
				cs.sendMessage(Data.KeineRechte);
				return true;
			} else {

				if (!(cs instanceof Player)) {

					cs.sendMessage(Data.p
							+ "�cDu kannst das nicht tun.");
					return true;

				} else {

					Player p = (Player) cs;
					World world = p.getWorld();
					world.setTime(13000);
					p.sendMessage(Data.p + "Du hast die Zeit in Welt �a"
							+ world.getName() + "�e auf �a19:00�e gesetzt.");
					return true;

				}

			}

		}

		return false;
	}

}
