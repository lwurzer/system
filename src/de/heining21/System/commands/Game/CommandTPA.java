package de.heining21.System.commands.Game;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandTPA implements CommandExecutor {

	public static HashMap<Player, ArrayList<Player>> tpwarten = new HashMap<Player, ArrayList<Player>>();
	public static ArrayList<String> teleport = new ArrayList<String>();
	
	public Main plugin;
	public CommandTPA(Main plugin) {
		this.plugin = plugin;
	}
	
	@SuppressWarnings("null")
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		final Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("tpa")) {
			if (p.hasPermission("System.tpa")) {
				if (args.length == 0) {
					p.sendMessage(Data.p + "Verwendung: �a/tpa <Spieler>");
				}
				
				if (args.length == 1) {
					final Player p2 = Bukkit.getPlayer(args[0]);
					if (p2 != null) {
						
						if (tpwarten.containsKey(p)) {
							tpwarten.get(p2).add(p);
						} else {
							ArrayList<Player> req = new ArrayList<Player>();
							req.add(p);
							tpwarten.put(p2, req);
						}
						
						p.sendMessage(Data.p + "Du hast dem Spieler �a" + p2.getName() + "�e eine Teleport-Anfrage gesendet. �a" + p2.getName() + " �ehat nun 30 Sekunden Zeit, diese Anfrage anzunehmen.");
						p2.sendMessage(Data.p + "�a" + p.getName() + "�e hat dir eine Anfrage gesendet, ob er sich zu dir teleportieren darf.");
						p2.sendMessage("  - �aAnnehmen mit: �b/tpaccept " + p.getName());
						p2.sendMessage("  - �cAblehnen mit: �b/tpdeny " + p.getName());
						
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							
							public void run() {
								if (tpwarten.get(p).contains(p2)) {
									
									p.sendMessage(Data.p + "�7Der Spieler �8" + p2.getName() + "�7 hat deine Teleportanfrage nicht angenommen und wurde deshalb automatisch abgelehnt.");
									p2.sendMessage(Data.p + "�7Du hast die Anfrage von �8" + p.getName() + " �7ignoriert. Deshalb wurde sie automatisch abgelehnt.");
									
									tpwarten.get(p).remove(p2);
								}
							}
						}, 30*20L);
						
					} else {
						p.sendMessage(Data.p + "�cDer Spieler �4" + p2.getName() + " ist nicht online.");
					}
					return true;
				}
			} else {
				p.sendMessage(Data.KeineRechte + Data.rang);
			}
		}
		return true;
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if(e.getFrom().getBlockX() == e.getTo().getBlockX() && e.getFrom().getBlockY() == e.getTo().getBlockY() && e.getFrom().getBlockZ() == e.getTo().getBlockZ()){
			if (teleport.contains(p.getName())) {
				teleport.remove(p.getName());
				p.sendMessage(Data.pfett + "�cDer Teleport zu wurde abgebrochen.");
			}
			return;
			
		}
	}
}
