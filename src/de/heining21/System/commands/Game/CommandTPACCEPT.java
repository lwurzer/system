package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandTPACCEPT implements CommandExecutor {
	
	public Main plugin;
	public CommandTPACCEPT(Main plugin) {
		this.plugin = plugin;
	}
	
	@SuppressWarnings("null")
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		final Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("tpaccept")) {
			
			if (args.length == 0) {
				p.sendMessage(Data.p + "Verwendung: �a/tpaccept <Spieler>");
			}
			
			 if (args.length == 1) {
			
				final Player p2 = Bukkit.getPlayer(args[0]);
				if (p2 != null) {
					
					if (CommandTPA.tpwarten.containsKey(p)) {
						if (CommandTPA.tpwarten.get(p).contains(p)) {
							CommandTPA.tpwarten.get(p).remove(p);
							p.sendMessage(Data.p + "Du hast die TP-Anfrage von �a" + p2.getName() + " �eANGENOMMEN. �7(Du wirst in 3 Sekunden teleportiert)");
							p2.sendMessage(Data.p + "�a" + p.getName() + " �ehat deine TP-Anfrage ANGENOMMEN. �7(Er wird in 3 Sekunden teleportiert)");
							CommandTPA.teleport.add(p2.getName());
							
							Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								
								public void run() {
									if (CommandTPA.teleport.contains(p2.getName())) {
										p.sendMessage(Data.p + "Du wurdest zu �a" + p2.getName() + "�e teleportiert.");
										p2.sendMessage(Data.p + "�a" + p.getName() + " �ewurde zu dir teleportiert.");
										p2.teleport(p.getLocation());
										CommandTPA.teleport.remove(p2.getName());
									}
								}
							}, 3*20L);

						} else {
							p.sendMessage(Data.p + "�4" + p2.getName() + " �chat dir keine Anfrage gestellt.");
						}

					} else {
						p.sendMessage(Data.p + "�4" + p2.getName() + " �chat dir keine Anfrage gestellt.");
					}

				} else {
					p.sendMessage(Data.p + "�cDer Spieler �4" + p2.getName() + "�c ist nicht online.");
				}
			}
		}
		return true;
	}
}
