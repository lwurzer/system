package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandTPALL implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("tpall")
				&& cs.hasPermission("System.tpall")) {
			Player p = (Player) cs;
			for (Player all : p.getServer().getOnlinePlayers()) {
				all.teleport(p);
				PlayEffect();
				PlaySound();
				all.sendMessage(Data.pfett
						+ "�e�lAlle Spieler wurden zu �a�l" + p.getName()
						+ "�e�l teleportiert!");
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	public void PlayEffect() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 0);
		}
	}

	public void PlaySound() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 50F, 1F);
		}
	}
}