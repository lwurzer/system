package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandTPDENY implements CommandExecutor {

	@SuppressWarnings("null")
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		final Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("tpdeny")) {
			
			if (args.length == 0) {
				p.sendMessage(Data.p + "Verwendung: �a/tpdeny <Spieler>");
			}
			
			 if (args.length == 1) {
					 
				Player p2 = Bukkit.getPlayer(args[0]);
				if (p2 != null) {
				
					if (CommandTPA.tpwarten.containsKey(p)) {
						if (CommandTPA.tpwarten.get(p).contains(p2)) {
							CommandTPA.tpwarten.get(p).remove(p2);
							p2.sendMessage(Data.p + "Du hast die TP-Anfrage von �a" + p.getName() + " �eABGELEHNT.");
							p.sendMessage(Data.p + "�a" + p2.getName() + " �ehat deine TP-Anfrage ABGELEHNT.");

						} else {
							p.sendMessage(Data.p + "�4" + p2.getName() + " �chat dir keine Anfrage gestellt.");
						}

					} else {
						p.sendMessage(Data.p + "�4" + p2.getName() + " �chat dir keine Anfrage gestellt.");
					}

				} else {
					p.sendMessage(Data.p + "�cDer Spieler �4" + p2.getName() + " �cist nicht online.");
				}
			 }
		}
		return true;
	}
}
