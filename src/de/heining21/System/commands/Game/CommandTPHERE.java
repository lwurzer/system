package de.heining21.System.commands.Game;

import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandTPHERE implements CommandExecutor {

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("tphere")
				&& cs.hasPermission("system.tphere")) {
			Player p = (Player) cs;
			if (args.length == 0) {
				cs.sendMessage("�eVerwendung: �a/tphere <Spieler>");
			} else if (args.length == 1) {

				Player p2 = null;
				try {

					p2 = p.getServer().getPlayer(args[0]);

				} catch (NullPointerException e) {
					cs.sendMessage(Data.UserNotExists);
					return true;
				}

				p2.teleport(p);
				p.sendMessage(Data.p + "�eDu hast �a" + p2.getName() + "�e zu dir teleportiert!");
				p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 50F, 1F);
				p.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 0);
				if (p2.hasPermission("System.tphere")) {
					p2.sendMessage(Data.p + "�eDu wurdest zu �a" + p.getName() + " �eteleportiert!");
					p2.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 50F, 1F);
					p2.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 0);
				}
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}

}