package de.heining21.System.commands.Game;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import de.heining21.System.Data;

public class CommandVANISH implements CommandExecutor, Listener {
	
	ArrayList<String> vanish = new ArrayList<String>();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("vanish")) {
			
			if (vanish.contains(p.getName())) {
				vanish.remove(p.getName());
				p.sendMessage(Data.p + "Du bist nun nicht mehr Vanish.");
				
				for (Player online : Bukkit.getOnlinePlayers()) {
					if (!online.hasPermission("System.vanish.admin")) {
						online.showPlayer(p);
					}
				}
				p.getInventory().setBoots(new ItemStack(Material.AIR));
				
			} else {
				vanish.add(p.getName());
				p.sendMessage(Data.p + "Du bist nun Vanish.");
				
				if (p.getInventory().getBoots() != null) {
					if (p.getInventory().getBoots().getType() != Material.LEATHER_BOOTS) {
						p.getInventory().addItem(p.getInventory().getBoots());
						p.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
					} else {
						p.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
					}
				} else {
					p.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
				}
				
				for (Player online : Bukkit.getOnlinePlayers()) {
					if (!online.hasPermission("System.vanish.admin")) {
						online.hidePlayer(p);
					}
				}
			}
		}
		return true;
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (vanish.contains(p.getName())) {		
			if (p.getInventory().getBoots().getType() == Material.LEATHER_BOOTS) {
				p.sendMessage(Data.p + "�cDu musst diese Schuhe im Vanish anlassen.");
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		Player p = (Player) e.getPlayer();
		if (vanish.contains(p.getName())) {
			if (p.getInventory().getBoots().getType() == Material.LEATHER_BOOTS) {
				p.sendMessage(Data.p + "�cDu musst diese Schuhe im Vanish anlassen.");
				e.setCancelled(true);
			}
		}
	}
}
