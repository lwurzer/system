package de.heining21.System.commands.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import de.heining21.System.Data;

public class CommandVORBAU implements CommandExecutor {
	
	PermissionManager pex = PermissionsEx.getPermissionManager();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("vorbau")) {
			if (p.hasPermission("System.vorbau")) {
				
				if (args.length != 2) {
					p.sendMessage(Data.p + "Verwendung: �a/Vorbau <Start|Stop> <Spieler>");
					return true;
				}
				if (args.length == 2) {
					if (args[0].equalsIgnoreCase("start")) {
						if (pex.getGroup("Vorbauer") != null) {
							pex.getUser(args[1]).addGroup("Vorbauer");
							p.sendMessage(Data.p + "Der Spieler �a" + args[1] + " �eist nun Vorbauer.");
						} else {
							p.sendMessage(Data.p + "�cDie Gruppe Vorbauer exestiert nicht.");
						}
					}
					
					if (args[0].equalsIgnoreCase("stop")) {
						pex.getUser(args[1]).removeGroup("Vorbauer");
						p.sendMessage(Data.p + "Du hast dem Spieler �a" + args[1] + " �eVorbauer entzogen.");
					}
					
				}
			} else {
				p.sendMessage(Data.KeineRechte);
			}
		}
		return true;
	}
}
