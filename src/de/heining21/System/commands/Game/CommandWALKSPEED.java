package de.heining21.System.commands.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandWALKSPEED implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("walkspeed")
				&& cs.hasPermission("system.speed")) {
			Player p = (Player) cs;
			if (args.length == 0) {
				cs.sendMessage("�eVerwendung: �a/WalkSpeed <1,2,3,4,5,6,7,8,9,10>");
			} else if (args[0].equalsIgnoreCase("0")) {
				p.setWalkSpeed(0F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a0�e.");
			} else if (args[0].equalsIgnoreCase("1")) {
				p.setWalkSpeed(0.1F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a1�e.");
			} else if (args[0].equalsIgnoreCase("2")) {
				p.setWalkSpeed(0.2F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a2�e.");
			} else if (args[0].equalsIgnoreCase("3")) {
				p.setWalkSpeed(0.3F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a3�e.");
			} else if (args[0].equalsIgnoreCase("4")) {
				p.setWalkSpeed(0.4F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a4�e.");
			} else if (args[0].equalsIgnoreCase("5")) {
				p.setWalkSpeed(0.5F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a5�e.");
			} else if (args[0].equalsIgnoreCase("6")) {
				p.setWalkSpeed(0.6F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a6�e.");
			} else if (args[0].equalsIgnoreCase("7")) {
				p.setWalkSpeed(0.7F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a7�e.");
			} else if (args[0].equalsIgnoreCase("8")) {
				p.setWalkSpeed(0.8F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a8�e.");
			} else if (args[0].equalsIgnoreCase("9")) {
				p.setWalkSpeed(0.9F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a9�e.");
			} else if (args[0].equalsIgnoreCase("10")) {
				p.setWalkSpeed(1F);
				cs.sendMessage(Data.p + "�eDein Geh-Speed ist nun �a10�e.");
			} else if (args.length != 0) {
				cs.sendMessage("�cFalsche Verwendung dieses Commands!");
				cs.sendMessage("�eRICHIGE Verwendung: �a/WalkSpeed <1,2,3,4,5,6,7,8,9,10>");
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}