package de.heining21.System.commands.Game;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.Funktionen.FunktionenWARP;

public class CommandWARP implements CommandExecutor {

	List<String> results = new ArrayList<String>();

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {

		if (sender instanceof ConsoleCommandSender) {
			Bukkit.getConsoleSender().sendMessage("�cDu musst ein Spieler sein, um diesen Befehl auf�hren zu k�nnen.");
			return true;
		}
		Player p = (Player) sender;

		// Warp
		if (cmd.getName().equalsIgnoreCase("warp")) {
			
			if (args.length == 0) {
				
				if (p.hasPermission("System.warp.list")) {
					p.sendMessage(Data.p + "�cDu hast keine Rechte, die Warps zu sehen.");
					return true;
				}

				File[] files = new File("plugins/System/Locations/Warps").listFiles();
				// If this pathname does not denote a directory, then
				// listFiles() returns null.
				// \\Locations\\
				String warps = " ";

				for (File file : files) {

					FileConfiguration cfg = YamlConfiguration
							.loadConfiguration(file);

					warps = warps + cfg.get("Name") + ", ";

				}

				p.sendMessage(Data.p + "Es existierem folgende Warps: ");
				p.sendMessage(Data.p + "�7" + warps);

				p.sendMessage(Data.p + "�eVerwendung:�a /Warp <WarpName>");
				return true;
			}
			if (args.length == 1) {
				
/*				if (p.getWorld() != Bukkit.getWorld("skypvp") || p.getWorld() != Bukkit.getWorld("world") || p.getWorld() != Bukkit.getWorld("worlds_nether") || p.getWorld() != Bukkit.getWorld("world_the_end") || p.getWorld() != Bukkit.getWorld("plotworld")) {
					p.sendMessage(Data.p + "�cIn der Welt �4" + p.getWorld().getName() + " �r�cist der Spawn-Command blockiert!");
					p.sendMessage(Data.p + "�7Bitte umgehend bei einem Moderator | Developer | Admin | Owner melden!");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tc &4&lAchtung: &r&2" + p.getName() + " &4ist in einer unerlaubten Welt &7[" + p.getWorld().getName() + "&7] &4und versuchte zu einem Warp zu kommen!");
					return true;
				}
*/				
				FunktionenWARP.Warp(p, args[0]);
				// p.sendMessage("�7Du wurdest erfolgreich zum Warp �a" +
				// args[0] + " �7geportet");
				return true;
			}
			if (args.length >= 2) {
				p.sendMessage(Data.ZuVieleArgs);
				p.sendMessage(Data.p + "�eVerwendung:�a /Warp <WarpName>");
				return true;
			}
		}
		// Set Warp
		if (cmd.getName().equalsIgnoreCase("setwarp")) {

			if (args.length == 0) {
				p.sendMessage(Data.ZuWenigArgs);
				p.sendMessage(Data.p + "�eVerwendung: �");
				return true;
			}
			if (args.length == 1) {
				if (!p.hasPermission("System.setwarp")) {
					p.sendMessage(Data.KeineRechte);
					return true;
				}
				try {
					FunktionenWARP.SetWarp(p, args[0]);
					// p.sendMessage(Data.p + "�7Warp �a" + args[0] +
					// "�7 wurde erfolgreich erstellt.");
				} catch (IOException e) {

					p.sendMessage(Data.p
							+ "�cWende dich an heining21.");
				}
				return true;
			}
			if (args.length >= 2) {

				p.sendMessage(Data.ZuVieleArgs);
				p.sendMessage(Data.p + "�eVerwendung: �a/SetWarp <WarpName>");
				return true;

			}

		}
		// Delete Warp
		if (cmd.getName().equalsIgnoreCase("delwarp")) {

			if (args.length == 0) {
				p.sendMessage(Data.ZuWenigArgs);
				p.sendMessage(Data.p + "�eVerwendung:�a /DelWarp <WarpName>");
				return true;
			}
			if (args.length == 1) {
				if (!p.hasPermission("System.delwarp")) {
					p.sendMessage(Data.KeineRechte);
					return true;
				}
				FunktionenWARP.DelWarp(p, args[0]);

				return true;
			}
			if (args.length >= 2) {
				p.sendMessage(Data.ZuVieleArgs);
				p.sendMessage(Data.p + "�eVerwendung:�a /DelWarp <WarpName>");
				return true;
			}

		}

		return false;
	}

	//
	//

}
