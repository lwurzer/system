package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.heining21.System.Data;

public class CommandWARPS implements CommandExecutor, Listener {
	
	public Inventory inv = null;

	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if ((cmd.getName().equalsIgnoreCase("warps"))) {
			
			inv = p.getPlayer().getServer().createInventory(null, 54, "�5�lWarps");
			
			ItemStack pvp = new ItemStack(Material.DIAMOND_SWORD);
			ItemMeta metapvp = pvp.getItemMeta();
			metapvp.setDisplayName("�5�lPvP");
			pvp.setItemMeta(metapvp);
			
			ItemStack EG = new ItemStack(Material.SKULL_ITEM);
			ItemMeta metaEG = EG.getItemMeta();
			metaEG.setDisplayName("�5�lEnderGames");
			EG.setItemMeta(metaEG);
			
			ItemStack SB = new ItemStack(Material.GRASS);
			ItemMeta metaSB = SB.getItemMeta();
			metaSB.setDisplayName("�5�lSkyBlock");
			SB.setItemMeta(metaSB);
			
			ItemStack PB = new ItemStack(Material.SNOW_BALL);
			ItemMeta metaPB = PB.getItemMeta();
			metaPB.setDisplayName("�5�lPaintBall");
			PB.setItemMeta(metaPB);
			
			ItemStack einsvseins = new ItemStack(Material.DIAMOND_SWORD);
			ItemMeta metaeinsvseins = einsvseins.getItemMeta();
			metaeinsvseins.setDisplayName("�5�l1vs1");
			einsvseins.setItemMeta(metaeinsvseins);

			inv.setItem(0, pvp);
			inv.setItem(2, EG);
			inv.setItem(4, SB);
			inv.setItem(6, PB);
			inv.setItem(8, einsvseins);
			
			p.getPlayer().openInventory(inv);
			
		} else {
			p.sendMessage(Data.KeineRechte);
		}
		return true;
	}
	
	@EventHandler
	public static void onInventoryClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if(e.getInventory().getTitle().contains(ChatColor.stripColor("Warps"))){
			e.setCancelled(true);
			
			  if(e.getCurrentItem().getType() == Material.GOLD_HOE) {
				  Bukkit.dispatchCommand(p, "warp quake");
				  p.closeInventory();
				  
			 } else if(e.getCurrentItem().getType() == Material.SKULL_ITEM)  { 
				  Bukkit.dispatchCommand(p, "warp EnderGames");
				   p.closeInventory();
				   
			  } else if(e.getCurrentItem().getType() == Material.GRASS)  { 
				  Bukkit.dispatchCommand(p, "warp SkyBlock");
				   p.closeInventory();
				   
			  } else if(e.getCurrentItem().getType() == Material.SNOW_BALL)  { 
				  Bukkit.dispatchCommand(p, "warp PaintBall");
				   p.closeInventory();
				   
			  } else if(e.getCurrentItem().getType() == Material.DIAMOND_SWORD)  { 
				  Bukkit.dispatchCommand(p, "warp 1vs1");
				   p.closeInventory();
			  }
		 }
	}
}