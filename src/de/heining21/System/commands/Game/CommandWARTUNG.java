package de.heining21.System.commands.Game;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.utils.UtilBROADCAST;

public class CommandWARTUNG implements CommandExecutor{

	static File file = new File("plugins/System", "config.yml");
	static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if(cmd.getName().equalsIgnoreCase("wartung")){
			
			if(cs.hasPermission("System.wartung")){
				
				if(args.length == 0){
					
					if(Wartung()){
						cs.sendMessage(Data.p + "Der Server befindet sich im Moment im Wartungmodus");
					}else{
						cs.sendMessage(Data.p + "Der Server befindet sich momentan nicht im Wartungsmodus");
					}
					cs.sendMessage(Data.p + "�7Verwende: /wartung <an|aus>");
					return true;
					
				}
				if(args.length == 1){
					
					if(args[0].equalsIgnoreCase("an")){
						
						if(Wartung()){
							cs.sendMessage(Data.p + "Der Wartungsmodus ist schon an. Benutze �7/wartung aus�e um ihn zu deaktivieren");
							
							return true;
						}else{
							cfg.set("System.Wartung", true);
							try {
								cfg.save(file);
							} catch (IOException e) {
								
							}
							UtilBROADCAST.broadCastMessage(Data.p + "Der Wartungmodus wurde von �6" + cs.getName() + "�e aktiviert.");
							
							for(Player pOnline : Bukkit.getOnlinePlayers()){
								
								if(pOnline.hasPermission("System.Wartung")){
									
								}else{
									pOnline.kickPlayer("�4�lDer Wartunsmodus wurde Aktiviert. Versuche es sp�ter nochmals");
								}
								
							}
							return true;
							
						}
						
					}if(args[0].equalsIgnoreCase("aus")){
						
						if(Wartung()){
							cfg.set("System.Wartung", false);
							try {
								cfg.save(file);
							} catch (IOException e) {
								
							}
							UtilBROADCAST.broadCastMessage(Data.p + "Der Wartungsmodus wurde von �6" + cs.getName() + " �edeaktiviert");
							return true;
						}else{
							
							cs.sendMessage(Data.p + "Der Wartungsmodus ist schon aus. Benutze �7/wartung an�e um ihn zu aktivieren");
							
						}
						
					}
					
				}
				
			}else{
				cs.sendMessage(Data.KeineRechte);
				return true;
			}
			
		}
			
		
		return false;
	}

	public static boolean Wartung(){
		
		if(cfg.getBoolean("System.Wartung")){
			
			return true;
			
		}else{
			return false;
		}
			
		
	}
	
}
