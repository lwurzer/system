package de.heining21.System.commands.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandWB implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("wb")
				&& cs.hasPermission("System.wb")) {
			Player p = (Player) cs;
			p.openWorkbench(null, true);
			p.sendMessage(Data.p + "Es wurde eine �aWerkbank �ege�ffnet!");
		} else {
			cs.sendMessage(Data.KeineRechte + Data.rang);
		}
		return true;
	}
}