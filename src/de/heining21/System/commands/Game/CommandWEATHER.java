package de.heining21.System.commands.Game;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandWEATHER implements CommandExecutor{

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("sun")){
			
			if(cs instanceof ConsoleCommandSender){
				
				cs.sendMessage(Data.ConsoleCantDO);
				return true;
				
			}
			if(!cs.hasPermission("System.weather")){
				cs.sendMessage(Data.KeineRechte);
				return true;
			}
			Player p = (Player)cs;
			
			World world = p.getWorld();
			world.setWeatherDuration(0);
			world.setThunderDuration(0);
			world.setThundering(false);
			world.setStorm(false);
			p.sendMessage(Data.p + "Das Wetter in der Welt �6" + world.getName() + "�e wurde auf �6Sonne�e gesetzt.");
			return true;
			
		}
		if(cmd.getName().equalsIgnoreCase("rain")){
			if(cs instanceof ConsoleCommandSender){
				
				cs.sendMessage(Data.ConsoleCantDO);
				return true;
				
			}
			if(!cs.hasPermission("System.weather")){
				cs.sendMessage(Data.KeineRechte);
				return true;
			}
			Player p = (Player)cs;
			
			World world = p.getWorld();
			world.setStorm(true);
			p.sendMessage(Data.p + "Das Wetter in der Welt �6" + world.getName() + "�e wurde auf �6Regen�e gesetzt.");
			return true;
		}
		
		return false;
	}

}
