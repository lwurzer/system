package de.heining21.System.commands.Game;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import de.heining21.System.Data;
import de.heining21.System.Main;

public class CommandWHITELIST implements CommandExecutor, Listener {
	
	static File file = new File("plugins/System", "config.yml");
	static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	public Main plugin;
	public CommandWHITELIST(Main plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		final Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("whitelist")) {
			if (p.hasPermission("System.whitelist")) {
				if (args.length == 0) {
					p.sendMessage(Data.p + "Verwendung: �a/WhiteList <an, aus>");
				}
				
				if (args.length == 1) {
					if (args[0].equalsIgnoreCase("an")) {
						if (Whitelist()) {
							p.sendMessage(Data.p + "�cDie Whitelist ist schon an.");
						} else {
							cfg.set("System.Wartung", true);
							try {
								cfg.save(file);
							} catch (IOException e) {
							}
							Bukkit.broadcastMessage(Data.pfett + "�c�lDie Whitelist wurde von �4�l" + p.getName() + " �c�laktiviert.");
							Bukkit.broadcastMessage(Data.p + "Es werde alle SPIELER in 30 Sekunden gekickt.");
							Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

								public void run() {
									Bukkit.broadcastMessage(Data.p + "�cAlle Spieler werden in 20 Sekunden gekickt! �7(Grund: WHITELIST)");
									Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
										public void run() {
											Bukkit.broadcastMessage(Data.p + "�cAlle Spieler werden in 10 Sekunden gekickt! �7(Grund: WHITELIST)");
											Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
												public void run() {
													Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tc &cAlle Spieler wurde von &4" + p.getName() + " &cgekickt. &7(Grund: WHITELIST WURDE AKTIVIERT)");
													for (Player Online : Bukkit.getOnlinePlayers()) {
														if (!p.hasPermission("System.whitelist.ignore")) {
															Online.kickPlayer(Data.p + "�cDie Whitelist wurde aktiviert. �bVersuche es gleich nochmal.");
														}
													}
													
												}
											}, 10*20L);
										}
									}, 10*20L);
									
								}
							}, 10*20L);
						}
					}
					
					if (args[0].equalsIgnoreCase("aus")) {
						
						if (Whitelist()) {
							cfg.set("System.Wartung", false);
							try {
								cfg.save(file);
							} catch (IOException e) {
							}
							
							Bukkit.broadcastMessage(Data.pfett + "�c�lDie Whitelist wurde von �4�l" + p.getName() + " �c�ldeaktiviert.");
						} else {
							p.sendMessage(Data.p + "�cDie Whitelist ist schon aus.");
						}
					}
				}
			}
		}
		return true;
	}
	
	public static boolean Whitelist() {
		
		if(cfg.getBoolean("System.Whitelist")){
			return true;
			
		} else {
			return false;
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = (Player) e.getPlayer();
		if (Whitelist()) {
			if (!p.hasPermission("System.whitelist.ignore")) {
				p.kickPlayer(Data.p + "�cDie Whitelist ist aktiviert. �bVersuche es gleich nochmal.");
			}
		}
	}
	
}
