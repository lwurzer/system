package de.heining21.System.commands.Game;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;
import de.heining21.System.commands.Ban.CommandBAN;
import de.heining21.System.commands.Ban.CommandWARN;

public class CommandWHOIS implements CommandExecutor{

	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("whois")){
			
			if(!cs.hasPermission("System.whois")){
				
				cs.sendMessage(Data.KeineRechte);
				return true;
				
			}
			
			if(args.length != 1){
				cs.sendMessage(Data.getFehler("Falsche Verwendung"));
			}
			
			
			
			if(Bukkit.getPlayer(args[0]) == null){
				
				cs.sendMessage(Data.UserNotExists);
				return true;
				
			}
			
			Player p = Bukkit.getPlayer(args[0]);
			
			cs.sendMessage("�7--------�6" + args[0] + "�7--------");
			cs.sendMessage("�e>Gamemode: �6" + p.getGameMode().toString().toLowerCase());
			cs.sendMessage("�e>OP:  �6" + p.isOp());
			cs.sendMessage("�e>�6POS�e--");
			cs.sendMessage("�e>>Welt: �6" + p.getWorld().getName());
			cs.sendMessage("�e>>X: �6" + p.getLocation().getBlockX());
			cs.sendMessage("�e>>Y: �6" + p.getLocation().getBlockY());
			cs.sendMessage("�e>>Z: �6" + p.getLocation().getBlockZ());
			cs.sendMessage("");
			cs.sendMessage("�e>Banned: �6" + CommandBAN.isBanned(p.getName()));
			cs.sendMessage("�e>Fly: �6" + p.isFlying());
			cs.sendMessage("�e>Warms: �6" + CommandWARN.getWarns(p));
			cs.sendMessage("");
			
			
			return true;
			
		}
		
		return false;
	}

}
