package de.heining21.System.commands.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class CommandXP implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("xp")
				&& cs.hasPermission("System.xp")) {
			Player p = (Player) cs;
			if (args.length == 0) {
				cs.sendMessage("�eVerwendung: �a/XP <Spieler>");
			}
			if (args.length == 1) {

				Player p2 = null;
				try {
					p2 = p.getServer().getPlayer(args[0]);
				} catch (NullPointerException e) {
					cs.sendMessage(Data.UserNotExists);
					return true;
				}

				p.sendMessage(Data.p + "�eDer Spieler �2" + p2.getName()
						+ " �ehat�a " + p2.getLevel() + " �eLevel!");
			}
		} else {
			cs.sendMessage(Data.KeineRechte);
		}
		return true;
	}
}
