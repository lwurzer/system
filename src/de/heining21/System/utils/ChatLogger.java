package de.heining21.System.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.bukkit.entity.Player;

public class ChatLogger {

	private static String fileName() {
		DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		return date.format(cal.getTime()) + ".log";
	}

	private static String prefix() {
		DateFormat date = new SimpleDateFormat("[dd-MM-yyyy HH:mm:ss] ");
		Calendar cal = Calendar.getInstance();
		return date.format(cal.getTime());
	}

	public static void writeMsgToFile(Player sender, Player rec, String Message) {

		BufferedWriter bw = null;
		File file = new File("plugins/System" + File.separator + "Msg-logs");
		if (!file.exists()) {
			file.mkdir();
		}
		try {
			bw = new BufferedWriter(new FileWriter(file + File.separator
					+ fileName(), true));
			bw.write(prefix() + sender.getName() + " (uuid: "
					+ sender.getUniqueId() + ") -> " + rec.getName()
					+ " (uuid: " + rec.getUniqueId() + "):" + Message);
			bw.newLine();
			return;
		} catch (Exception ex) {
		} finally {
			try {
				if (bw != null) {
					bw.flush();
					bw.close();
				}
			} catch (Exception ex) {
			}
		}

	}

	public static void writeToFile(Player player, String message) {

		BufferedWriter bw = null;
		File file = new File("plugins/System" + File.separator + "Chatlogs");
		if (!file.exists()) {
			file.mkdir();
		}
		try {
			bw = new BufferedWriter(new FileWriter(file + File.separator
					+ fileName(), true));
			bw.write(prefix() + player.getName() + " (uuid: "
					+ player.getUniqueId() + "): " + message);
			bw.newLine();
			return;
		} catch (Exception ex) {
		} finally {
			try {
				if (bw != null) {
					bw.flush();
					bw.close();
				}
			} catch (Exception ex) {
			}
		}
	}
}
