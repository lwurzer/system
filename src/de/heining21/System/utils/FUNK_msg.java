package de.heining21.System.utils;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.heining21.System.Data;

public class FUNK_msg {
	public static void rToPlayer(Player sender, String Nachricht) {

		UUID senderUUID = sender.getUniqueId();

		UUID recUUID = SendertoRec.get(senderUUID);

		if (Bukkit.getPlayer(recUUID) != null) {
			Player rec = Bukkit.getPlayer(recUUID);

			sendMessage(sender, rec.getName(), Nachricht);

		} else {
			sender.sendMessage(Data.UserNotExists);
		}
	}

	public static void sendMessage(Player sender, String recName, String msg) {

		if (Bukkit.getPlayerExact(recName) != null) {

			Player rec = Bukkit.getPlayerExact(recName);
			sender.sendMessage("�7�l[�aIch �c-> �4" + recName + "�7�l]�r�f: �r" + msg);
			rec.sendMessage("�7�l[�4" + sender.getName() + " �c-> �aIch�7�l]�r�f: �r" + msg);

			SendertoRec.put(sender.getUniqueId(), rec.getUniqueId());
			SendertoRec.put(rec.getUniqueId(), sender.getUniqueId());

			ChatLogger.writeMsgToFile(sender, rec, msg);

		} else {

			sender.sendMessage(Data.UserNotExists);

		}

	}

	// Sender ==> Empf�nger
	public static HashMap<UUID, UUID> SendertoRec = new HashMap<UUID, UUID>();

}
