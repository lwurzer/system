package de.heining21.System.utils;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class UUIDCache implements Listener {

	public static void addUsersToUUIDCache() {

		for (Player pOnline : Bukkit.getOnlinePlayers()) {

			UUID pOnlineUUID;
			try {
				pOnlineUUID = UUIDFetcher.getUUIDOf(pOnline.getName());
				UUIDCacheMap.put(pOnline.getName(), pOnlineUUID);
			} catch (Exception e) {

			}

		}

	}

	public static void addUUIDToCache(Player p) {

		try {
			UUIDCacheMap.put(p.getName(), UUIDFetcher.getUUIDOf(p.getName()));
		} catch (Exception e) {
		}

	}

	public static void clearUUIDCache() {

		UUIDCacheMap.clear();

	}

	public static UUID getUserUUID(Player p) {
		
		return UUIDCacheMap.get(p.getName());
		
	}

	public static UUID getUserUUIDOffline(String name) {

		try {
			return UUIDFetcher.getUUIDOf(name);

		} catch (Exception e) {
			return ZERO_UUID;
		}

	}

	public static void removeUserFromUUIdCache(Player p) {

		UUIDCacheMap.remove(p.getName());

	}

	private static HashMap<String, UUID> UUIDCacheMap = new HashMap<String, UUID>();
	private static final UUID ZERO_UUID = UUID
			.fromString("00000000-0000-0000-0000-000000000000");

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent e) {

		addUUIDToCache(e.getPlayer());

	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerKick(PlayerKickEvent e) {

		removeUserFromUUIdCache(e.getPlayer());

	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerQuit(PlayerQuitEvent e) {

		removeUserFromUUIdCache(e.getPlayer());

	}

}
