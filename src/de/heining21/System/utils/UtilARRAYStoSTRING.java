package de.heining21.System.utils;

import java.util.List;

public class UtilARRAYStoSTRING {

	public static String arrayToString(String[] arrays, int erstesArg) {

		StringBuffer result = new StringBuffer();
		for (int i = erstesArg; i < arrays.length; i++) {
			result.append(arrays[i]);
			result.append(" ");
		}
		String mynewstring = result.toString();

		return mynewstring;

	}
	public static String listToString(@SuppressWarnings("rawtypes") List list, int erstesArg){
	
		StringBuffer result = new StringBuffer();
		for (int i = erstesArg; i < list.size(); i++) {
			result.append(list.get(i));
			result.append(" ");
		}
		String mynewstring = result.toString();

		return mynewstring;
		
		
	}

}
